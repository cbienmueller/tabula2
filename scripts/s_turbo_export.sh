#!/bin/bash

#________________________________________________________________________
# s_turbo_export.sh                                                       
# This file is part of tabula.info, which is free software under              
#     the terms of the GPL without any warranty - see the file COPYING 
# Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
#         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
#________________________________________________________________________
#
# s_turbo_export.sh lädt eine vorhandene export.html per ftp hoch, und
# verschiebt sie unter neuem Namen mit Zeitstempel in das Log-Verzeichnis
# Zugangsdaten und Pfad stehen in der ti.config. 
# 
# Dieses Skript wird vom script_dispatcher.sh aufgerufen, welcher per 
#       Cron-Job mit den Rechten von www-data regelmäßig aufgerufen werden!
#________________________________________________________________________

scriptpath=`dirname $(readlink -f ${0})`/
basepath=`realpath ${scriptpath}..`/
##basepath=`../bin/ti_tool.py basepath`
logpath=${basepath}data/log/

log=${logpath}"upload.log"

ftpc=`${basepath}bin/ti_tool.py  s_turbo_FTPCredentials`
ftppath=`${basepath}bin/ti_tool.py  s_turbo_FTPPath`
ftpcsspath=`${basepath}bin/ti_tool.py  s_turbo_FTPCSSPath`

exporthtml=${basepath}data/tmp/export.html
exporthtml2=${basepath}data/tmp/export.html
exportcss1=${basepath}www/static/ti.css



(
echo ${exporthtml}, ${exportcss1}

date
if [ -f ${exporthtml} ];
then
	echo "Das Skript "$0" sollte jetzt hochladen"
	ls -halt export*
	if curl -v --upload-file ${exporthtml}    -u ${ftpc} ${ftppath} 
	then
        	curl -v --upload-file ${exportcss1} -u ${ftpc} ${ftpcsspath} 
		curl -v --upload-file ${exporthtml2}    -u ${ftpc} ${ftppath}../lehrer/ 
		filename=${logpath}"export_"`date --iso-8601=minutes`.html
		mv ${exporthtml} ${filename}
	fi
fi
) >>${log} 2>&1

