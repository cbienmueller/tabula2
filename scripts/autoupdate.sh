#!/bin/sh
# Dieses schlichte (jetzt dash-kompatible) Skript aktualisiert die Installation mittels
#  git (nachdem lokale Änderungen ge-stash-t wurden).
#  Dann wird die Apache-Konfiguration aktualisiert.
#  Schließlich startet es den Apache neu. BieC 2021-03-05, 2022-07-03, 2023-08-19 (logpath)

mkdir -p /var/opt/tabula.info/system/log/
cd /opt/tabula.info/tabula2
(
    /bin/date
    /usr/bin/git stash
    /usr/bin/git config pull.ff only
    /usr/bin/git pull
    cp install/tabula.conf /etc/apache2/sites-available/
    /bin/systemctl restart apache2
)  >> /var/opt/tabula.info/system/log/update.log 2>&1

