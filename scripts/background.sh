#!/usr/bin/env bash

cd /opt/tabula.info/tabula2/bin/
echo `date` "Starting new background tasks" >> /var/opt/tabula.info/system/log/cronbg.log
for dir in /var/opt/tabula.info/datasite/*; do
    if [ -d "$dir" ]; then
        FILE=${dir}/data/configdb.sq3
        if [ -f "$FILE" ]; then
            python3 background.py `basename ${dir}` >>  /var/opt/tabula.info/system/log/cronbg.log &
            sleep 10
        fi
    fi
done


