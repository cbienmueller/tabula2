#!/usr/bin/bash
data_path=/var/opt/tabula.info/datasite/$1
mkdir -p ${data_path}/{cal,download,log,tmp,upload,work}
chown -R www-data:www-data ${data_path}
chmod -R g+w ${data_path}
