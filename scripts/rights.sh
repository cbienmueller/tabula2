#!/bin/bash
if [[ $(id -u) -ne 0 ]] ; then echo "**** Skript muss als root bzw. mit sudo gestartet werden *****" ; exit 1 ; fi
targetpath=/opt/tabula.info
varpath=/var/opt/tabula.info

    # Jetzt das aktuelle / neue Verzeichnislayout
    mkdir -p ${varpath}/datasite/local/{cal,download,log,tmp,upload,work}
    mkdir -p ${varpath}/web/local/{dyn,local}
    mkdir -p ${varpath}/system/log
    if [ -d ${targetpath}/local/data ] ; then
        mv ${targetpath}/local/data/* ${varpath}/datasite/local/
        mv ${targetpath}/local/www/* ${varpath}/web/local/
        rmdir ${targetpath}/local/data
        rmdir ${targetpath}/local/www
        rmdir ${targetpath}/local
    fi

    chown -R :www-data  ${targetpath}
    chmod       ug+x    ${targetpath}/tabula2/bin/*.py
    chmod       ug+x    ${targetpath}/tabula2/scripts/*.sh
    chown -R :www-data  ${varpath}
    chmod -R    ug+w    ${varpath}
    
    cp ${targetpath}/tabula2/install/tabula.conf /etc/apache2/sites-available/tabula.conf
    systemctl restart apache2
