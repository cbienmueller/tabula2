#!/bin/bash
if [[ $(id -u) -ne 0 ]] ; then echo "**** Skript muss als root bzw. mit sudo gestartet werden *****" ; exit 1 ; fi
if [ "$1" == "batch" ] ; then  echo "Batch für mehrere Instanzen wird nicht mehr unterstützt"; exit 1; fi
targetpath=/opt/tabula.info
varpath=/var/opt/tabula.info
runpath=/run/tabula.info
RESET=`tput sgr0`
BOLD=`tput bold`
GREEN=`tput setaf 2`${BOLD}
RED=`tput setaf 1`${BOLD}
CYAN=`tput setaf 6`${BOLD}
cat <<bgrstxt
*******************************************************************************
*                                                                             *
*        Setupskript für den DEV-Branch von tabula.info Version 2.1           *
*     für Debian 11,12 (Bullseye, Bookworm) oder Ubuntu LTS 22.04  (Jammy)    *
*                            Stand:  ${CYAN}16. August 2023${RESET}                          *
*                          (Neuinstallation repariert)                        *
*                                                                             *
*  !! Ein Upgrade von kleineren Versionen ist selten rückgängig zu machen !!  *
*     Benutzung auf eigene Gefahr! Lesen Sie im Zweifel diese Datei durch!    *
*                                                                             *
* Dieses Skript installiert die Software nach   /opt/tabula.info und          *
*               legt alle echten Daten nach /var/opt/tabula.info.             *
*               Sessiondaten kommen evtl. nach  /run/tabula.info.             *
*               Dabei wird ggf. eine Datenmigration durchgeführt.             *
*               Dann wird der Apache so konfiguriert, dass er dies nutzt.     *
*                                                                             *
* Sie dieses Skript immer wieder laufen lassen (außer Sie haben etwas in      *
*               /opt/tabula.info/tabula2 auf Dateiebene modifiziert).         *
*               Damit erhalten Sie jeweils die aktuelle Version.              *
*     ${BOLD}Fertigen Sie stets vorher ein Backup des gesamten Verzeichnisses an!${RESET}    *
*******************************************************************************
* Bereit? ${GREEN}Eingabetaste${RESET} zum Installieren!     Doch nicht? ${RED}Strg-C${RESET} zum Abbrechen *
*******************************************************************************
bgrstxt
read
mylog=`mktemp`
mytime=`date +%Y-%m-%d_%H.%M`       # Aktuelle Zeit, problemlos für Dateinamen
oldpath=/opt/tabula.old.${mytime}
{
echo tabula.info-Installation nach $targetpath beginnt `date`
echo Schritt 1: Installationen
    ## Debian/Ubuntu-spezifisch - ggf. ändern, händisch ausführen o.Ä.
    apt-get update
    apt-get remove --purge imagemagick -y
    apt-get install --yes git apache2 libapache2-mod-wsgi-py3 python3 python3-bcrypt python3-icalendar python3-werkzeug graphicsmagick curl at tree fonts-linuxlibertine sqlite3 coreutils locales
    if [ "$?" != "0" ]; then
        cat <<installfehlgeschlagen
        ${RED}* Leider ist das Nachinstallieren der benötigten Software fehlgeschlagen.
        * Bitte obige Fehlermeldungen überprüfen und neu starten.
        * Abbruch der Installation von t.i ${RESET}
installfehlgeschlagen
        exit
    fi
    ## deutsches und US locale sicherstellen
    sed -i 's/^# *\(de_DE.UTF-8\)/\1/' /etc/locale.gen
    sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen
    locale -a
    locale-gen
    update-locale 
    ## Die folgenden Tools sind nur eine Empfehlung für leichtere Aministration
    # apt-get install --yes libvte9 geany x11-utils mc openssh-server
    
echo Schritt 2: evtl. Upgrade und Download
    systemctl stop apache2
    cd /opt/
    # Upgrade-Test via existierende Verzeichnisse
    # Sehr alte Version
    touch $targetpath/cgi-bin/data/config.sq3 >/dev/null 2>&1
    if [ -f $targetpath/cgi-bin/data/config.sq3 ] ; then
        echo "Migration einer uralten Installation - KEINE Daten werden übernommen!"
        echo "alte Version gesichert in ${oldpath}"
        mv $targetpath ${oldpath} 
    fi
    # Version 1.2 und älter
    touch $targetpath/data/config.sq3 >/dev/null 2>&1
    if [ -f $targetpath/data/config.sq3 ] ; then
        echo "Migration einer Installation <= 1.2 - nur Konfigurations-Daten werden übernommen!"
        echo "alte Version gesichert in ${oldpath}"
        mv $targetpath ${oldpath} 
    fi
    
    # Jetzt das aktuelle / neue Verzeichnislayout
    mkdir -p ${targetpath}
    mkdir -p ${varpath}/datasite/local/{cal,download,log,tmp,upload,work}
    mkdir -p ${varpath}/web/local/{dyn,local}
    mkdir -p ${varpath}/system/log
    mkdir -p ${runpath}
    chown -R www-data:www-data ${runpath}
    # runpath ist nach dem nächsten Reboot weg, wird aber durch Schritt 5 (s.u.) zukünftig automatisch erzeugt

    if [ -d ${targetpath}/local/data ] ; then
        echo "Migration einer 2.0 Installation - alles wird übernommen!"
        mv ${targetpath}/local/data/* ${varpath}/datasite/local/
        mv ${targetpath}/local/www/* ${varpath}/web/local/
        rmdir ${targetpath}/local/data
        rmdir ${targetpath}/local/www
        rmdir ${targetpath}/local
    fi

        
    if [ -f ${oldpath}/cgi-bin/data/config.sq3 ] ; then
	    echo "Uebernehme keine Uraltdaten!!!"
        #cp -av ${oldpath}/cgi-bin/data/{*.sq3} 	${targetpath}/local/data
        #cp -av ${oldpath}/local/*                    			${targetpath}/local/www/local/
    fi
    if [ -f ${oldpath}/data/config.sq3 ] ; then
	    echo "Uebernehme Altdaten"
        cp -av ${oldpath}/data/{configdb.sq3,accessdb.sq3,messages.sq3,persons.sq3}  ${varpath}/datasite/local/
	    cd ${varpath}/local
# Merge nun die Datenbanken - aus Syntaxgründen linksbündig
sqlite3 messages.sq3 <<EOF
.output messages_dump.sql
.dump messages
.quit
EOF
sqlite3 persons.sq3 <<EOF
.output persons_dump.sql
.dump persons
.quit
EOF

        sqlite3 content.sq3 < messages_dump.sql
        sqlite3 content.sq3 < persons_dump.sql
        sqlite3 content.sq3 ".tables"
        rm messages.sq3
        rm persons.sq3
        cp -av ${oldpath}/www/local/*         ${varpath}/web/local/www/local/
    fi
    
    # nun wird die Software entfernt und neu eingespielt
    cd ${targetpath}
    rm -r tabula2
    git clone -b dev https://cbienmueller@bitbucket.org/cbienmueller/tabula2.git
        
echo Schritt 3a: Dateirechte in Unterverzeichnissen von $targetpath
    chown -R :www-data  ${targetpath}
    chmod       ug+x    ${targetpath}/tabula2/bin/*.py
    chmod       ug+x    ${targetpath}/tabula2/scripts/*.sh
echo Schritt 3b: Dateirechte in Unterverzeichnissen von $varpath
    chown -R :www-data  ${varpath}
    chmod -R    ug+w    ${varpath}
    
echo Schritt 4: Apache2
    # Erlaube auch dem Apache at-Befehle auszuführen indem sein Username aus der deny-Datei gelöscht wird
    grep --invert-match "www-data" /etc/at.deny > temp_at_deny && mv temp_at_deny  /etc/at.deny 
    
    cp ${targetpath}/tabula2/install/tabula.conf /etc/apache2/sites-available/tabula.conf
    
    # Die folgenden Befehle sind Debian-spezifisch (also auch für Ubuntu u.ä.) und müssen auf anderen Systemen ersetzt werden!
    a2ensite tabula

    # ggf. alte Module und Konf. entfernen
    a2dismod -q cgid        
    a2dismod -q cgi
    a2dissite -q 000-default
    a2dissite -q default 
    unlink /etc/apache2/sites-enabled/000-tabula* >/dev/null 2>&1
    
    # jetzt für alle Änderungen neu starten
    systemctl restart apache2

echo Schritt 5: Conf-Datei für systemd für tabula.info-Verzeichnis in /run anlegen
    cp ${targetpath}/tabula2/install/tabula.info.conf /usr/lib/tmpfiles.d/
    
echo Schritt 6: Background-Task anlegen / ändern
    if egrep tabula2.bin.background.py /etc/crontab; then
        echo Entferne background.py-Aufruf
        grep --invert-match "tabula2.bin.background.py" /etc/crontab > temp_crontab && mv temp_crontab  /etc/crontab 
    fi
    if egrep tabula2.scripts.background.sh /etc/crontab; then
        echo Eintrag existiert bereits - ggf. kontrollieren!
    else
        echo Eintrag wird angelegt
        echo " " >> /etc/crontab
        echo "# ti backgroundaufgaben" >> /etc/crontab
        echo "*/2 *   * * *   www-data ${targetpath}/tabula2/scripts/background.sh" >> /etc/crontab
    fi
    
    
cat <<endetxt
****************************************************************
****         Ende der Installation von tabula.info          ****
****************************************************************
**** Starten Sie nun die Anzeige in einem Browser mit einer **** 
**** IP-Adresse dieses Servers oder localhost               ****
****************************************************************
**** Administrieren Sie tabula.info im Browser über         ****
**** http://<ipadresse>/ti/management                       ****
****************************************************************
endetxt

} | tee $mylog
finallog=${varpath}/system/log/install2.1dev_${mytime}.log
mv ${mylog} ${finallog} && echo Logfile gespeichert unter ${finallog}

