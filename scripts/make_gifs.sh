#!/bin/bash

#################
# Programmstart #
#################

scriptpath=`dirname $(readlink -f ${0})`/
basepath=`realpath ${scriptpath}..`/
cgipath=${basepath}bin/
logpath=${basepath}data/log/
staticpath=${basepath}www/static/
cd ${scriptpath}../data/tmp

rm nr???.gif
target=${staticpath}cd/
rm ${target}c???.gif

lastnr=bar
convert -size 64x32 xc:"#000044" -fill yellow -gravity SouthEast -font "Ubuntu-Mono-Bold" -pointsize 30 -draw "text 0,0 '- '" ${target}cbar.gif
	
for i in $( seq 0 200); do
	nr="$(printf '%03d' "$i")"
	convert -size 64x32 xc:"#000044" -fill "#336699" -gravity SouthEast -font "Ubuntu-Mono-Bold" -pointsize 30 -draw "text 0,0 '${i}s'" nr${nr}.gif
	convert nr${nr}.gif ${target}c${lastnr}.gif -set delay 100 -loop 1 -layers OptimizePlus ${target}c${nr}.gif
	lastnr=$nr
done

rm nr???.gif
rm ${target}l???.gif

lastnr=bar
convert -size 64x32 xc:"#DDDDDD" -fill yellow -gravity SouthEast -font "Ubuntu-Mono-Bold" -pointsize 30 -draw "text 0,0 '- '" ${target}cbar.gif
	
for i in $( seq 0 200); do
	nr="$(printf '%03d' "$i")"
	convert -size 64x32 xc:"#DDDDDD" -fill "#336699" -gravity SouthEast -font "Ubuntu-Mono-Bold" -pointsize 30 -draw "text 0,0 '${i}s'" nr${nr}.gif
	convert nr${nr}.gif ${target}l${lastnr}.gif -set delay 100 -loop 1 -layers OptimizePlus ${target}l${nr}.gif
	lastnr=$nr
done

rm nr???.gif
