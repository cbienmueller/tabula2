'''
________________________________________________________________________
 infobox.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul liefert zusätzliche Infoseiten für leeren Platz
________________________________________________________________________

'''

# Batteries
import random

# TI
import ti_lib

INFO_TXT = '''
    <div class="infos" style="text-align:center;">
    <img src="{filename}" style="max-width: 100%; height: auto; width: auto; " />
    {info_by}
    </div>
'''
INFO_TXT_MINI = '''
    <div class="infos" style="text-align:center;">
    <img src="{filename}" style="max-width: 50%; height: auto; width: auto; " />
    {info_by}
    </div>
'''


def get_info(ti_ctx, title="Info", mini=False):
    """Diese Funktion liefert bereits hochgeladene Infobilder eingebettet zurück
    
    Dazu werden zwei Stück HTML erzeugt und zurückgegeben, welche die Info-Bilder per SRC einbinden.
    """
    heute_iso_date = ti_lib.iso_date_heute_plus_delta_days(0)
    infopath = ti_ctx.paths.get_dyn_path("info") 
    info_pix_cfgs_list = dir2info_pix_configs_list(ti_ctx, infopath)
    the_pix = []
    for ipcfg in info_pix_cfgs_list:
        if ipcfg.prio:
            if ipcfg.lastday >= heute_iso_date:
                for i in range(ipcfg.prio):
                    the_pix.append(ipcfg)
            else:  # seltener Fall eines abgelaufenen Bildes, das noch eine Prio > 0 hat - das ändern wir
                ipcfg.prio = 0
                ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
    
    pic2show1 = ""
    pic2show2 = ""
    info_by1 = ""
    info_by2 = ""
    ti_ctx.res.debug("Summe über alle Info-Seiten-Prios", len(the_pix))
    if not the_pix:
        # Logo (lokal, dann ti)
        pic2show = '/ti/local/logo.png'  # Logo der Schule
        if not (infopath / pic2show).exists():
            if not ti_ctx.cnf.get_db_config_bool('Flag_HideTI', False):
                # TI-Logo, ggf. w. ü
                pic2show = '/ti/static/pix/logo.png'  # TI-Logo
            else:  # aha, keine Logos, keine Bilder, report Mega failure :-)
                return ti_lib.Multi_Detail_Response(cancel_flag=True)
        return ti_lib.Multi_Detail_Response([INFO_TXT.format(filename=pic2show, info_by='')])
        
    else:
        # Zufallsbilder wählen
        random.shuffle(the_pix)
        # ti_ctx.res.debug("Liste aller Bilder", str(thenames))
        
        for pixcfg in the_pix:
            if not pic2show1:
                pic2show1 = pixcfg.name
                info_by1 = pixcfg.author
            elif not pic2show2:
                # nur wenn anderer Name als vorher (Namen können doppelt vorkommen um erhöhte Priorität einzubringen)
                if pixcfg.name != pic2show1:
                    pic2show2 = pixcfg.name
                    info_by2 = pixcfg.author
                    break
    # irgend eine Bild soll also angezeigt werden
    if info_by1:
        info_by1 = f'''<br><div style="text-align:right;font-size:80%">{_('Infoseite hochgeladen von') } {info_by1}</div>'''
    if info_by2:
        info_by2 = f'''<br><div style="text-align:right;font-size:80%">{_('Infoseite hochgeladen von') } {info_by2}</div>'''
    skeleton = INFO_TXT_MINI if mini else INFO_TXT
    md = ti_lib.Multi_Detail_Response()
    md.append_s(skeleton.format(filename=f'{ti_ctx.paths.get_dyn_url()}/info/{pic2show1}', info_by=info_by1))
    if pic2show2:
        md.next_abschnitt()
        md.append_s(skeleton.format(filename=f'{ti_ctx.paths.get_dyn_url()}/info/{pic2show2}', info_by=info_by2))
    return md
    
    
def dir2info_pix_configs_list(ti_ctx, infopath):
    """Gibt eine Liste von InfoPixCfg-Objekten zurück.
    
    Es kommen nur Dateinamen in Frage für die es auch die Datei name-thumb.ext gibt!
    Treffer werden in der Datenbank ausgelesen.
    """
    info_pix_cfgs_list = []
    lastname = ''
    lastext = ''
    directory = list(infopath.iterdir())
    directory.sort()  # thumb kommt vor der eigentlichen Datei!
    # ti_ctx.res.debug("quasidirectory", directory)
    if directory: 
        for filepath in directory:            
            if not filepath.suffix:
                continue
            if '-thumb' in filepath.stem:  # thumbnail gefunden? Merken!
                lastname = filepath.stem.split('-thumb')[0]
                lastext = filepath.suffix
            elif filepath.stem == lastname and filepath.suffix == lastext:  
                # passt zu einem gemerkten thumbnail? In Liste eintragen!
                info_pix_cfgs_list.append(ti_ctx.cnf.infopix_get_full_cfg(filepath.name))
    return info_pix_cfgs_list


if __name__ == '__main__':
    ti_lib.quickabort2frames()
