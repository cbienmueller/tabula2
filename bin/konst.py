'''
________________________________________________________________________
 konst.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul hält insbesondere die Konstanten, welche in verschiedenen Modulen genutzt werden.
    Die Versionsinfomation wird nun in version.py gehalten und geändert.
________________________________________________________________________

'''

import version

RECHTEPersonenruf = 1
RECHTEMeldungen = 2
RECHTEMeldungenerweitert = 4
RECHTEInfoupload = 8
RECHTEInfouploaderweitert = 16
RECHTEVertretungsplan = 32
RECHTEKalender = 64
"..."
RECHTEAdministration = 2 ** 15


TI_MAXTIME = 300  # Sekunden - länger nicht sinnvoll - oder?

TOOLPATH = "/home/tabula/upload/"


# für man.py (MIDDLE darf beliebig wiederholt werden)
MAN_FRAMES_HTML_MENU = '''
    <div class="managementmenu">
        {man_menu}
    </div>
    <div class="managementrechts"><!-- enthält erst 2x detail für Titel und Meldungen -->
        <div class="managementdetail" style="min-height:30px;">
            <h2 style="margin:0px;margin-top:3px;text-align:center;">{man_titel}</h2>
        </div>
        <div class="managementdetail" style="min-height:30px;border:0px">
            {man_meldung}
        </div>
        <div class="unfloatleft"></div>
        <!-- Es folgen die restlichen (eigentlichen) Details -->\n'''
    
    
MAN_FRAMES_HTML_END = '''
        <div class="unfloatleft"></div>
    </div><!-- Ende von managementrechts -->
    <div class="unfloatleft"></div>
'''+version.TI_BRANDING

BLACK_SCREEN = '''
        <div style="color:#300;text-align:center;
                    position:fixed;top:{}px;left:{}px;
                    width:10em;">
            {}<br>{}<br><small><i>{}</i></small>
        </div>
'''

INCLUDESCRIPTS = '''
    <script src="/ti/static/js/jquery.js" type="text/javascript"></script>
    <script src="/ti/static/js/ti.js" type="text/javascript"></script>
'''


def get_configkategorien(mit_turbo=False):
    '''Liefert ein Array aus Tupeln (KategorieName:Struktur) zurück.
    Die Strukturen bestehen Arrays aus Tupeln/Tripeln (Parameter, Hilfetext, Standardwert)'''     
    
    ct3_5_hilfe = _('''Benenne neben Q-Stufen und Lehrkräfte hier eine von drei weiteren Clientkategorien,
                    an welche die Meldungen adressiert werden können.
                    Minuszeichen verhindert Anzeige. A, B, M sind die entsprechenden Endungen,
                    die beim Hochladen von Plänen ausgewertet werden (wie auch Q und L)''')
                    
    ti_config_b = [
        ('TI_Title', _('Titel des Tabulasystems'), _('Aushang')),
        ('AushangDauer', _(
            'Wieviel Sekunden soll die reine Meldungs/Infoseite (phase=0) mindestens angezeigt werden.'
            '<br>Addiert wird dazu dynamisch:<br>&nbsp;* je Meldung 1-3 Sekunden (je nach Länge)<br>'
            '&nbsp;* je 3 Personenrufe eine Sekunde.<br>Diese Zeit wird auch addiert, wenn es nur einen halbseitigen '
            'Plan gibt und rechts daneben alle Meldungen und Info-Seiten angezeigt werden.'), '10'),
        ('Login_Duration', _(
            'Wieviele Minuten bleibt ein User eingeloggt'), '15'),
        ('Flag_Hide_Navi', _(
            'Soll die Navigationsleiste verborgen werden?'), 'Nein'),
        ('Flag_HideTI', _(
            'Sollen die Versionsnummer, Produktname und Platzhalter verborgen werden?'), 'Nein'),
        ('AusblendeVerzoegerungSec', _(
            'Anzahl Sekunden, nach denen Extrameldungen ausgeblendet werden'), '12'),
        ('+SubTextL', _(
            'Nachricht, die am unteren Rand links dauerhaft angezeigt wird'), ''),
        ('+SubTextR', _(
            'Nachricht, die am unteren Rand rechts dauerhaft angezeigt wird'), ''),
        ('ManagementNetwork', _(
            'IP-Adressbereich, dem Managementrechte ohne eigenes Login zugebilligt werden. '
            'Schreibweise nur: 192.168.2.0/24 für das so beginnende ClassC-Netz.<br><i>Kopplung</i> in der '
            'Clientkonfiguration ermöglicht, dass bestimmte Clients im ManagementNetwork die Rechte eines '
            'gleichnamigen Users bekommen.'), '127.0.0.1/32'),
        ('Man_TrustedProxy', _(
            'IP-Adresse eines vorgelagerten (reverse) Proxies, dessen x-forwarded-for '
            'vertraut werden soll. Sicherheitsrelevant für ManagementNetwork!'), ''),
        ('ti_site_code', _(
            '''Code (z.B. 4711), den der vertrauenswürdige Proxy mit 'RequestHeader set X-TiSite "MYGYM-4711" ' '''
            ''' überträgt für freien Zugriff - wenn die Site MYGYM heißt.'''), ''),
        ('Flag_Navi_unten', _(
            'Soll die Navigation für TouchScreen unten angezeigt werden?'), 'Nein'),
        ('R_locale', 'Sprache, nur wenn installiert.<br>Language, only if installed.', 'de_DE en_US da_DK'),
    ]
    ti_config_e = [('ClientTypeQ1', 'Name der ersten Q-Stufe', 'Q12'),
                   ('ClientTypeQ2', 'Name der zweiten Q-Stufe', 'Q13'),
                   ('ClientTypeA', ct3_5_hilfe, '-'),
                   ('ClientTypeB', ct3_5_hilfe, '-'),
                   ('ClientTypeM', ct3_5_hilfe, '-'),
                   ('NachtruheAb',
                    'Ab welcher Uhrzeit (hh:mm) sollen die Bildschirme nur schwarz anzeigen.', '22:00'),
                   ('NachtruheBis', 
                    'Bis zu welcher Uhrzeit (hh:mm) sollen die Bildschirme nur schwarz anzeigen.', '06:00'),
                   ('Flag_IPeinblenden', 
                    'Soll (zum Einrichten) Name und IP rechts unten eingeblendet werden', '0'),
                   ('ms_per_scrollline', 
                    'Alle wieviele Millisekunden soll eine Pixelzeile gescrollt werden. Zwischen 10 und 150!', '20'),
                   ]

    messages_configs = [
        ('Message_MaxTitleLength', _(
            'Maximale Länge einer Meldung (Überschrift)'), '60'),
        ('Message_Lasthour_Vorlage', _(
            'was ist die vorgeschlagene letzte Anzeigeunterrichtsstunde'), '7'),
        ('max_virt_mess_lines', _(
            'Wie viele Zeilen lang darf die Personenruf+Meldungs-Spalte sein, '
            'bevor die die Personenrufe nach rechts kommen.<br>Dabei werden Tage und Meldungsüberschriften mit dem '
            'Faktor 1, 2 gezählt.<br>Den aktuellen gerundeten Wert kann man im Quelltext der angezeigten '
            '"Aushang"-Seite unter virtuelle_nachrichten_zeilenzahl in den Debuggingmeldungen in den Kommentaren '
            'am Ende nachlesen'), '27'),
        ('PrioWechselStunde', (
            'Ab welcher Unterrichtsstunde werden Meldungen für "übermorgen" nicht mehr '
            'mit geringer Priorität angezeigt.'''), '7')]

    info_configs = [
        ('Flag_ShowInfos', _(
            'Sollen Info-Seiten angezeigt werden'), 'True'),
        ('Flag_ShowMultiInfos', _(
            'Sollen bei Gelegenheit auch 2 Info-Seiten gleichzeitig angezeigt werden'), 'True'),
        ('info_pdf_ani_delay', _(
            'Werden mehrseitige pdf-Dateien als Info-Seiten hochgeladen, '
            'so werden sie gif-animiert. Dieser Wert (zum Zeitpunkt des Hochladens) '
            'gibt die Anzeigedauer einer Seite in Sekunden an.'), '5'),
        ('MaxInfoPix', _(
            'Wie viele Info-Seiten dürfen angelegt werden?'), '6'),
        ('info_default_anzeigedauer', _(
            'Vorgeschlagenes Enddatum für Infoseiten ist heute plus dieser Wert'), '3')]

    cal_configs = [
        ('Flag_ShowCal', _(
            'Sollen Kalendereinträge angezeigt werden'), 'True'),
        ('cal_calendar_days', _(
            'Für wie viele Tage (inkl. heute) sollen Kalendereinträge gezeigt werden.'), '8'),
        ('cal_eventname_maxlen', _(
            'Basis für Kürzung der Eventnamen.'), '30'),
        ("cal_min_reload_min", _('minimale Zeit zwischen Reloads in Minuten'), '30') 
            ]    
    
    persons_configs = [
        ('+Ziele', _(
                'Die durch Semikolon getrennten Ziele, zu denen Personen gerufen werden können. '
                'Ein führendes Pluszeichen (+) sortiert den Eintrag in die erste Gruppe.'
                'Eine führende Tilde (~) sortiert den Eintrag in die dritte Gruppe.'
                'Rest in der mittleren Gruppe. Innerhalb jeweils alphabetisch.'
                ), '+Sekretariat;Hr. Müller;~Fr. Adams'),
        ('psns_min_entry_len', _('Minimale Länge des Namens'), '3'),
        ('Flag_psns_caps', _('Kapitälchen für ersten Nemen?'), 'False')
            ]

    apaxp_help = _(
            'Bei der PDF-Datei sollen i.d.R. außen herum Ränder abgeschnitten werden. '
            'Geben Sie für jede Himmelsrichtung die Prozentzahl an, die entfernt werden sollen')

    s_any_struct = [
        ('!INFO', _(
            'Bitte beachten Sie, dass Änderungen in den folgenden Einstellungen eine erneute Konvertierung auslösen.'), ''),
        ('s_any_dauer2', _(
            'Standardanzeigedauer für zweispaltige Pläne'), '55'),
        ('s_any_dauer1', _(
            'Standardanzeigedauer für einspaltige Pläne. <br>Werden zwei einspaltige Pläne '
            'nebeneinander angezeigt, so addieren sich die Dauern'), '35'),
        ('+s_any_dateinamen', _(
            'Dateiname(n) beim Download (außer file://), mehrere mit ";" trennen; '
            '"{}" wird dabei durch die Ziffern von 0 bis 9 ersetzt, aber ab 2 nach einer fehlenden Datei abgebrochen; '
            '"{datum} wird durch das ISO-Datum von heute und der nächsten 26 Tage ersetzt.'''
            ), 'subst_00{}.htm;mensaplan.pdf'),
        ('+s_any_quelle', _(
            'Von welcher Adresse (inkl. Protokoll) sollen Vertretungspläne gelesen werden? '
            'Unterstützt werden file:// (lokaler Pfad), sowie http://, https:// und ftp://. '
            'Vor dem Herunterladen wird getestet ob die Datei vplansemaphore.txt existiert. '
            'Nur dann werden die Vertretungsplan-Dateien geladen.'), ''),
        ('s_any_hintergrundtakt_s', _(
            'Legt den MINDEST-Abstand (in s) zwischen zwei Aufrufen des Hintergrundprozesses fest '
            '(z.B. 300 entspricht 5 Minuten).<br>Unter einer Minute ist meist nicht sinnvoll!<br>'
            'Ausgelöst wird der Hintergrundprozess sowieso erst durch einen Aufruf einer Seite auf dem Server.'), '300'),
        ('s_any_hintergrundtakt_rushhour_s', _(
            'Legt den MINDEST-Abstand (in s) zwischen zwei Aufrufen des Hintergrundprozesses '
            '<b>während der Rushhour</b> fest (z.B. 300 entspricht 5 Minuten).<br>'
            'Unter einer Minute ist meist nicht sinnvoll!<br>'
            'Ausgelöst wird der Hintergrundprozess sowieso erst durch einen Aufruf einer Seite auf dem Server.'), '120'),
        ('s_any_hintergrund_rushhour', _(
            'Legt die Rushhour für den Hintergrundprozesses fest, z.B. 7 für 7:00 bis 7:59 Uhr'), '7'),
        ('s_any_nur_erster_plan_bis_uhrzeit', _(
            'Bis zu dieser Uhrzeit (hh:mm) wird nur die erste Planseite angezeigt - '
            'je nach Darstellung können das ein oder zwei Pläne sein.'''), '8:00'),
        ('s_any_autoconvert_intervall_min', _(
            'Nach wie vielen Minuten soll ein Turboplan neu konvertiert werden?'), '20'),
        ('flag_s_any_kein_zweitplan', _(
            'Soll lieber eine Infoseite statt eines zweiten Plans angezeigt werden, '
            'wenn auf der Planseite noch Platz ist, da der erste Plan nur die linke Hälfte beansprucht?'), '0'),
        ('flag_turboplan', _(
            'Soll die Konfiguration für den Turboplan angezeigt werden?'), '0'),
    ]
    s_html_struct = [
        ('!INFO', _(
            'Bitte beachten Sie, dass Änderungen in den folgenden Einstellungen eine erneute Konvertierung auslösen.'), ''),
        ('flag_s_any_html_do_scroll', _(
            'Soll in HTML-Dateien JavaScript eingefügt werden um ein automatisches Scrolling '
            '(nur bei Bedarf) zu aktivieren?'), 'Ja'), 
        ('flag_s_any_html_remove_refresh', _(
            'Soll aus HTML-Dateien ein meta-tag, das einen automatischen Refresh macht,'
            ' entfernt werden?'), 'Ja'),
        ('s_any_html_max_tabellenzeilen', _(
            'Nach wievielen Tabellenzeilen soll die HTML-Darstellung zweispaltig werden? '
            '<10 oder zu groß: Abgeschaltet<br>EXPERIMENTELLES FEATURE, klappt nur mit recht einfachen '
            'HTML-Tablellen!'''), '0'),
        ('s_any_html_split_extratag', _(
            'Wenn die zu spaltende Tabelle in einem Container sitzt, '
            'so kann er hier eingetragen werden - z.B. center für Untis'), ''),
        ('flag_s_any_html_ganzseitig', _(
            'Sollen HTML-Seiten immer ganzseitig angezeigt werden? '
            'Sonst nur wenn via Split eine lange Seite geteilt wird.'), '0')
    ]    
    
    # Entfernt: ,
    #    ('flag_s_any_schnellscrollen', _(
    #        'soll das Scrollen von HTML-Seiten beschleunigt sein?'), '0')
    
    s_pdf_struct = [
        ('!INFO', _(
            'Bitte beachten Sie, dass Änderungen in den folgenden Einstellungen eine erneute Konvertierung auslösen.'),
            ''),
        ('s_any_pdf_seitendauer', _(
            'Wie lange soll beim waagrechten Scrollen durch die Seiten '
            'einer mehrseitigen PDF-Datei je Seite gewartet werden?'), '10'),
        ('s_any_pdf_hoch_abschneiden_nord_p', apaxp_help, '5'),
        ('s_any_pdf_hoch_abschneiden_west_p', apaxp_help, '5'),
        ('s_any_pdf_hoch_abschneiden_sued_p', apaxp_help, '5'),
        ('s_any_pdf_hoch_abschneiden_ost_p', apaxp_help, '5'), 
        ('s_any_pdf_hoch_x_prozent', _(
            'Auf wie viele Prozent in x-Richtung soll das PDF skaliert werden?<br>'
            'Verzerrt einerseits, kann aber ggf. Raum besser ausnutzen!'), '100'),
        ('s_any_pdf_hoch_x_breite', _(
            'Wie breit soll ein aus einer PDF-Seite konvertiertes Bild in Pixel sein?<br><br>'
            '1920 für Querformat, ganzseitig eine Seite (mit x_prozent bis zu 135)<br><br>'
            '960 für zwei Seiten auf dem Bildschirm (hochkant: mit x_prozent bis zu 135 und y_hoehe=1000,'
            '<br>quer: z.B. x_prozent=100 und y_hoehe=680 oder <br>'
            'x_prozent=85 und y_hoehe=800)<br><br>'
            '640 für hochkant, drei Seiten auf dem Bildschirm (mit x_prozent ab 90)<br>'''), '640'),
        ('s_any_pdf_hoch_y_hoehe', _(
            'Wie hoch soll ein aus einer PDF-Seite konvertiertes Bild in Pixel sein<br>'
            'hochkant für FullHD bis zu 1040px'), '1040'),
        ('s_any_pdf_quer_abschneiden_nord_p', apaxp_help, '5'),
        ('s_any_pdf_quer_abschneiden_west_p', apaxp_help, '5'),
        ('s_any_pdf_quer_abschneiden_sued_p', apaxp_help, '5'),
        ('s_any_pdf_quer_abschneiden_ost_p', apaxp_help, '5'),
        ('s_any_pdf_quer_x_prozent', _(
            'Auf wie viele Prozent in x-Richtung soll das PDF skaliert werden?<br>'
            'Verzerrt einerseits, kann aber ggf. Raum besser ausnutzen!'), '100'),
        ('s_any_pdf_quer_x_breite', _(
            'Wie breit soll ein aus einer PDF-Seite konvertiertes Bild in Pixel sein?<br><br>'
            '1920 für Querformat, ganzseitig eine Seite (mit x_prozent bis zu 135)<br><br>'
            '960 für zwei Seiten auf dem Bildschirm (hochkant: mit x_prozent bis zu 135 und y_hoehe=1000,'
            '<br>quer: z.B. x_prozent=100 und y_hoehe=680 oder <br>'
            'x_prozent=85 und y_hoehe=800)<br><br>'
            '640 für hochkant, drei Seiten auf dem Bildschirm (mit x_prozent ab 90)<br>'''), '1910'),
        ('s_any_pdf_quer_y_hoehe', _(
            'Wie hoch soll ein aus einer PDF-Seite konvertiertes Bild in Pixel sein<br>'
            'hochkant für FullHD bis zu 1040px'), '1040')
    ]    
    
    turbo_struct = [
        ('s_turbo_maxRowsInSchedule', 
            'legt maximale Zeilenanzahl im Vertretungsplan fest.'
            ' Muss zu Displaygröße, Browserconfig und frontend.css passen', '26'),
        ('s_turbo_FTPCredentials',
            'FTP-Zugangsdaten - wird von externem Skript benutzt', 'user:passwort'),
        ('s_turbo_FTPPath', 
            'Pfad, wo mit FTP die export.html abgelegt werden soll - wird von externem Skript benutzt',
            'ftp://eebb.kronberg-gymnasium.de/secure/'),
        ('s_turbo_FTPCSSPath', 
            'Pfad, wo mit FTP die CSS-Datei abgelegt werden soll - wird von externem Skript benutzt', 
            'ftp://eebb.kronberg-gymnasium.de/'),
        ('flag_s_turbo_userinput', 
            'Flag, ob User über das Web rückmelden dürfen (experimentell!).', 
            '0'),
            
    ]
    karray = [(_('Basis'), 'basis', ti_config_b),
              (_('Erweitert'), 'extd', ti_config_e),
              (_('Meldungen'), 'mess', messages_configs),
              (_('Personenruf'), 'pers', persons_configs),
              (_('Infoseiten'), 'info', info_configs),
              (_('Kalender'), 'cal', cal_configs),
              (_('Plananzeige Allgemein'), 's_any', s_any_struct),
              (_('Plananzeige HTML'), 'html', s_html_struct),
              (_('Plananzeige PDF'), 'pdf', s_pdf_struct)]
    if mit_turbo:
        karray.append(('Plananzeige Turbo', 'turbo', turbo_struct))
    return karray


