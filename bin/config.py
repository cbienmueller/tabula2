'''
________________________________________________________________________
 config.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config.py vereint den Dialog und die Tätigkeiten zur Konfiguration
________________________________________________________________________
'''

# Batteries
import lzma
import sys

# TI
import ti_lib
import version
import konst
    

def htmloptionenzeile(bitmuster, bez_fkt, url, 
                      inputname, hiddenname, 
                      hiddenvalue, csrftoken, autobreak=False):
    '''gibt 
        Buttons aus, die nach dem 
        Bitmuster eingefärbt sind und mit den 
        kuerzeln, welche die bez_fkt als zweiten Wert zurückliefert, beschriftet sind.
    Ein Klick ruft die 
        url auf, wobei drei Werte übertragen werden: versteckt der
        hiddenname mit dem hiddenvalue, sowie
        csrftoken mit dem csrftoken sowie als
        inputname der angeklickte Wert (eine Zweierpotenz)'''
    
    liste = ['\n\t<!-- eine htmloptionenzeile aus config.py beginnt -->']
    staticcontent = f'''
    <div style="float:left;width:{{weite}}em;text-align:right;">
        <form action="{url}" method="post" >
            <input name="{hiddenname}" value="{hiddenvalue}" type="hidden" />
            <input name="csrftoken" value="{csrftoken}" type="hidden" />'''
    
    for i in range(20):
        gn, gk = bez_fkt(i)  # GruppenName, GruppenKürzel
        if gk and not gk == "  ":
            bitwert = 1 << i
            wert = bitmuster & bitwert
            weite = 1.5
            weite += 0.6*len(gk)
            liste.append(staticcontent.format(weite=weite))
            liste.append(f'''
            <input name="{inputname}" value="{str(bitwert)}" type="hidden" />
            <input style="background:{'green' if wert else 'orange'}
            {';' if not wert else ';font-weight:bold;'}" value="{gk}" type="submit" />
        </form>
    </div>''')
    liste.append('\n\t<!-- eine htmloptionenzeile aus config.py endet -->\n')
    return '\n'.join(liste)


KAUSWAHL_ITEM_SKELETON = ''' 
    <div style="float:left;max-width:9em;">
        <form action="/ti/do/managepost/config" method="post">
            <input name="paerchen_parameter" value="kat_select" type="hidden">
            <input name="paerchen_wert" value="{kcode}" type="hidden">
            <input name="csrftoken" value="{csrftoken}" type="hidden">
            <input {stil} value="{kname}" type="submit" title="{title}">
        </form>
    </div>'''
KAUSWAHL_SELECTED_STIL = ''' style="background-color:green; font-weight:bold; color:white;" '''


def config_dialog(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    csrftoken=ti_ctx.set_csrftoken('config')
    kats = konst.get_configkategorien(ti_ctx.cnf.get_db_config_bool('flag_turboplan', False))
    # kats ist ein Array aus Tupeln (kontextname, kontextcode, config_struct), ggf. inkl. Turbo
    katselects = ti_ctx.cnf.get_db_config('kat_select', "all")
    
    # ### Auswahl der anzuzeigenden Kontexte
    kauswahl = []
    kauswahl.append(KAUSWAHL_ITEM_SKELETON.format(kcode="all",
                                                  kname=_("Alle"),
                                                  csrftoken=csrftoken,
                                                  stil=KAUSWAHL_SELECTED_STIL if katselects == "all" else "",
                                                  title=_("Alle Kontexte im Überblick")))
    plananzeigentitel = _("Plananzeigen")
    for kname, kcode, cstruct in kats:
        if kcode == 's_any':  # Zeilenwechsel für Plananzeigen!
            kauswahl.append(KAUSWAHL_ITEM_SKELETON.format(kcode="rights",
                            kname=_("Benutzer"),
                            csrftoken=csrftoken,
                            stil=KAUSWAHL_SELECTED_STIL if katselects == "rights" else "",
                            title=_("Benutzerverwaltung")))
            kauswahl.append(f'''
                <div style="clear:left"> </div>
                <div class="condensed" style="float:left;margin-top:3px;">{plananzeigentitel}:</div>''')
        if kname.startswith(plananzeigentitel.split()[0][:-2]):
            kname2 = kname.split(" ", 1)[-1]  # [len(plananzeigentitel):]
        else:
            kname2 = kname
        if len(kname2) > 16:
            kname2 = kname2[:15]+"."
        kauswahl_item_class = "" if len(kname2) < 8 else ''' class="condensed" '''
        kauswahl.append(
            KAUSWAHL_ITEM_SKELETON.format(
                kcode=kcode,
                kname=kname2,
                csrftoken=csrftoken,
                stil=kauswahl_item_class + (KAUSWAHL_SELECTED_STIL if katselects == kcode else ""),
                title=kname)
        )
    md_response.append_s(f'''
        <div class="kategorie" style="background-color: lightgrey;text-align:left;">\n
            <h3>{_("Kontextwahl")}:</h3> {"".join(kauswahl)}</div>''')
    
    kategoriekastendefinition = '''\t<div class="kategorie">\n'''
    for kname, kcode, cstruct in kats:
        if katselects != "all" and katselects != kcode:
            continue
        if kcode == 's_any' and katselects == "all":  # Willkürlicher Wechsel in die nächsten man_details...
            md_response.next_abschnitt()
        md_response.append_s(kategoriekastendefinition)
        md_response.append_s(f'\t\t<h3>{_("Kontext")}: {kname}</h3>\n')
        cstruct.sort()
        for parameter, chelp, cstd in cstruct:
            if parameter.startswith('!INFO'):
                md_response.append_s(f'''
                \n\t\t\t<div style="float:left;width:35em;text-align:justify;">{chelp}</div>
                \n\t\t\t<div class="unfloatleft"></div>''')
                continue
            if parameter.startswith('+'):
                eingabelaenge = 25
                maximallaenge = 250
                parameter = parameter[1:]
            else:
                eingabelaenge = 10
                maximallaenge = 50
            
            helpstd = cstd  # Standardwert, wie er in der Hilfe angezeigt werden soll
            # Behandlung von Flags/boolschen Werten
            if parameter.lower()[:5] == 'flag_':
                anzeigeparameter = parameter[5:]
                cstandard = ti_lib.strg2bool(cstd)
                # klar auf nur "True" oder "False" setzen...
            # Behandlung mit Radiobuttons
            elif parameter.lower()[:2] == 'r_':
                anzeigeparameter = parameter[2:]
                cstandard = cstd.split()[0]
                helpstd = cstandard
                # klar auf nur "True" oder "False" setzen...
            else:
                anzeigeparameter = parameter
                cstandard = cstd
                # jeder Parameter, der erklärt wird, soll auch angelegt sein:

            chelp += f'''<br>{_('Standardwert')}: { helpstd if helpstd else '&lt;'+_('keiner')+'&gt;' }'''
            
            wert = ti_ctx.cnf.get_db_config(parameter, 'keinWert!')
            if wert == 'keinWert!':
                ti_ctx.cnf.set_db_config(parameter, cstandard) 
                wert = ti_ctx.cnf.get_db_config(parameter)  # muss jetzt klappen    
                
            if anzeigeparameter.startswith('s_any_pdf_'):
                anzeigeparameter = anzeigeparameter[10:]
            elif anzeigeparameter.startswith('s_any_'):
                anzeigeparameter = anzeigeparameter[6:]
            elif anzeigeparameter.startswith('s_turbo_'):
                anzeigeparameter = anzeigeparameter[8:]
            
            md_response.append_s(f'''
            <div class="inputdiv">
                <form action="/ti/do/managepost/config" method="post" >
                    {anzeigeparameter}
                    <input name="paerchen_parameter" value="{parameter}" type="hidden">''')
            if parameter.lower().startswith('flag_'):
                checked = " checked=checked " if ti_lib.strg2bool(wert) else ""
                md_response.append_s(f'''
                    <input name="paerchen_wert"  type="checkbox" {checked} />''')
            elif parameter.lower().startswith("r_"):
                stdwerte = cstd.split()
                radioinput = ""
                for einwert in stdwerte:
                    radiocheck = 'checked="checked"' if einwert == wert else ""
                    radioinput += f'''<input name="paerchen_wert" value="{einwert}" type="radio" {radiocheck}>{einwert} '''
                md_response.append_s(radioinput)
            else:
                md_response.append_s(f'''
                    <input name="paerchen_wert" value="{wert}" 
                    type="text" size="{str(eingabelaenge)}" 
                    maxlength="{str(maximallaenge)}"  class="eingabe" />''')
            md_response.append_s('''
                    <input name="csrftoken" value="{csrftoken}" type="hidden"><input value="OK" type="submit">
                </form>
            </div>
            <div class="buttonsdiv">
                <form action="/ti/do/managepost/config" method="post" >
                    <input name="paerchen_parameter" value="{parameter}" type="hidden" />
                    <input name="paerchen_wert" value="{cstandard}" type="hidden" />
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="{standardknopf}" type="submit">
                    {poph}
                </form>
            </div>'''.format(csrftoken=csrftoken, 
                             parameter=parameter, 
                             cstandard=cstandard, 
                             standardknopf=_("Standard"), 
                             poph=popuphilfe(parameter, chelp)))
            
            md_response.append_s('<div class="unfloatleft"></div>')
            
        md_response.append_s('</div>')
        
    # # Benutzerverwaltung
    if katselects == "all" or katselects == "rights":
            
        if katselects == "all":
            md_response.next_abschnitt()
        md_response.append_s(kategoriekastendefinition)
        md_response.append_s(f'''<h3>{_("Kontext")}: {_('Benutzerverwaltung')}</h3>\n''')
        import login
        alleuser = ti_ctx.ses.get_userlist()
        ti_ctx.res.debug('AlleUser:', str(alleuser))
        for name, rechte in alleuser:
            ti_ctx.res.debug('name und rechte', name + '#' + str(rechte))
            rechtenamenhilfe = 'Aktueller Stand:<br><br>'
            for i in range(16):
                rn, rk = login.get_rechte_bez(i)
                if rn != " ":
                    rechtenamenhilfe += rk + ': ' + rn + ' ' + \
                                        ('erlaubt' if rechte & login.get_rechte_nr(i) else 
                                         'verboten') + '<br>'
        
            md_response.append_s('<div style="float:left;width:6em;text-align:right;">')       
            md_response.append_s(name + '&nbsp;&nbsp;')
            md_response.append_s('</div>')
            
            md_response.append_s(htmloptionenzeile(rechte, login.get_rechte_bez, 
                                                   '/ti/do/managepost/config', 'rechtetoggle', 'username', 
                                                   name, csrftoken))
            md_response.append_s('<div style="float:left;">')       
            md_response.append_s(popuphilfe('Benutzerrechte', rechtenamenhilfe))
            md_response.append_s('</div>')
            
            if name != 'vianetwork':
                # # Passwort zurücksetzen
                md_response.append_s(f'''<div style="float:left;">
                <form action="/ti/do/managepost/config" method="post" >&nbsp;&nbsp;
                <input name="user2reset" value="{name}" type="hidden" />
                <input name="csrftoken" value="{csrftoken}" type="hidden" />
                <input value="PW" type="submit" />
                </form></div>''')
                # # User löschen
                md_response.append_s('<div style="float:left;">')       
                md_response.append_s('<form action="/ti/do/managepost/config" method="post" >&nbsp;&nbsp;')
                md_response.append_s('<input name="user2delete" value="' + name +
                                     '" type="hidden" />')
                md_response.append_s('<input name="csrftoken" value="' + csrftoken +
                                     '" type="hidden" />')
                md_response.append_s('''<input value="Del" type="submit" 
                    style="background-color:red;color:white;font-weight:bold;" />
                </form></div>
                <div style="float:left;">''')
                md_response.append_s(popuphilfe('Passwort rücksetzen und User löschen', '''
                Das Passwort wird auf den Namen "tabula.info" zurückgesetzt. <br>
                Ein User, der nicht mehr benötigt wird, kann hier ohne Rückfrage gelöscht werden!'''))
                md_response.append_s('</div>')
            else:
                md_response.append_s('''<div style="float:left;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
                <div style="float:left;">''')       
                md_response.append_s(popuphilfe('kein Passwort, kein Löschen', '''Die Berechtigung
                wird in der ti-Basiskonfiguration über das "ManagementNetwork" geregelt. 
                Hier wird nur festgelegt, welche Rechte so ein Benutzer erhält. 
                Diese Rechte kumulieren mit seinen durch Anmeldung nachgewiesenen Rechten.'''))
                md_response.append_s('</div>')
                
            md_response.append_s('<div class="unfloatleft"></div>')

        md_response.append_s('''<div style="float:left;">
        <form action="/ti/do/managepost/config" method="post" >
        Neuer User: 
        <input name="newuser" value="neuername" type="text" size="10" maxlength="16"  class="eingabe"/>''')
        md_response.append_s('<input name="csrftoken" value="' + csrftoken + 
                             '" type="hidden"><input value="Anlegen" type="submit">')
        md_response.append_s('</form>')
        md_response.append_s('</div><div class="unfloatleft"></div>')
            
        md_response.append_s('</div>')  # schließt Benutzerverwaltungs-kategorie

    # Abschluss mit Anleitung
    md_response.next_abschnitt()
    md_response.append_s('''
        <div class="kategorie" style="text-align:justify;color:blue;float:right;">
            <h5>Anleitung:</h5>
            <p>Jede einzelne Eingabemöglichkeit hat ihre eigene Hilfe.<br>
            Es wird jeweils ein typischer Standardwert vorgeschlagen<br>
            Jede einzelne Änderung (außer i.d. Benutzerverwaltung) muss mit Klick auf "OK" o.ä. abgeschickt werden!</p>
            <p>Bei Problemen können Sie eine <a href="../debugfile" target="_blank">Datei</a> 
            mit Informationen zu Ihrem System erstellen lassen. Diese dann in einer Mail an den Programmautor
            im Anhang versenden.</p>
        </div><!--Ende Anleitungskasten-->
''')
    return md_response
    

def popuphilfe(titel, hilfetext):
    return f'''
            <div class="tooltip">
                <span class="fragezeichen"></span><span class="tooltiptext"><b>{titel}</b><br>{hilfetext}</span>
            </div>'''
    
    
def handle_locale_change(ti_ctx, wert):
    ti_ctx.res.debug("locale change bemerkt")
    import paths
    if wert in 'de_DE en_US da_DK':
        paths.modify_ti_ini(ti_ctx.cnf.paths.get_data_path(), "DEFAULT", "locale", wert)
        return '<br>' + _('Änderung ist beim nächsten Seitenaufruf sichtbar.')
    return ""
        

trigger = {"R_locale": handle_locale_change}
    
    
def config_todo(ti_ctx):
    eapp = ""
    if ti_ctx.check_csrftoken('config'):
        # Ab hier werden die Eingaben verarbeitet
        eapp = ti_ctx.req.get_value('paerchen_parameter')
        triggerinfo = ''
        if len(eapp) > 0:
            eapw = ti_ctx.req.get_value('paerchen_wert')
            if eapp.startswith('s_any_') or eapp.startswith('flag_s_any_'):
                ti_ctx.cnf.set_db_config('s_any_force_convert', 1)
                ti_ctx.cnf.set_db_config('background_timeout', 0)
            if eapp[:5].lower() == 'flag_':
                ti_ctx.res.debug('eapp=eapw', eapp, eapw)
                if ti_lib.strg2bool(eapw):
                    ti_ctx.cnf.set_db_config(eapp, "True")
                    return '+Eingeschaltet wurde: ' + eapp
                else:
                    ti_ctx.cnf.set_db_config(eapp, "False")
                    return '+Ausgeschaltet wurde: ' + eapp
            else:
                ti_ctx.cnf.set_db_config(eapp, eapw)
            if eapp in trigger:
                triggerinfo = trigger[eapp](ti_ctx, eapw)

            return '+Gesetzt wurde: ' + eapp + '="' + eapw + '"' + triggerinfo
        username = ti_ctx.req.get_value('username')
        if len(username) > 0:
            rechtetoggle = ti_ctx.req.get_value_int('rechtetoggle')
            if rechtetoggle > 0 and rechtetoggle < 65536:
                rechte = ti_ctx.ses.get_userrechte(username)
                rechte = rechte ^ rechtetoggle  # xor
                ti_ctx.ses.set_userrechte(username, rechte)
                return '+Rechte geändert'
            else:
                return '-keine Rechte geändert'
        user2reset = ti_ctx.req.get_value('user2reset')
        if len(user2reset) > 0:
            ti_ctx.ses.reset_userpasswd(user2reset)
            return '+Passwort von ' + user2reset + ' wurde zurückgesetzt'
        user2delete = ti_ctx.req.get_value('user2delete')
        if len(user2delete) > 0:
            ti_ctx.ses.remove_dbuser(user2delete)
            return '+Der User ' + user2delete + ' wurde gelöscht'
        newuser = ti_ctx.req.get_value('newuser')
        if len(newuser) > 0:
            ti_ctx.ses.adduser(newuser)
            return '+' + newuser + ' wurde mit Standardpasswort angelegt'
        # Sonst...
        return '-Kein Parameter übergeben'
        # Abbruch
    return ''  # wurde garnicht mit Formular aufgerufen oder csrftoken gefälscht
    
    
def make_debugfile(ti_ctx):
    import subprocess
    import ti_lib
    datei = ['''<!DOCTYPE html>\n<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="content-type">
    </head>
    <body>
    <h1>Debuginformationen</h1>
    ''', version.TI_BRANDING]
    data_path = ti_ctx.paths.get_data_path()
    basepath = ti_ctx.paths.get_basepath()

    files = [('ti.ini', 'yellow'),
             ('log/background.log', 'lightgreen'), 
             ('log/background.log.old', 'lightyellow')]
    commands = [('date', ""), 
                ('tree -d ' + data_path.as_posix(), ""), 
                ('ls -Rhaltn ' + data_path.as_posix(), "data-rechte"),
                ('ls -Rhaltn -I.git ' + basepath.as_posix(), "base-rechte"),
                ('hostnamectl', ""),
                ('ls /etc/*-release', "releases"),
                ('cat /etc/*-release', "release-file"),
                ('locale -a', "locales inst."),
                ('locale', "")
                ]

    linkliste = '<b>GoTo:</b><br>Dateien: '
    for i in range(len(files)):
        linkliste += f'<a href="#file{i}">{i}. {files[i][0]}</a> '
    linkliste += '<br>Befehle: '
    for i in range(len(commands)):
        if commands[i][1]:
            linkliste += f'<a href="#command{i}">{i}. {commands[i][1]}</a> '
        else:
            linkliste += f'<a href="#command{i}">{i}. {commands[i][0].split()[0]}</a> '
    linkliste += '<br>Und die <a href="#dbentries">DB</a>\n'
    
    for count, (filename, color) in enumerate(files):
        chunk = [f'''<h2 style="background-color:{color};" id="file{count}">Datei: {data_path / filename}</h2>\n
            <pre style="background-color:{color};">''']
        try:
            with (data_path / filename).open(mode='r', encoding='utf-8') as fin:
                for line in fin:
                    chunk.append(line)
            datei.append(linkliste)
            datei.append(''.join(chunk) + '\n</pre>')
        except Exception as e:
            datei.append('Datei konnte nicht gelesen werden<br>' + 
                         '<br>\n'.join(chunk) + '\n</pre>\n' + ti_lib.beschreibe_exception(e))
                         
    for count, (command, comment) in enumerate(commands):
        if comment:
            comment = '"' + comment + '" '
        chunk = [f'''<h2 style="background-color:#DDF;" id="command{count}">Befehl: {comment}{command}</h2>\n<pre>''']
        try:
            stdout = subprocess.check_output(command, shell=True, 
                                             stderr=subprocess.STDOUT).decode("utf-8", "ignore")
            for line in stdout.split("\n"):
                line = line.replace(' ', '&nbsp;')
                chunk.append(line)
            datei.append(linkliste)
            datei.append('\n'.join(chunk) + '\n</pre>')   
        except Exception as e:
            import ti_lib
            datei.append('Befehl konnte nicht ausgeführt werden<br>' + 
                         '<br>\n'.join(chunk) + '\n</pre>' + ti_lib.beschreibe_exception(e))

    dump = ti_ctx.cnf.dump_db_config()
    datei.append(linkliste)
    chunk = ['<h2 id="dbentries">DATENBANK: Konfigurationseinträge</h2>\n']
    chunk.append('<table>')
    for line in dump:
        chunk.append('<tr><td>{}</td><td>{}</td></tr>'.format(line[0], line[1]))
    chunk.append('</table>')
    datei.append('\n'.join(chunk))   
    datei.append(linkliste)
    datei.append("\n ** Das war's! Viel Erfolg beim Debuggen...")
    return "<br>\n<br>\n ============================================================== <br>\n<br>\n".join(datei)
    
    
def do_config(ti_ctx):
    if not ti_ctx.check_management_rights(konst.RECHTEAdministration):
        return [], -1

    debugfileflag = ti_ctx.req.get_value('debugfile')
    # ti_ctx.res.debug('debugfile', debugfileflag)
    if debugfileflag.lower() == 'true':
        df = make_debugfile(ti_ctx).encode('utf-8')
        sys.stdout.buffer.write(b'Content-Type: application/octet-stream\n')
        sys.stdout.buffer.write(b'Content-Disposition: attachment;filename=config.html.xz\n\n')
        sys.stdout.buffer.write(lzma.compress(df))
        exit(0)
    else:
        erfolg = config_todo(ti_ctx)
        ti_ctx.res.debug("Erfolg", erfolg)
        if erfolg:
            ti_ctx.set_erfolgsmeldung(erfolg)
        return config_dialog(ti_ctx)


def debugfile(ti_ctx):
    if not ti_ctx.check_management_rights(konst.RECHTEAdministration):
        return ''
    df = make_debugfile(ti_ctx).encode('utf-8')
    return lzma.compress(df, preset=8)
    
    
if __name__ == '__main__':
    ti_lib.quickabort2frames()
