'''
________________________________________________________________________
 s_turbo_userinput.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_userinput.py nimmt POSTs entgegen und 
    trägt sinnvolle Eingaben in eine Datenbank ein. 
    
    Außerdem liefert es an s_turbo auf Anfrage die Info, 
    in welchen Kommentarfeldern Markierungen eingeblendet werden sollen (AiM)
________________________________________________________________________
'''

# Batteries
import sqlite3
from random import randint

# TI
# import ti
# import ti_lib

""" 
Die Datenbank enthält zwei Tabellen:
    UserCodes: person, code, failed, locktime
    UserInput: tag, stunde, person, markierung
    
"""


class TurboUInput_DB:
    
    def __init__(self, ti_paths):

        # Verbindung zur Datenbank herstellen
        try:
            self.tui_conn = sqlite3.connect(ti_paths.get_data_path('turbouidb.sq3'))
            tui_cursor = self.tui_conn.cursor()
            # Standarddefinitionen, auf die "gebaut" werden kann - nicht mehr ändern!!
            tui_cursor.execute('''CREATE TABLE IF NOT EXISTS UserInput 
                                        (   tidate TEXT, 
                                            stunde INTEGER DEFAULT 0, 
                                            person TEXT, 
                                            markierung INTEGER DEFAULT 0,
                                            von TEXT,
                                            primary key (tidate, stunde, person)
                                            ) ''')
            tui_cursor.execute('''CREATE TABLE IF NOT EXISTS UserCodes
                                        (   person TEXT, 
                                            code TEXT,
                                            failed INTEGER DEFAULT 0,
                                            locktime INTEGER DEFAULT 0,
                                            primary key (person)
                                            ) ''')
            self.tui_conn.commit()

        except Exception as e:
            import ti2
            ti2.panic(_('Keine Verbindung zur Turbo-User-Input-Datenbank turbouidb.sq3. Rechte falsch gesetzt?'), e, 
                      'basepath:' + self.paths.get_basepath().as_posix() + 'data_path:' + 
                      self.paths.get_data_path().as_posix() + '<br>username:' + self.paths.username)
        
        welcomelist = ["admin", "stupla", "StoJ"]
        for name in welcomelist:
            self.set_user(name)
        
    def get_tui_cursor(self):
        return self.tui_conn.cursor()

    def get_markierung(self, tidate, stunde, person):
        tui_cursor = self.get_tui_cursor()
        tui_cursor.execute('SELECT markierung, von FROM UserInput WHERE tidate=? AND stunde=? AND person=?',
                           (tidate, stunde, person))
        markierung, von = 0, ""
        for zeile in tui_cursor:
            markierung, von = zeile
        return markierung, von

    def set_markierung(self, tidate, stunde, person, markierung, vonperson):
        tui_cursor = self.get_tui_cursor()
        tui_cursor.execute('INSERT OR REPLACE INTO UserInput (tidate, stunde, person, markierung, von) VALUES (?, ?, ?, ?, ?)',
                           (tidate, stunde, person, markierung, vonperson))
        self.tui_conn.commit()

    def get_user(self, person):
        tui_cursor = self.get_tui_cursor()
        tui_cursor.execute('SELECT code, failed, locktime FROM UserCodes WHERE person = ?', (person, ))
        for zeile in tui_cursor:
            return zeile
        return "", 0, 0

    def set_user(self, person, force_newcode=False):
        if len(person) < 4:
            return
        co, fa, lo = self.get_user(person)
        if co and not force_newcode:
            return
        tui_cursor = self.get_tui_cursor()
        
        if person not in ["admin", "stupla"]:
            code = f'{random_pair()}-{random_triple()}-{random_pair()}'
        else:
            code = f'{random_pair()}{random_triple()}{random_pair()}{random_pair()}'
        tui_cursor.execute('INSERT OR REPLACE INTO UserCodes (person, code) VALUES (?, ?)',
                           (person, code))
        self.tui_conn.commit()

    def set_userfailed(self, person, newfailed, newlocktime):
        # db_cursor.execute('UPDATE clients SET gruppen=? , istconf=1 WHERE ip=?', (gruppen, ip))
        tui_cursor = self.get_tui_cursor()
        tui_cursor.execute('UPDATE UserCodes SET failed = ?, locktime = ? WHERE person = ?',
                           (newfailed, newlocktime, person))
        self.tui_conn.commit()


def random_chars(count, fullrange=False):
    char_range = "34679ACEFGHKLMNPQRTUVWXYÄÖÜ&$*?+!%#"
    last = len(char_range)-1 if fullrange else 23
    result = ""
    for i in range(count):
        result += char_range[randint(0, last)]
    return result
    
    
def random_pair():
    return random_chars(2)


def random_triple():
    return random_chars(3, fullrange=True)
    
