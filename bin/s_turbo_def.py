'''
________________________________________________________________________
 s_turbo_def.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_def.py definiert die Klassen, die beim Verwalten des
  Vertretungsplans eingelesen vom Programm "Turbo Vertretung" benötigt werden. 
 Die entsprechenden Objekte werden in import_turbo.py angelegt
  und in schedule.py angezeigt
________________________________________________________________________
'''
# Batteries
# (nix)
# TI
import ti

# !! diese Datei wird nicht internationalisiert, da kga-spezifisch !!


COLUMN_NAMES =  ["Std", "KlNr.", "Kl.", "Fach", "Plan", "Kurs", "Vertret.", "Raum", "Bem.", "XXX"]
        # Liste mit 2 Arrays, die für jede Spalte ein Flag haben, 
        # ob die Spalte dargestellt werden soll
        #   Erstes Array für normale Darstellung, 
        #   Zweites für Veröffentlichung
COLUMN_DISPLAY=[[True,  False, True,  True,  True,  False, True,  True,  True,  False], 
                [True,  False, False, False, False, True,  True,  True,  True,  False]]
        # Liste mit 2 Arrays, die für jede Spalte ein Flag haben usw.
COLUMN_BOLD=   [[False, False, True,  False, False, False, False, False, False, False], 
                [False, False, True,  False, False, False, False, False, False, False]]
        # Liste mit 2 Arrays, die für jede Spalte ein Flag haben usw.
COLUMN_SMALL=  [[False, False, False, False, False, False, False, False, True,  False], 
                [False, False, False, False, False, False, False, False, True,  False]]

CLASS_ARRAY = ["", "s_t_fresh", "s_t_ausfall", "s_t_gelb", "s_t_gruen"]
PAUSENNAME = ['Früh-', '', '1.<small> Pause</small>', '', '2.<small> Pause</small>',
              '', 'Mittagsp.', '', '', '', 'Nachm.p.', '', '', '', '']

'''Definition der Datenstruktur
 
 Um (mindestens) zwei Varianten bei gleicher Datengrundlage zu produzieren
 wird die Sichtbarkeit und Darstellung der Spalten über Flags gesteuert, 
 welche in je (mindestens) zwei Arrays abgelegt sind:
 Array 0 für normale Bildschirmdarstellung, 
 Array 1 für Veröffentlichung im Internet
 
 
'''
# Präfix für Kommentare - wenn es welche gibt
COMMENT_HEAD = ['', 'Früher: ', '']


def debug(*args):
    # ## FIXME ti_lib.log("s_turbo:", *args)
    pass


class S_Schedule(object):
    ''' Objekt, das einen vollständigen Vertretungsplan definiert'''
    def __init__(self):
        self.days = []            # Array mit 1-5 S_Day-Objekten  
        self.first5days = []      # Liste mit day-objects (S_Day) für die folgenden 5 Tage
        self.timestamp = 0        # klassischer timestamp, ignoriert solange 0
        
    def set_timestamp(self, ts):
        self.timestamp = ts
        
    def add_day(self, s_day):
        self.days.append(s_day)
    
    def analyze_days(self):
        '''Selbstanalyse, 
            erst aufzurufen, wenn komplett gefüllt - 
            aber frisch vor der Darstellung aufrufen 
            (wg. des Verkleinerns abgelaufener Stunden)'''
        for day in self.days:
            day.analyze_rows()
        
        self.first5days = []
        
        for i in range(30):                                 # 30 Tage durchlaufen
            iday = ti.get_following_worktidate(i)      # Datum des jetzt+i-ten Werktages berechnen
            found = False
            for day in self.days:                           # alle Tage im S_Obj abklappern
                if day.get_date() == iday:                  # wenn er das oben berechnete Datum hat
                    found = True
                    the_day = day  # nur eine referenz
            if found:
                self.first5days.append(the_day)
                if debuglevel > 0:
                    debug("zu first5days addiert:", the_day.get_date())
                if i > 1:
                    self.first5days[-1].mark_as_plan()
                if len(self.first5days) == 5:
                    break
            
    def get_first5days(self):
        return self.first5days


class S_Day(object):            # Definition eines Tages im Vertretungsplan
    ''' Objekt, das einen Tag im Vertretungsplan definiert'''
    def __init__(self, tidate):
        self.date = tidate        # tidate, für den dieses Objekt gilt
        self.datestring = ti.get_string_from_tidate(tidate, short=True)
        self.datestringshort = ti.get_string_from_tidate(tidate, veryshort=True)
        self.rows = []            # Liste mit Zeilenobjekten vom Typ S_Row
        self.comment = ['', '', '']  # Drei Kommentartexte für den ganzen Tag
        if debuglevel > 0:
            debug("New S_Day:", str(tidate))
        
    def add_row(self, s_row):
        append_flag = True
        
        for row in self.rows:
            if append_flag and abs(s_row.expire-row.expire) == 1:
                test_flag = True  # voller Hoffnung
                for own, new in zip(row.meine_zellen(include_stunde=False), s_row.meine_zellen(include_stunde=False)):
                    if own != new:
                        test_flag = False
                
                if test_flag:  # identisch
                    row.ergaenzeStunde(s_row.getStunde())
                    append_flag = False  # Arbeit abgeschlossen
                
        if append_flag:
            self.rows.append(s_row)
        
    def add_comment(self, comment, level=0):  # Kommentare in 100-102 in csv als 0-2 in level
        global debuglevel
        debug("Setze Kommentar , level", (comment, level))
        
        if level and self.comment[level]:
            self.comment[level] += ' |&nbsp;'
        self.comment[level] += comment
        if debuglevel > 1:
            debug("Setze Kommentar im level", (comment, level))
        
    def analyze_rows(self):
        '''Selbstanalyse. Aufruf erst wenn vollständig gefüllt!
        Errechnet Variablen, die mit get_rowmedian usw. ausgelesen werden'''
        global debuglevel
        # Gesamthöhe = 0.6 * kleinere Zeilen + volle Zeilen
        # halbe Höhe = Gesamthöhe/2
        # benötigte Zeilen für halbe Höhe:
        #  halbe Höhe/0, 6 ergibt kleine Zeilen-Anzahl
        #  wenn genug kleine Zeilen -> Lösung
        #  wenn zu wenig kleine Zeilen:
        #   fehlende Höhe = halbe höhe - 0, 6*vorhandene kleine zeilen
        #   zusätzliche (volle) Zeilen= fehlende höhe :-)
        # Analog für Drittel...
        akthour = ti.get_akt_hour(self.date)  # liefert -1 wenn nicht "heute"
        all2display = list(range(1, 16))
        self.rows.sort(key=lambda zeile: zeile.sortvalue)
        row_count = 0
        small_row_count = 0
        for row in self.rows:
            try:
                row_hour = row.get_expire()
                if row_hour in all2display:
                    row_count += 1 if row_hour > akthour else 0.6
                    small_row_count += 0 if row_hour > akthour else 1
            except Exception:
                pass  # ignore 
        for l_i in (0, 1):
            row_count += (len(self.comment[l_i].split('<BR>'))*4.0)/5.0
            # grobe Schätzung, dass jeder Absatz ca. 4/5-Zeilen lang ist

        median = (row_count/2)/0.52
        drittel = (row_count/3)/0.52
        zweidrittel = (row_count*2/3)/0.52
        if median > small_row_count:
            median = ((row_count/2.0)-0.52*small_row_count)+small_row_count
        if drittel > small_row_count:
            drittel = ((row_count/3.0)-0.52*small_row_count)+small_row_count
        if zweidrittel > small_row_count:
            zweidrittel = ((row_count*2.0/3.0)-0.52*small_row_count)+small_row_count

        self.rowcount = int(row_count+0.5)
        self.rowmedian = int(median+0.5)
        self.rowthird = int(drittel+0.5)
        self.rowtwothird = int(zweidrittel+0.5)
        if debuglevel > 0:
            debug("Analysierte Rows für:", 
                  str(self.date) + "(=" + ti.get_string_from_tidate(self.date) + ") C:" +
                  str(self.rowcount) + "M:" + str(self.rowmedian))
        if debuglevel > 0:
            debug(" Analys.Rows: median:", str(self.rowmedian)+" Thrd:"+str(self.rowthird)+" 2Thrd:"+str(self.rowtwothird))
    
    def get_rowcount(self):     
        return self.rowcount

    def get_rowmedian(self):    
        return self.rowmedian

    def get_rowthird(self):     
        return self.rowthird

    def get_rowtwothird(self):  
        return self.rowtwothird

    def get_date(self):         
        return self.date

    def get_datestring(self):   
        return self.datestring

    def get_datestringshort(self):  
        return self.datestringshort

    def get_comment(self, level):
        return (COMMENT_HEAD[level]+self.comment[level] if self.comment[level] else '')
        
    def mark_as_plan(self):
        self.datestring += ' (geplant)'


class S_Row(object):
    ''' Objekt, das eine Zeile im Vertretungsplan definiert'''
    global debuglevel

    def __init__(self, rowout, highlight=0, sortvalue=0, ist_stunde=True, neuflag=0):
        self.rowout = rowout        # Array mit allen Zellen der Zeile
        self.expire = rowout.stunde_int     # Letzte Unterrichtsstunde, in der diese Zeile gezeigt wird
        #  (redundante Information, da wohl in irgendeiner Spalte enthalten)
        self.highlight = highlight
        # red, green, yellow?
        # Definition:0: Nix, also schwarze Schrift auf weiß
        #            1: Rote Schrift auf weiß
        #            2: Grüne Schrift auf weiß
        #            3: Gelber Hintergrund mit schwarzer Schrift
        #            4: Grüner Hintergrund mit schwarzer Schrift
        self.ist_stunde = rowout.ist_stunde  # wird für Pausenaufsichten o.ä. auf False gesetzt 2022-01-20
        self.neuflag = neuflag
        
        self.sortvalue = sortvalue if sortvalue > 0 else expire*100  # da sortvalue vereinfacht stunde*100 ist
        
        self.not_if_reduced = ["Aufsicht", "Pr1", "Pr2", "Pr3"]
        
        if debuglevel > 1:
            debug("New Sortvalue:", self.sortvalue)

    def get_expire(self):
        return self.expire

    def get_highlight(self):
        return self.highlight

    def get_neuflag(self):
        return self.neuflag

    def meine_zellen(self, include_stunde=True):
        if include_stunde:
            yield self.rowout.stunde
        yield "120466"  # fake Klassennummer
        yield self.rowout.klassen
        yield self.rowout.fach
        yield self.rowout.ausfall
        yield self.rowout.key
        yield self.rowout.einsatz
        yield self.rowout.raum
        yield self.rowout.kommentar
        
    def getStunde(self):
        return self.rowout.stunde
        
    def ergaenzeStunde(self, wert):
        self.rowout.stunde += "+" + wert
        
    def reducedOK(self, reduced):
        if not reduced:
            return True
        if not self.ist_stunde:
            return False
        for rule in self.not_if_reduced:
            if self.rowout.fach.startswith(rule):
                return False
        return True
        
    def row2htmlTR(self, abgelaufen, reduced, fatborder, lightborder, pause_gefunden, level):
        newstyle = ''
        newclass = ''
        prioclass = ''
        output = []
        
        if not self.ist_stunde:  # also z.B. eine Pausenaufsicht
            if not pause_gefunden:
                fatborder = True
                pause_gefunden = True
            newclass += ' italic '
            self.rowout.stunde = PAUSENNAME[self.rowout.stunde_int]
            if self.rowout.fach.startswith('Pause'):
                self.rowout.fach = 'Aufsicht'
        else:
            pause_gefunden = False
            if "prä" in self.rowout.klassen:
                newclass += ' italic '
                
        if abgelaufen:
            output.append('\t<tr class="abgelaufen">')
        else:
            output.append('\t<tr>')
            if self.get_highlight():
                newclass += CLASS_ARRAY[self.get_highlight()]
            if self.get_neuflag():
                prioclass = ' s_t_fresh '
        self.rowout.kommentar = self.rowout.kommentar.split('**')[0]
        if fatborder:
            fatborder = False
            lightborder = False
            newstyle += 'border-top-width:5px;'

        if lightborder:
            lightborder = False
            newstyle += 'border-top-width:3px;'

        if not (reduced and pause_gefunden):
            for i, zelle in enumerate(self.meine_zellen()):
                # Zellenanfang
                tdstr = '<td '
                if i > 0 or not pause_gefunden:
                    tar = ''
                else:
                    tar = 'text-align:right'
                if (len(newclass) or len(prioclass)) and i >= (1 if self.ist_stunde else 0):
                    if i and i < 7:
                        tdstr += f' class="{newclass} {prioclass}" '
                    else:
                        tdstr += f' class="{newclass}" '
                if i == 4:
                    tdstr += self.rowout.uidata
                if len(newstyle+tar): 
                    tdstr += f' style="{newstyle+tar}" '
                # Zellenende
                if get_column_display(level, i):
                    output.append(tdstr + get_column_style(level, i) + '>' + str(zelle) + '</td>')
            # Zeilenende        
            output.append('</tr>\n')
        return "".join(output), fatborder, lightborder, pause_gefunden


def get_column_names(column):
    return COLUMN_NAMES[column]


def get_column_display(level, column):
    return COLUMN_DISPLAY[level][column]


def get_column_style(level, column):
    if COLUMN_BOLD[level][column]:
        return "bigright=true"
    if COLUMN_SMALL[level][column]:
        return "smaller=true"
    return ""


debuglevel = 1
