'''
________________________________________________________________________
 persons.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 persons.py definiert die Datenstruktur P_Call und gibt ansonsten
 eine Liste der zu rufenden Schüler als HTML-Code zurück
________________________________________________________________________
'''
# Batteries

# TI
import ti_lib


class P_Call(object):
    def __init__(self, rowid, longname, target, timestamp):
        self.rowid = rowid
        self.longname = longname
        self.target = target
        self.timestamp = int(timestamp)
        
    def get_rowid(self):
        return self.rowid
        
    def get_true_longname(self):
        return self.longname
    
    def get_visible_longname(self, Flag_psns_caps):
        c, n = self.get_visible_class_name(Flag_psns_caps)
        return f'{c}:{n}'
    
    def get_visible_class_name(self, Flag_psns_caps):
        vislongname = self.longname if not self.longname.startswith("0") else self.longname[1:]
        pklasse, pname = vislongname.split(':', 1)
        if Flag_psns_caps:         # Automatische Groß/Kleinschrift erzeugen und als Kapitälchen anzeigen - alles in CSS
            pnamenteile = pname.split(maxsplit=1)
            pn_sep = ' '
            if len(pnamenteile) == 1:
                pnamenteile = pname.split(',', 1)
                pn_sep = ', '
            if len(pnamenteile) == 1:
                pnamenteile.append("")
                pn_sep = ""
            pname = f'<span class="capitalized_caps">{pnamenteile[0]}</span>{pn_sep}{pnamenteile[1]}'
        return pklasse, pname
        
    def get_target(self):
        return self.target
    
    def get_timestamp(self):
        return self.timestamp
        
    def get_age_hours(self, timestamp):
        return (timestamp-self.timestamp)//3600


def iterate_persons(ti_ctx, reverse_flag=False):
    po_array = pdb_get_persons(ti_ctx)
    Flag_psns_caps = ti_ctx.cnf.get_db_config_bool('Flag_psns_caps')
    itmess = []            
    gk_state = 0
    if (len(po_array) > 0):
        itmess.append(f'''
        <div class="persons blueborder"><h3><u>{_('Personenruf')} {
            ' (höchste Klasse zuerst)' if reverse_flag else ''
            }:</u></h3> \n\t<table cellspacing="0" width="auto">\n''')
        
        timestamp_now = ti_ctx.sys.get_unix_time() 
        bewegung = '➜'
        if reverse_flag:
            po_array.reverse()
        oldpklasse = ""
        oldplistklasse = ""
        plistclass = ""
        for p in po_array:
            agehours = p.get_age_hours(timestamp_now)
            
            farbe = ''
                
            if agehours < 2:                          # grün für Frischlinge
                farbe = 'green'
            elif agehours > 24:                       # Rot einfärben nach 24 h
                farbe = 'red'
            elif agehours > 8 and ti_lib.get_akt_hour() >= 9:  # Orange einfärben ab 9 Uhr wenn nicht weniger als 8 h alt
                farbe = 'gelb'
            else:
                farbe = 'blue'
            pklasse, pname = p.get_visible_class_name(Flag_psns_caps)
            if pklasse != oldpklasse:
                oldpklasse = pklasse
                oldplistclass = plistclass
                plistclass = 'personlisteneueklasse'
                spekulatius = 'platzhalter1'
                if gk_state in (1, 3): 
                    itmess[len(itmess) - 1] = itmess[len(itmess)-1]. replace('platzhalter2', 'gleiche_klasse_bottom')
                    gk_state = (gk_state + 1) % 4
            else:
                oldplistclass = plistclass
                plistclass = 'personlistegleicheklasse'
                if gk_state == 0:
                    gk_state = 1
                    gk_class = 'gleicheklasse_a'
                elif gk_state == 2:
                    gk_state = 3
                    gk_class = 'gleicheklasse_b'
                itmess[len(itmess) - 1] = itmess[len(itmess)-1]. replace('platzhalter1', gk_class)
                if oldplistclass != plistclass:
                    itmess[len(itmess) - 1] = itmess[len(itmess)-1]. replace('platzhalter2', 'gleiche_klasse_top')
                spekulatius = gk_class
            pimpedklasse = ti_lib.compress_output(pklasse, 3, 1)
            if pimpedklasse:
                pimpedklasse += ':'
            pimpedtarget = ti_lib.compress_output(p.get_target(), 11)
            itmess.append(f'''
            <tr>
                <td nobord="true" class="{plistclass}" style="text-align:right;width:4em;">
                    <span alt_person="{farbe}" class="{spekulatius} platzhalter2" {gk_state}>
                        {pimpedklasse}
                    </span></td>
                <td nobord="true" class="{plistclass}">
                    <span alt_person="{farbe}">
                        {pname}
                    </span>&nbsp;
                    {bewegung} {pimpedtarget} </td>
            </tr>\n''')
        if gk_state in (1, 3): 
            itmess[len(itmess) - 1] = itmess[len(itmess)-1]. replace('platzhalter2', 'gleiche_klasse_bottom')
        itmess.append('\t</table>')
        itmess.append('</div>\n')
        
        # End of iterating persons
        return ''.join(itmess)
    return ''
    

def pdb_get_persons(ti_ctx):
    '''gibt alle zu den Parametern passenden Personen als Objekt-Array zurück'''
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""
            SELECT ROWID, longname, target, timestamp
            FROM persons
            ORDER BY longname""")
    po_array = []
    for row in cursor:
        rowid, longname, target, timestamp = row
        pobj = P_Call(rowid, longname, target, timestamp)
        po_array.append(pobj)
    cursor.close()
    return po_array


def pdb_get_longname(ti_ctx, rowid):
    '''gibt die gerufene Person einer rowid aus, strippt doofe Zeichen raus'''
    longname = ''
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""
            SELECT longname
            FROM persons
            WHERE ROWID=?""", (rowid, ))
    for row in cursor:
        longname, = row
    cursor.close()
    return longname.replace("(", "").replace(")", "").replace("'", "").replace(":", "").replace(", ", "")


def pdb_write_person(ti_ctx, longname, target, timestamp):
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""INSERT INTO persons
                    (longname, target, timestamp) 
                    VALUES (?, ?, ?)""", 
                   (longname, target, timestamp))
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()
    

def pdb_delete_person(ti_ctx, rowid):
    longname = pdb_get_longname(ti_ctx, rowid)
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""DELETE FROM persons WHERE rowid=?""", (rowid, ))
    anzahl = cursor.rowcount
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()
    if anzahl == 1:
        return f'+Es wurde ein Personenruf für {longname} gelöscht'
    return '+Es wurden '+str(anzahl)+' Personenrufe gelöscht'


if __name__ == '__main__':
    ti_lib.quickabort2frames()
