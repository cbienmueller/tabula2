'''
________________________________________________________________________
 s_turbo_import.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_import.py liest aus der Datei vplan.csv die Informationen um 
 die Datenstrukturen gemäß def_schedule.py aufzubauen. schedule.py kann
 die Daten dann in HTML anzeigen lassen.
 * Diese Datei muss für jedes Programm zu Vertretungsplanung *
 *  bzw. dessen Exportdatei angepasst werden.                *
________________________________________________________________________
'''
# Batteries
import csv
from dataclasses import dataclass, astuple

# TI
import ti
import ti_lib
import s_turbo_def 
import s_turbo_userinput_db

# Keiner hat behauptet, dass die folgende Datei leicht zu lesen ist
# Sie ist speziell auf das KGA abgestimmt
# !! diese Datei wird nicht internationalisiert, da kga-spezifisch !!


UI_MARKIERUNG = ['', 
                 '<img src="/ti/static/pix/mebis_logo.png"><span style="color:#10a8d1"'
                 ' title="Arbeitsaufträge\nin Mebis"> A.i.M.</span> ',
                 '<img src="/ti/static/pix/abs.png"><span style="color:#10a8d1;"'
                 ' title="Arbeitsaufträge\nbei Schüler*innen"> A.b.S.</span> '
                 ]
        
KOMMENTARERSETZUNG = [("Zusammenlegung", 
                       '<img src="/ti/static/pix/2rings.png"><span class="condensed"> Zusammen­legung</span> '),
                      ("geänderter Raum!",
                       '<img src="/ti/static/pix/2arrows.png"><span class="condensed"> Raumänderung!</span> '),
                      ("Stillarbeit",
                       '<img src="/ti/static/pix/silence.png"><span class="condensed"> Stillarbeit</span> '),
                      ("!", "! "), 
                      ("! !", "!!"),
                      ("  ", " "),
                      ("AiM",
                       UI_MARKIERUNG[1])]
                       
KLASSENERSETZUNG = [("senz", ".")]

# Die folgenden Strings sind Schlüsselwörter, damit die sie enthaltenden Zeilen im Internet nicht angezeigt werden! 
AUSSCHLUESSE_BEI_REDUZIERT = ["krank:", "dbf.:", "fortbildung:", "intern:", "dbf:", "(1)", "(2)", "(3)",
                              "(4)", "(5)", "(6)", "(7)", "(8)", "(9)", "mutterschutz"]


LEHRERERSETZUNG = {"Debes Carla": "Debes Carla", "Debes Carolin": "Debes Carol."}


@dataclass
class TurboRowIn:
    datum:      str = ""                                             # noqa: E241
    stundestrg: str = ""  # mit Punkt nach Zahl                      # noqa: E241
    stunde:     str = ""  # als float                                # noqa: E241
    klassen:    str = ""  # auch mehrere als space-getrennte Liste   # noqa: E241
    fach:       str = ""  # betroffenes Fach                         # noqa: E241
    ausfall:    str = ""  # Kürzel der ausfallenden Lehrkraft        # noqa: E241
    einsatz:    str = ""  # Name der eingesetzten Lehrkraft          # noqa: E241
    raum:       str = ""  # Raumbezeichnung                          # noqa: E241
    kommentar:  str = ""                                             # noqa: E241
    neuflag:    str = "0"  # Flag für kurzfristige Änderung          # noqa: E241
    typ:        str = ""                                             # noqa: E241
        
        
@dataclass
class TurboRowOut:
    stunde:     str = ""    # als Zahl                               # noqa: E241
    klassennr:  int = 0                                              # noqa: E241
    klassen:    str = ""    # komplett HTML formatiert               # noqa: E241
    fach:       str = ""    # betroffenes Fach                       # noqa: E241
    ausfall:    str = ""    # Kürzel der ausfallenden Lehrkraft      # noqa: E241
    key:        str = ""    # Spezial für Seminarnamen               # noqa: E241
    einsatz:    str = ""    # Name der eingesetzten Lehrkraft        # noqa: E241
    raum:       str = ""    # Raumbezeichnung                        # noqa: E241
    kommentar:  str = ""    # finaler Kommentar                      # noqa: E241
    ist_stunde: bool = True  # Standard: Unterrichtsstunden          # noqa: E241
    uidata:     str = ""    # enthält data-Attribute um gezielt Userinput zu holen  # noqa: E241
    
    
def debug(*args):
    # ## FIXME - ti_lib.log("s_turbo:", *args)
    pass


def cleanteacher(lehrer):
    lehrer = lehrer.replace(', ', ' ').strip()
    if not lehrer:
        return ''
    while '  ' in lehrer:
        lehrer = lehrer.replace('  ', ' ')
    if lehrer in LEHRERERSETZUNG:
        return LEHRERERSETZUNG[lehrer]
    lelist = lehrer.split(' ')
    if len(lelist) > 1:
        if lelist[1] == 'Dr.' and len(lelist) > 2:
            # return f'{lelist[0]} Dr. {lelist[2][0]}.'     # mit oder ohne Dr.
            return f'{lelist[0]} {lelist[2][0]}.'
        return lelist[0] + ' ' + lelist[1][0] + '.'
    if lehrer.startswith('---'):
        return '-->'
    return lehrer


def customize_table(import_table, timestamp, tuidb):
    '''Hier findet die Anpassung der csv-Datei des Stundenplan-Programms zur Darstellung in tabula.info statt'''
    global schedule_obj
    global debugging
    lastday = 0
    
    # alle zeilen bearbeiten
    for raw_row in import_table:
        row_in = TurboRowIn(*raw_row)
        
        row_out = TurboRowOut() 
        # jetzt die Fallunterscheidungen für die verschiedenen Spalten
        if debugging:
            debug("TurboRow als Objekt", row_in)
            debug("TurboRow als Tupel", astuple(row_in))
            debug("TurboRow als Liste", list(astuple(row_in)))
        if row_in.datum[0].isdigit():
            # Datum, wird hier in das tabula.info-eigene Format konvertiert wird und 
            #  für die Festlegung des zu verwendenden day_obj herangezogen:
            day = ti.get_tidate_from_string(row_in.datum)
            if debugging: 
                debug("Zeile für Tag ", ti.get_string_from_tidate(day) + " aus " + row_in.datum)
            if lastday != day:
                if lastday != 0:
                    schedule_obj.add_day(day_obj)
                day_obj = s_turbo_def.S_Day(day)
                lastday = day
            # Unterrichtsstunde
            if ',' in row_in.stunde:
                row_out.stunde = row_in.stunde.split(',')[0]  # lästiges Kommaformat für Pausen ausblenden...
                row_out.ist_stunde = False
            else:
                row_out.stunde = row_in.stunde
            if row_out.stunde.isdigit():
                row_out.stunde_int = int(row_out.stunde)
            else:
                row_out.stunde_int = 0
                
            # betroffenen Klassen
            #  -> kürze deren Bezeichnungen
            #    (dabei ist Klassenliste eine Liste aller betroffenen Klassen von Zeile)
            # Annahme: alle Klassen kommen aus einer Klassenstufe und enden mit einem Buchstaben
            for ist, soll in KLASSENERSETZUNG:
                row_in.klassen = row_in.klassen.replace(ist, soll)
                
            row_in.klassen = row_in.klassen.lower()
            klassencondensed = True if len(row_in.klassen) > 3 else False          
            klassennr = 0
            if (len(row_in.klassen) < 2 or row_in.klassen[0].isdigit() or row_in.klassen[1].isdigit()):
                klassenliste = row_in.klassen.split()
                klassenchars = []
                
                for eineklasse in klassenliste:
                    # aus "8a 8b" die 8 extrahieren 
                    if klassennr < 1:
                        klassennr = ti_lib.strg2firstint(eineklasse, 0)
                    if eineklasse[-1].isalpha():
                        klassenchars.append(eineklasse[-1])
                    
                if row_out.ist_stunde and klassennr:
                    if row_in.klassen.startswith("["):
                        klaus = "[" + str(klassennr) + "]"
                    else:
                        klaus = str(klassennr) + '+'.join(klassenchars)
                else:
                    klaus = ' '.join(klassenliste)
            else:
                klaus = row_in.klassen
            row_out.klassennr = klassennr
            if klassencondensed:
                row_out.klassen = '<span class="condensed">' + klaus + '</span>'
            else:
                row_out.klassen = klaus
            
            # Spalte 4 enthält das betroffene Fach und wird gekürzt
            # if '11' in row_in.fach:
            fach = row_in.fach.split()
            condensed_ab = -1
            for i in range(len(fach)):
                if fach[i].startswith('NATT'):
                    fach[i] = 'NuT'
                elif i and len(fach[i]) > 7:  # kürze nicht erstes Fach-Element
                    fach[i] = fach[i][:7] + '.'
                    if condensed_ab < 0:
                        condensed_ab = i
                elif fach[i] == 'statt' and condensed_ab < 0:
                    condensed_ab = i
            if condensed_ab < 0:      
                row_out.fach = ' '.join(fach)
            else:
                row_out.fach = ti_lib.combine_with_condensed(" ".join(fach[:condensed_ab]),
                                                             " " + " ".join(fach[condensed_ab:]))
            
            # Spalte 5 enthält den zu vertretenden Lehrer als Kürzel
            row_out.ausfall = row_in.ausfall
            # keyfach = row_in.fach[:5]  # 
            keyfach = row_in.fach  # Rette Seminarbezeichnungen
            if keyfach.startswith("SPK"):
                keyfach = "spo" + keyfach[3:]
            key = row_in.klassen + "_" + keyfach
            key = key.lower()
            if row_in.klassen in ['11', '12']:
                row_out.fach = keyfach.lower()
                row_out.key = key
            else:
                row_out.key = row_out.klassen + ": " + row_out.fach
            if row_in.klassen in ['11', '12'] and debugging:
                debug('danach row3,4:', row_in.klassen,
                      row_in.fach, 'row_out3,4,5:',
                      row_out.fach, row_out.ausfall, row_out.key)

            # Spalte 6 enthält den vertretenden Lehrer im Vollnamen.
            # Der Vorname wird abgekürzt, wobei Dr. erkannt wird
            row_out.einsatz = ti_lib.compress_output(cleanteacher(row_in.einsatz), 8, 4)
            
            # Spalte 7 enthält die Raumbezeichnung
            if row_out.ist_stunde:  # "-" in row_in.raum and
                row_out.raum = ti_lib.compress_output(row_in.raum, 3)
            else:
                row_out.raum = row_in.raum
                        
            # Spalte 8 enthält einen Kommentar (ggf. zum ganzen Tag) 
            #  und wird zu einem HTML-String mit Zeilenvorschub zusammengefügt
            commentline = row_in.kommentar
            # ti_debug("commentline", commentline)
            # ti_debug("commentline0-22", commentline[:22])
            highlight = 0 
            if row_out.stunde_int < 100:
                # für die Kommentar-Markierung - nicht für Pausen :-)
                if row_out.ist_stunde:
                    ui_markierung, ui_von = tuidb.get_markierung(tidate=day, stunde=row_out.stunde_int, person=row_out.ausfall)
                else:
                    ui_markierung, ui_von = 0, ""
                    
                # für das klickbare Feld "fehlende Lehrkraft"
                if row_out.klassennr > 0:
                    row_out.uidata = f' data-uitidate="{day}" data-uistunde="{row_out.stunde_int}"' \
                                     f' data-uiperson="{row_out.ausfall}" onclick="tui_dialog()" '
                else:
                    row_out.uidata = ""
                    
                commentlen = len(commentline.strip())
                for ist, soll in KOMMENTARERSETZUNG:
                    commentline = commentline.replace(ist, soll)
                commentline = eliminate_bracket_redundance(commentline)
                commentline = commentline.replace('\\n', ' ')
                if commentline.find('statt') >= 0:
                    st_vor, st_rest = commentline.split("statt", 1)
                    if "," in st_rest:
                        st_rest, st_ende = st_rest.split(",", 1)
                        st_rest += ", "
                    else:
                        st_ende = ""
                    commentline = '{}<span class="italic">statt{}</span>{}'.format(st_vor, st_rest, st_ende)
                if commentline.find('**') >= 0:   # Flag für neue Einträge
                    if commentline.find('**gr') >= 0:  # grün
                        highlight = 4
                    elif commentline.find('**ge') >= 0:  # gelb
                        highlight = 3
                    elif commentline.find('**ro') >= 0:  # rot
                        highlight = 1
                    else:
                        highlight = 1
                    commentline = commentline.split("**")[0]

                if "fällt aus" in commentline:
                    if not highlight:
                        highlight = 2  # grüne Schrift
                    if commentline.startswith("fällt aus") and not row_in.einsatz:
                        vgz_ort = commentline.find("vorgezogen")
                        if vgz_ort >= 0:
                            row_out.einsatz = f'''<span class="condensed" style="float:right;">(vorgez. {
                                              commentline[vgz_ort + 10:]})</span>'''
                            commentline = 'fällt aus'
                        
                if commentlen > 20 or "fällt aus" in commentline:
                    commentline = f'''<span class="condensed">{commentline}</span>'''
                    
                if "Raumänderung" in commentline:
                    row_out.raum = f'<span class="turboraum">{row_out.raum}</span>'
                    if len(commentline.split("Raumänderung")[1]) > 12:
                        commentline = commentline.replace("Raumänderung", "Raum")
                if ui_markierung > 0 and ui_markierung < len(UI_MARKIERUNG):
                    commentline += UI_MARKIERUNG[ui_markierung]
            
            elif row_out.stunde_int in (100, 101, 102): 
                if not row_in.neuflag.startswith("0"):
                    highlight = 1  # Haneke-Markierung für neue Einträge
                
                # ab hier für einen Tageskommentar (100) vertretende Lehrer (101) oder fehlende Lehrer (102)
                comment = commentline.split('\\n')
                for i in range(len(comment)):
                    if len(comment[i].strip()) == 0:
                        comment[i] = '<!--feed-->'
                    else:
                        if comment[i][0] == ' ':
                            comment[i] = '      ' + comment[i].lstrip()
                        elif comment[i][:3] == '---':
                            comment[i] = ' --> '
                        comment[i] = "&nbsp;&nbsp;&nbsp;".join(comment[i].rstrip().split("   "))
                if len(comment) > 1 and row_out.stunde_int == 100:
                    commentline = ""
                    firstbold = True
                    dekofettunterstrichen = ' style="text-decoration:underline; font-weight:bold;"'
                    dekofett = ' style="font-weight:bold;"'
                    dekorot = 'class="s_t_fresh"'
                    dekogelb = 'class="s_t_gelb"'
                    dekogruen = 'class="s_t_gruen"'
                    for i in range(0, len(comment)):
                        deko = ''
                        if comment[i].find("**") >= 0:
                            if comment[i].find("**gr") >= 0:
                                deko = dekogruen
                                comment[i] = comment[i].replace("**gr", "")
                            elif comment[i].find("**ge") >= 0:
                                deko = dekogelb
                                comment[i] = comment[i].replace("**ge", "")
                            else:
                                deko = dekorot
                                comment[i] = comment[i].replace("**", "")
                        
                        doppel = comment[i].split(":", 1)
                        if len(doppel) == 2:
                            if firstbold:
                                comment[i] = '<span ' + deko + dekofettunterstrichen + '>' + doppel[0] + ':</span>'
                                if deko:
                                    comment[i] += '<span ' + deko + '>' + doppel[1] + '</span>'
                                else:
                                    comment[i] += doppel[1]
                                firstbold = False
                            else:
                                comment[i] = '<span ' + deko + dekofett + '>' + doppel[0] + ':</span>'
                                if deko:
                                    comment[i] += '<span ' + deko + '>' + doppel[1] + '</span>'
                                else:
                                    comment[i] += doppel[1]
                        elif deko:
                            comment[i] = '<span ' + deko + '>' + comment[i] + "</span>"
                        if comment[i] == '<!--feed-->':
                            firstbold = True
                        if i > 0:
                            if comment[i] != '<!--feed-->' or comment[i - 1] != '<!--feed-->':
                                commentline += '<BR>' + comment[i]
                        else:
                            commentline = comment[0]
                elif len(comment) and row_out.stunde_int == 101:
                    comment = commentline.split(' ')
                    commentstunde = ""
                    if comment[-1].strip().startswith('('):
                        commentstunde = comment[-1]
                        comment.pop(-1)
                    commentline = (cleanteacher(" ".join(comment)).replace(" ", "&nbsp;") +
                                   '<small>&nbsp;' + commentstunde + '</small>')
                    debug("101-Kommentar", commentline)
                else:
                    commentline = comment[0]
            row_out.kommentar = commentline
            
            if debugging:
                debug("s_turbo_import", row_out)
            if int(row_out.stunde) < 100:
                day_obj.add_row(s_turbo_def.S_Row(row_out, 
                                                  highlight=highlight,
                                                  sortvalue=(int(row_out.stunde) * 100 +
                                                             (50 if not row_out.ist_stunde else 0) +
                                                             int(row_out.klassennr)),
                                                  neuflag=0 if row_in.neuflag.startswith("0") else 1))
            else:
                day_obj.add_comment(commentline, row_out.stunde_int - 100)
            
    if lastday != 0:
        schedule_obj.add_day(day_obj)
    # krönender Abschluss:
    schedule_obj.set_timestamp(timestamp)
    schedule_obj.analyze_days()
                

def eliminate_bracket_redundance(line):
    '''Wenn in Klammern ein Text kommt, 
        der bereits im Text davor enthalten ist,
        so wird der mit den Klammern entfernt'''
    if "(" in line:
        left, rest = line.split("(", 1)
        if ")" in rest:
            inside, right = rest.split(")", 1)
            if inside in left or inside in right:
                return f'{left} {right}'
    return line


def read_table(ti_paths, filename='', timestamp=0):
    global table_gelesen
    global debugging
    
    if debugging: 
        ti_paths.log("s_t_i.read_table startet")
    if table_gelesen:
        if debugging:
            debug(f'read_table für {filename} wurde erneut aufgerufen und daher abgebrochen')
        return
    try:
        pathundname = ti_paths.get_data_path("tmp/vplan.csv") if not filename else filename
        reader = csv.reader(pathundname.open(mode="r", encoding='iso-8859-1'), delimiter="|")
        import_table = []
        for row in reader:
            import_table.append(row)
    
    except Exception:
        ti.prt('''<H1>I/O-Fehler - Plan konnte nicht eingelesen werden</H1>
            Evtl. wurde noch kein Vertretungsplan hochgeladen oder<br>
             er hat falsche Leseberechtigungen?''')
        exit()
    if debugging: 
        ti_paths.log(f"s_t_i.read_table hat {len(import_table)} Zeilen eingelesen:")
    tuidb = s_turbo_userinput_db.TurboUInput_DB(ti_paths)
    customize_table(import_table, timestamp, tuidb)
    table_gelesen = True

        
def get_schedule_obj():
    return schedule_obj
    
# ## fixme - hier muss noch einmal das Multitasking/Multithreading geklärt werden... 2023-05-28
#            Vermutung: dies wird nur zum Einlesen über background verwendet, daher frei von Multi-something! 2024-02-25


schedule_obj = s_turbo_def.S_Schedule()


debugging = True

table_gelesen = False


