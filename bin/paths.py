'''
________________________________________________________________________
 paths.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul stellt nur die fast gleichnamige Klasse zur Verfügung,
     welche alle relevanten Pfade ermittelt, zur Verfügung stellt und 
     damit z.B. ein Logfile pflegt.
________________________________________________________________________

'''

# Batteries (included)
import sys
import os
import pwd
import time
import locale
import configparser
import gettext
from pathlib import Path

# TI-Import
import konst
import ti_lib

PST_OK = 0           # erfolgreich initialisiert
PST_ERROR = -1       # Fehler ist aufgetreten und steht in meldung
PST_UNKNOWN = 1      # unbekannte site wurde gewählt


class TI_Paths():
    def __init__(self, site="local"):
        """ init legt die ehemaligen globalen Variablen an.
            Diese werden in einem Objekt der Klasse TI_Paths zurückgegeben,
            welches auch eine Rückmeldung mit Level (0 ok, 1 site existiert nicht, -1 fatal error - abort) enthält.
        
            site ist auch der Verzeichnisname neben "local", welcher für andere siten die Daten hält.
        """
        self.status = PST_OK
        self.meldung = ""

        if sys.version_info < (3, 7):
            self.status = PST_ERROR
            self.meldung = "tabula.info benötigt mindestens Python 3.7 oder neuer!"
            self.execption = BaseException("Python version too old. 3.7 or newer required.")
            return

        # # Zuerst absoluten Pfad die tabula.info-Installation feststellen
        # basepath ist das Verzeichnis, in dem bin und andere Dateien von t.i liegen, also /opt/tabula.info/tabula2.
        # varpath ist ab 2.1 der Pfad /var/opt/tabula.info für alle Daten
        
        self.basepath = Path(__file__).absolute().parent.parent   # ergibt /opt/tabula.info/tabula2
        self.varpath = Path('/var/opt/tabula.info/')
        self.rampath = Path('/run/tabula.info/')

        # Ohne DATA fäuft nichts
        self.data_path = (self.varpath / 'datasite' / site).resolve()  # ist wg. multihoming dynamisch
        if not (self.data_path).exists():  # Fallback ist single-home-Variante!
            site = 'local'
            self.data_path = (self.varpath / 'datasite' / site).resolve() 
        self.site = site
        if not (self.data_path).exists():
            self.status = PST_UNKNOWN
            self.meldung = "site existiert nicht!"  # nun aber wirlich nichts...
            return
        
        # Pfad zum temporären Verzeichnis (ramdisk) für z.B. session-Datenbank - gleichzeitig Versionscheck...
        self.temp_path = (self.rampath / site).resolve()
        try:
            ensure_path_exists(self.temp_path)
        except Exception:
            import ti2
            ti2.panic('Es fehlt die neue systemd-Integration (/run).<br>'
                      'Bitte das Installationsskript ab 2.1 aufrufen!', 
                      BaseException("/run/tabula.info muss existieren und beschreibbar sein"))
        
        # Nun werden die weiteren Pfade unter Nutzung des Werts von site berechnet
        # Pfade im Filesystem / Disk
        self.dyn_fs_path = (self.varpath / 'web' / site / 'dyn').resolve() 
        ensure_path_exists(self.dyn_fs_path)
        self.local_fs_path = (self.varpath / 'web' / site / 'local').resolve() 
        ensure_path_exists(self.local_fs_path)
        
        for pfad in ['upload', 'work', 'download', 'log', 'tmp']:
            ensure_path_exists(self.data_path / pfad)
                
        for pfad in ['cal']:
            ensure_path_exists(self.temp_path / pfad)
            
        for pfad in ['info']:
            ensure_path_exists(self.dyn_fs_path / pfad)
                
        # Pfade auf dem Webserver
        self.dyn_web_route = f'/ti/web/{site}/dyn'
        self.local_web_route = f'/ti/web/{site}/local'
        
        self.username = pwd.getpwuid(os.getuid())[0]
        if self.username == 'www-data':
            logfilename = 'background.log'
        else:
            logfilename = self.username + '.log'
        self.logfile_path = self.data_path / 'log' / logfilename
        self.logfile_old_path = self.data_path / 'log' / (logfilename + '.old')
                        
        ti_ini = read_ti_ini(self.data_path)
        
        self.thelocale = ti_ini['DEFAULT']['locale']
        if self.thelocale and self.thelocale != 'de_DE':
            try:
                translate = gettext.translation('tabula', self.basepath / 'locales',
                                                languages=[self.thelocale], fallback=False)
            except Exception:
                sys.stderr.write('\nti_lib.py: locale NOT set to '+self.thelocale+'\n')
                sys.stderr.flush()
                self.thelocale = 'de_DE'
                translate = gettext.translation('tabula', self.basepath / 'locales', 
                                                languages=['de_DE'], fallback=True)  # fallback ist das relevante...
        else:
            translate = gettext.translation('tabula', self.basepath / 'locales',
                                            languages=['de_DE'], fallback=True)  # fallback ist das relevante...
        translate.install()

        try:
            locale.setlocale(locale.LC_ALL, (self.thelocale, 'UTF8'))   
        except Exception:
            try:
                locale.setlocale(locale.LC_ALL, ('de_DE', "utf-8"))  # nur um sicher zu gehen...
            except Exception:
                import ti2
                ti2.panic(f'Kann weder auf gewählte Sprach-Einstellungen {ti_paths.thelocale}, noch de_DE schalten', 
                          BaseException(f"Es muss Linux mit locale {ti_paths.thelocale}-UTF installiert sein!"))
            
        # for pfad in ['dyn', 'local']:
        #    if not (self.dyn_fs_path / pfad).exists():
        #        (self.dyn_fs_path / pfad).mkdir()
            
        # if False:  # except Exception as e:
        #    self.status = PST_ERROR
        #    self.meldung = str(e)
        
    def reset_log(self):
        self.log('\n'+'#'*73+'\n# '+_('Dieses Logfile wird hiermit abgeschlossen, umbenannt und neu angelegt')+' #\n'+'#'*73)
        try:
            self.logfile_old_path.unlink()
        except Exception:
            pass
        self.logfile_path.rename(self.logfile_old_path)
        self.log(_('Neues Logfile nach Reset'))

    def log(self, *args):
        items = []
        for i in args:
            items.append(str(i))
        try:
            with self.logfile_path.open(mode='a', encoding='utf-8') as fout:
                fout.write(time.strftime('%d %b %Y %H:%M:%S', time.localtime())+': '+' '.join(items)+'\n')
        except Exception as e:
            panic(f"ti_lib failed on writing logfile {self.logfile_path}", e)

    # # basepath: Ort des bin-Verzeichnisses, also parent des Ortes, wo diese Datei liegt. Der
    # #           Rest basiert darauf

    def get_basepath(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return self.basepath / parameter

    def get_binpath(self, ):  # nach Definition...
        """gibt pathlib.Path-Objekt zurück"""
        return self.basepath / 'bin'

    def get_toolpath(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return Path(konst.TOOLPATH) / parameter

    def get_data_path(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return self.data_path / parameter

    def get_dyn_path(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return self.dyn_fs_path / parameter 

    def get_local_path(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return self.local_fs_path / parameter 

    def get_temp_path(self, parameter=''):
        """gibt pathlib.Path-Objekt zurück"""
        return self.temp_path / parameter

    def get_dyn_url(self, parameter=''):
        """gibt STRING zurück!"""
        if parameter:
            return f'{self.dyn_web_route}/{parameter}'
        else:
            return self.dyn_web_route

    def get_local_url(self, parameter=''):
        """gibt STRING zurück!"""
        if parameter:
            return f'{self.local_web_route}/{parameter}'
        else:
            return self.local_web_route

    ##########################################
    # standardhtml-head erzeugen

    def standardcss(self, ismanagement=False, til='', with_js=False):
        the_end = "backend" if ismanagement else "frontend" 
        the_js = '\n\t<script src="/ti/static/js/ti.js" type="text/javascript">' \
                 '</script><!-- by paths.py -->\n ' if with_js else ""
        if not til:
            til = int(time.time() / 1000)  # ca. die 20-min-Intervalle seit 1970... Ändert sich also alle ca. 20 min

        if self.get_local_path(f'{the_end}.css').is_file():
            localcss = f'''<link rel="stylesheet" type="text/css" href="{
                       self.get_local_url( f'{the_end}.css')
                       }?q={til}">'''
        else:
            localcss = ''

        return f'''
        <link rel="stylesheet" type="text/css" href="/ti/static/css/all_end.css?q={til}">
        <link rel="stylesheet" type="text/css" href="/ti/static/css/{the_end}.css?q={til}">
        {localcss}
        {the_js}
    ''' 


def ensure_path_exists(path):
    if not path.exists():
        path.mkdir(parents=True, exist_ok=True)
    
    
def write_ti_ini(data_path, ti_ini):
    """Schreibe eine ini-Datei in das Datenverzeichnis
    
    ti_ini muss vom Typ ConfigParser sein."""
    with (data_path / 'ti.ini').open(mode='w') as inifile:
        ti_ini.write(inifile)


def modify_ti_ini(data_path, section, parameter, value):
    ti_ini = read_ti_ini(data_path)
    ti_ini[section][parameter] = value
    write_ti_ini(data_path, ti_ini)


def read_ti_ini(data_path):
    """Hier wird die ini-Datei eingelesen - zeitlich deutlich vor sqlite-Configs"""
    ti_ini = configparser.ConfigParser()
    ti_ini.read(data_path / 'ti.ini')
    # eigentlich fertig. Nun noch Spezialfälle wie migration
    if not ti_ini['DEFAULT']:
        ti_ini['DEFAULT'] = {'locale': ''}
        print("default neu angelegt")
        write_ti_ini(data_path, ti_ini)
    if not ti_ini['DEFAULT'].get('locale'):
        _thelocale_ = "#undefined"
        try:
            with (data_path / 'locale.conf').open(mode='r', encoding='utf-8') as localedatei:
                while _thelocale_ and not _thelocale_[0].isalpha():
                    _thelocale_ = localedatei.readline().strip()
            if _thelocale_ and _thelocale_[0].isalpha():
                ti_ini['DEFAULT']['locale'] = _thelocale_
            (data_path / 'locale.conf').unlink()
        except Exception:
            ti_ini['DEFAULT']['locale'] = 'de_DE'
        write_ti_ini(data_path, ti_ini)
    return ti_ini


if __name__ == '__main__':
    ti_lib.quickabort2frames()
