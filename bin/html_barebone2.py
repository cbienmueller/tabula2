'''
________________________________________________________________________
 html_barebone.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul enthält nur statische HTML-Inhalte, die tw. von JS ausgefüllt werden

________________________________________________________________________

'''

PANIC_TEMPLATE = '''<!DOCTYPE html>\n
<head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="300">
    <style>body{{ background:#F00;color:#000}}
        div {{ background:#FFF;color:#F00;font-weight:bold;margin:2em;padding:2em;}}
        /* h3, h2 {{ background:#505;color:#220;padding:1em;}} */
        p, pre {{color:#000;}}
        p {{font-weight:bold;}}
        .meldung {{color:darkgrey;}}
        span {{background:lightgreen;color:yellow;}}
        #branding {{
            font-size:80%; bottom: 150px; right: 100px; 
            box-shadow: 4px 4px 6px grey; width: 12em; position: absolute; }}
        #branding, #branding * {{ 
            text-align: center;  padding:0;  margin:0;  
            color: #668888;  background-color: #ddd;  border-color: #bbb;}}
    </style>
</head>
<body>
    <div><h2>Fatal Error / Nicht behebbarer Fehler:</h3>
        <p><small>in {}</small></p>
        <p>{}</p><br>
        <h3>{}</h3><br>
        <pre class="meldung">{}</pre>
        <pre class="meldung">{}</pre>
        <pre class="meldung">{}</pre>
        <h3>{}</h3><br>
        <pre>{}</pre>
    </div>
    {}
</body>'''

ROOT_FORWARD = '''<!DOCTYPE html>
<head>
    <meta http-equiv="refresh" content="0; URL=/ti/do/frontend">
    <link rel="shortcut icon" type="image/x-icon" href="/ti/static/pix/favicon.ico">
</head>
<body>forwarding...</body>'''

FRONTEND = '''<!doctype html>
<head>
    <title>t.i Anzeige</title>
    <link rel=stylesheet href="/ti/static/css/frontend.css" type=text/css>
    <link rel="shortcut icon" type="image/x-icon" href="/ti/static/pix/favicon.ico">
</head>
<body onload='ti_start()'>
<div id="contentp" class="tiP_Content">&nbsp;1&nbsp;</div>
<div id="contentm" class="tiM_Content">
    <div class="tiM_Status" id="contentmstatus">
        <div class="overlap_right"><span id="ti_time">10</span><span id="alerter">*</span></div>
        <span id="frontstatus"></span>
    </div>
    <div class="tiM_Square" id="square_m0">
        <div id="contentm0">&nbsp;...&nbsp;</div>
    </div>
    <div class="tiM_Square" id="square_m1">
        <div id="contentm1">&nbsp;...&nbsp;</div>
    </div>
</div>
<div style="float:none"> </div>

<div id="bodenvorhang"><div style="float:left" id="contentbottomleft"><h2>
    &nbsp;&nbsp;BOTTOM</h2></div><div style="float:right" id="contentbottomright"><h2>&nbsp;&nbsp;BOTTOM</h2></div></div>

<!-- Ende der visuellen Komponenten -->
<script src="/ti/static/js/jquery.js" charset="utf-8"></script>
<script src="/ti/static/js/ti2.js"     charset="utf-8"></script>
</body>
'''
