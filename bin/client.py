'''
________________________________________________________________________
 client.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 client.py verwaltet zentral 
        die Informationen über den 
        aufrufenden Client
            (Refaktoring 2019 and again 2023)
________________________________________________________________________
'''
from dataclasses import dataclass
import ti_lib


def get_TI_Client(ip_address, ti_cnf, ti_response):
    """ ip_address ist die Info über den Ursprung des   Requests, also interne IP oder extern gelieferte Cookie-ID
        ti_cnf ist das Objekt, das die db-Konfiguration verwaltet
        ti_response das Objekt, das die Response für diesen Request verwaltet.
    """
    
    # check, if entry exists
    ti_client = ti_cnf.client_read_ti_client(ip_address)
    if ti_client:
        ti_cnf.client_ping(ip_address, ti_response)
        return ti_client
        
    # im Folgenden wird ein neuer Eintrag angelegt, wenn es ein interner Request war
    if ti_lib.ip_in_local_networks(ip_address):
        ti_cnf.client_ping(ip_address, ti_response)
        ti_client = ti_cnf.client_read_ti_client(ip_address)
    else:
        ti_cnf.client_ping("Extern", ti_response)
        ti_client = ti_cnf.client_read_ti_client("Extern")
        
    if not ti_client:
        ti_client = TI_Client("extern_"+ip_address, 'extern', 0, 0, 99, 0, False, 0, 0, '')
        ti_response.debug(f'Externer Aufrufer {ip_address}, keinen Client vermerken')

    if not ti_client.gruppen & 256:
        ti_client.forward = '' 
    return ti_client


@dataclass
class TI_Client:  # gemeint ist Hardwareclient, Browser
    ip: str
    name: str
    timestamp: int = 0
    minphase: int = 0
    maxphase: int = 99
    gruppen: int = 0
    istanzg: int = 0
    cukopplung: int = 0
    istconf: int = 0
    forward: str = ""
        
    # liefert Client-spezifische Grenzen für anzuzeigende
    # Pläne, erste Grenze < 0  invertiert Bereich

    def phaselimits(self):
        return self.minphase, self.maxphase

    def groups(self):
        return self.gruppen & 63, self.gruppen >> 6, self.forward
        
    def max_groups_number(self):
        gruppen = self.gruppen & 63
        cgn = 6
        while cgn > 0:
            if gruppen >= 1 << (cgn - 1):
                return cgn
            cgn -= 1
        return 0
        
    def ist_rpi(self):
        return (self.gruppen & 512) > 0
        
    def ist_lehrer(self):
        return self.gruppen & 32
            
    def ist_cukopplung(self):  # Flag für Kopplung von Client und User
        return self.cukopplung

    def ist_in_management_network(self, managementnetwork):
        paar = managementnetwork.split("/", 1)
        if len(paar) > 1:
            network, mask = paar
            return ti_lib.ip_in_network(network, mask, self.ip)
        else:
            return False
    
    def ist_in_internal_network(self):
        return ti_lib.ip_in_local_networks(self.ip)
        
    def get_name(self):
        return self.name


