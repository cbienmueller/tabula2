'''
________________________________________________________________________
 config_clients.py                                                        
 This file is free software under         
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist  Freie Software gemäß
             der GPL ohne jegliche Garantie - siehe die Datei COPYING 
________________________________________________________________________

 config_clients.py vereint den Dialog und die Tätigkeiten
            zur Konfiguration und Überwachung der Clients
________________________________________________________________________
'''

# Batteries
# (nix)
# TI
import ti_lib
import ti
import config


def config_dialog(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    csrftoken = ti_ctx.set_csrftoken('clients')

    sortbyname = ti_ctx.cnf.get_db_config_bool('client_sortbyname')
    sort_bezeichnung = _("Sortierung nach Name") if sortbyname else _("Sortierung nach IP-Adresse")
    
    alleclients = ti_ctx.cnf.client_get_list(sortbyname)  # Liste von TI_Client
    # diese Liste enthält Tupel. 
    # Sortiert ist sie nach istconf, dann nach istanzg, dann entweder nach ip (flase) oder nach name (true)
    
    statemachine = 0  
    # 0: noch keine Überschrift geschrieben, 
    # 1: hat "konfigurierte Anzeigen" geschrieben, 
    # 2: hat "konfigurierte Clients geschrieben, 
    # 3: hat "unkonf. C." geschrieben
    
    for ti_client in alleclients:   # Liste von TI_Client
        # ip, name, timestamp, maxphase, gruppen, istconf, istanzg, cukopplung, forward = tupel
        if statemachine == 0 and ti_client.istconf and ti_client.istanzg:
            md_response.append_s(f'''{CLIENT_DIV_START}
        <h3>{_("Kontext")}: {_("verwaltete Anzeigerechner")} \n
            {config.popuphilfe('Anzeigerechner', 
            """per IP-Adresse identifizierte Clients, welche als öffentlich Anzeigen dienen. 
            Deren Funktion kann überwacht werden. Rufe /ti/do/astatus auf!""")}
            <div style="position: relative;display: inline-block;">
                <form action="/ti/do/managepost/config_clients" method="post">
                    <input name="client_sortbyname" value="toggle" type="hidden">
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="{sort_bezeichnung}" type="submit" title="{_("Sortierung nach Name oder IP")}">
                </form>
            </div>
        </h3>\n''')
            statemachine = 1
        elif statemachine <= 1 and ti_client.istconf and not ti_client.istanzg:
            if statemachine > 0:  # der Vorgänger wurde genutzt
                md_response.append_s('</div><!--Kontext div zu-->\n') 
                md_response.next_abschnitt()
            md_response.append_s(CLIENT_DIV_START)
            md_response.append_s(f'<h3>{_("Kontext")}: {_("verwaltete Clients")}</h3>\n')
            statemachine = 2
        elif statemachine <= 2 and not ti_client.istconf:
            if statemachine > 0:
                md_response.append_s('</div><!--Kontext div zu-->\n')  # ein Vorgänger wurde genutzt </div>
                md_response.next_abschnitt()
            md_response.append_s(CLIENT_DIV_START)
            md_response.append_s(f'<h3>{_("Kontext")}: {_("unkonfigurierte Clients")}\n')
            md_response.append_s(f'''<div style="position: relative;display: inline-block;">
                                        <form action="/ti/do/managepost/config_clients" method="post">
                                            <input name="client_deleteoldz" value="doit" type="hidden">
                                            <input name="csrftoken" value="{csrftoken}" type="hidden">
                                            <input value="{_('Lösche Einträge älter als 2 Wochen') }" type="submit">
                                        </form>
                                    </div></h3>''')
            statemachine = 3  # final
        
        # Anfang einer Clientbeschreibung
        md_response.append_s(f'''
        <div style="float:left;margin-bottom:0;padding:0;">
            <div class="client_ip_blue condensed"><b>{
                ti_lib.readable_ip(ti_client.ip)
            }</b></div>''')
        # Beginn des umfassenden DIVs um die Clientdetails
        md_response.append_s(f'''
        
            <div style="float:left;">
                <form action="/ti/do/managepost/config_clients" method="post" >
                    <input name="clientip" value="{ti_client.ip}" type="hidden" />
                    <input name="newname" value="{ti_client.name}" type="text" size="12" maxlength="16" class="eingabe" />
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="{_("OK")}" type="submit">
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Umbenennen', 
                """Jeder Client erhält beim ersten Zugriff seinen Hostnamen, 
                wenn er im DNS gefunden wird. Dieser kann geändert werden. 
                Er wird z.B. beim Anzeigenstatus angezeigt und bei über den Client 
                legitimierten Meldungen als Autor angegeben""")}
            </div>''')
        
        md_response.append_s(f'''
            <div style="float:left;">
                <form action="/ti/do/managepost/config_clients" method="post" >&nbsp;
                    <input name="ip2remove" value="{ti_client.ip}" type="hidden" />
                    <input name="csrftoken" value="{csrftoken}" type="hidden" />
                    <input value="{_("Löschen")}" type="submit" 
                                    style="background-color:red;color:white;font-weight:bold;" />
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Client löschen', 
                """Der Client wird aus der Datenbank entfernt, 
                   bis er selber wieder auf das System zugreift.""")}
            </div>
        
            <div style="float:left;">       
                <form action="/ti/do/managepost/config_clients" method="post" >&nbsp;
                    <input name="csrftoken" value="{csrftoken}" type="hidden" />
                    <input name="cukopplung_ip" value="{ti_client.ip}" type="hidden" />
                    <input value="Kopplung" type="submit" 
                    {'style="background-color:green;font-weight:bold;"' if ti_client.cukopplung else ''} />
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe('Kopplung', 
                """Ein Anwender, der diesen Client zum Managen benutzt erhält die Rechte des 
                <i>gleichnamigen</i> Users - wenn einer existiert...<br><br>
                Funktioniert aus Sicherheitsgründen nur im ManagedNetwork!""")}
            </div>''')

        md_response.append_s('''
            <div style="float:left;padding:0px;">''')   
        warheute = ti.get_tidate(ti_client.timestamp) == ti.get_todays_tidate()
        langher = ti.get_tidate(ti_client.timestamp) < ti.get_todays_tidate() - 7
        if warheute:
            md_response.append_s('<span style="color:green;font-size:70%;font-weight:bold;line-height:60%;">')
        elif langher:
            md_response.append_s('<span style="color:grey;font-size:70%;line-height:60%;">')
        else:
            md_response.append_s('<span style="color:black;font-size:70%;line-height:60%;">')
        datum, leer, uhrzeit = ti.get_datestring_from_unix_time(ti_client.timestamp).rpartition(' ')
        md_response.append_s(f' &nbsp;{datum}<br>{uhrzeit}')
        md_response.append_s('</span>')
        md_response.append_s('''
            </div>''')
        
        ########################
        # zweite Zeile beginnen
        md_response.append_s('\n\t\t<!--Beginn 2. Zeile-->\n')
        md_response.append_s('''
            <div class="unfloatleft"></div>
            ''')
        if ti_client.istanzg:
            ti_client.gruppen |= 128
        md_response.append_s(config.htmloptionenzeile(
                ti_client.gruppen, ti_ctx.cnf.get_gruppenbez, 
                '/ti/do/managepost/config_clients', 'gruppentoggle', 'clientip', ti_client.ip, 
                csrftoken))
        
        gruppennamenhilfe = 'Clientgruppen:<br><br>'
        for i in range(20):
            gn, gk = ti_ctx.cnf.get_gruppenbez(i)  # GruppenName, GruppenKürzel
            if gn:
                gruppennummer = 1 << i
                if gruppennummer == 64:
                    gruppennamenhilfe += '<br>Eigenschaften:<br>'
                if gruppennummer < 64:
                    gruppennamenhilfe += gk + ': ' + \
                                          ('Zeige Meldungen für ' if ti_client.gruppen & gruppennummer 
                                           else 'Verberge Meldungen für ') + gn + '<br>'
                else:
                    gruppennamenhilfe += gk + ': ' + \
                                          ('Client ist vom Typ ' if ti_client.gruppen & gruppennummer 
                                           else 'Client ist kein  ') + gn + '<br>'
        md_response.append_s('<div style="float:left;">')       
        md_response.append_s(config.popuphilfe('Aktueller Stand:', gruppennamenhilfe))
        md_response.append_s('\t</div>')
        
        # dritte Zeile für Anzeigeneinstellungen
        md_response.append_s('\n\t\t<!--Beginn 3. Zeile-->\n')
        if ti_client.istanzg and not (ti_client.gruppen & 256):
            mipha, mapha = ti_client.minphase, ti_client.maxphase  # ti_ctx.get_client_phaselimits(ti_client.ip)
            checked = ""
            if mipha < 0:
                mipha = - mipha
                checked = " checked=checked "
            md_response.append_s(f'''
            <div class="unfloatleft"></div>
            <div style="float:left;">
                <form action="/ti/do/managepost/config_clients" method="post" >{_('Anzeigebereich')}:
                    <input name="clientip" value="{ti_client.ip}" type="hidden" />
                    <input name="minphase" value="{str(mipha)}" type="text" size="2" maxlength="2" class="eingabe"/> {
                        _('bis')}
                    <input name="maxphase" value="{str(mapha)}" type="text" size="2" maxlength="2" class="eingabe"/> 
                    <input name="phaseintervallinvertiert"      type="checkbox" {checked} /> {
                        _('Bereich umkehren!')}
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="OK" type="submit">
                </form>
            </div>
            <div style="float:left;">
                {config.popuphilfe(_('Anzeigebereich'), _("""Der Bereich startet bei 0 für den Aushang.
                    Alle höheren Nummern stehen für die Pläne.
                    Hiermit kann für diesen Client eingeschränkt werden, was er anzeigen soll.
                    Die angegebenen Grenzen sind inklusiv!
                    Passt ein folgender Plan noch mit auf die Seite des Bereichsendes, so wird er jedoch auch angezeigt!<br>
                    Die Bereichsauswahl kann invertiert werden:
                    Dann werden alle Pläne außer dem festgelegten Bereich angezeigt.
                    Perfekt für zwei Bildschirme nebeneinander, wo der erste einen kleinen Bereich und
                    der andere den Rest dynamisch anzeigt."""))
                }
            </div>
            <div class="unfloatleft"></div>
            ''')
            
        if ti_client.gruppen & 256:  # vierte Zeile einfügen für Eingabe des Forward-Ziels (STATT 3. Zeile!)
            md_response.append_s('\n\t\t<!-- Beginn 4. Zeile-->\n')
            md_response.append_s(f'''
            <div class="unfloatleft"></div>
            <div style="float:left;">
                <form action="/ti/do/managepost/config_clients" method="post" >
                    <input name="clientip" value="{ti_client.ip}" type="hidden" />
                    <input name="forwardziel" value="{ti_client.forward}" type="text" 
                                                                  size="18" maxlength="255"  class="eingabe" />
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="Umleiten des Clients" type="submit">
                </form>
            </div><!-- Ende 4. Zeile-->\n''')
        
        # Ende des umfassenden DIVs um die Clientdetails
        md_response.append_s('''
        </div><!--Ende Clientdetails-->
        <div style="border-bottom:solid 2px grey; margin-bottom:4px;" class="unfloatleft"></div>\n\n''')

    if statemachine == 0:
        md_response.append_s(CLIENT_DIV_START)
        md_response.append_s('      <h3 style="text-align:left;">Es sind noch keine Clients erkannt worden.</h3>\n')
    md_response.append_s('''
            </div><!-- Ende alle Clients -->''') 
    md_response.next_abschnitt()
    md_response.append_s(f'''
    {CLIENT_DIV_START}
        <h3>{_("Kontext")}: {_("Clients anlegen")}        </h3>
            <div style="float:left;">
                 <form action="/ti/do/managepost/config_clients" method="post">
                    Clientname:<input name="CAName" value=" " type="Text">
                    Anzahl:<input name="CAZahl" value="1" type="Number" size="4">
                    <input name="csrftoken" value="{csrftoken}" type="hidden">
                    <input value="{_('anlegen') }" type="submit">
                </form>
            </div>
            <div style="float:left;">{config.popuphilfe(_('Clients anlegen'), 
                _("""Hier angelegte Clients erhalten einen Code, über den sie <b>einmal</b> aktiviert werden können.
                Dazu wird ein Cookie übermittelt.
                Bei einer Anzahl &gt;1 werden mehrere Clients mit den Namen name_1, name_2 usw.
                 mit individuellen Codes angelegt."""))}
            </div>
    </div><!--Kontext-"Client-anlegen"-div zu -->\n''')
    return md_response
    

def status_info(ti_ctx):
    tosay = []
    items2show = [('uploadtimestamp_klartext', 'Letzter Upload (Info o. V-plan)'),
                  ('uploadtimestamp_klartext_Plan', '&nbsp;&nbsp;Letzter Plan-Upload'),
                  ('uploadtimestamp_klartext_Info', '&nbsp;&nbsp;Letzter Info-Upload'),
                  ('background_last_status', 'Zeitpunkt und Ergebnis des letzten Background-Prozesses')]
    tosay.append('''<div style="float:left;
                                margin:0.5em;border:1;background-color:#EEF;
                                border-style:solid;border-color:blue;border-radius:5px;
                                padding:3px;text-align:left;">
                        <h3 style="text-align:left;">Status des Systems</h3>
                        ''')
    # handselektierte Stati:
    tosay.append(statuszeile('Aktueller User', ti_ctx.ses.get_session_user()))
    
    for parameter, beschreibung in items2show:
        wert = ti_ctx.cnf.get_db_config(parameter, '(nicht gesetzt)')  # muss ja nicht klappen
        tosay.append(statuszeile(beschreibung, wert))
       
    tosay.append('</div>')  # schließt Statusanzeige
    return '\n'.join(tosay)
    

def statuszeile(beschreibung, wert):
    return f'''
                        <h4>{beschreibung}</h4>
                        <pre>   {str(wert)}</pre>'''
    
    
def config_todo(ti_ctx):
    if ti_ctx.check_csrftoken('clients'):
        # Ab hier werden die Eingaben verarbeitet
        
        delete_oldz = ti_ctx.req.get_value('client_deleteoldz')
        if delete_oldz == 'doit':
            ti_ctx.cnf.client_remove_oldz()
            return '+ Es wurde gelöscht'
            
        sortbyname = ti_ctx.req.get_value('client_sortbyname')
        if sortbyname:
            if not ti_ctx.cnf.get_db_config_bool('client_sortbyname'):
                ti_ctx.cnf.set_db_config('client_sortbyname', True)
                so = "Name"
            else:
                ti_ctx.cnf.set_db_config('client_sortbyname', False)
                so = "IP"
            return '+ Sortierung nach ' + so + '.'

        clip = ti_ctx.req.get_value('clientip')
        if clip:
            newname = ti_ctx.req.get_value('newname').strip()
            if len(newname) > 0:
                ti_ctx.cnf.client_set_name(clip, newname)
                return '+ Neuer Name für Client identifiziert mit ' + ti_lib.readable_ip(clip) + ': ' + newname

            mipha = ti_ctx.req.get_value_int('minphase', -1)
            mapha = ti_ctx.req.get_value_int('maxphase', -1)
            if mapha >= 0:  # negative minphase sind nun erlaubt für invertieren des Bereichs
                if mipha > mapha:
                    mipha, mapha = mapha, mipha
                if mipha > 0 and ti_ctx.req.get_value_bool('phaseintervallinvertiert'):
                    mipha = -mipha
                
                ti_ctx.cnf.client_set_phaselimits(clip, mipha, mapha)
                
                return f'''+ Anzeigebereich des Clients identifiziert mit {ti_lib.readable_ip(clip)
                       } geändert von { str(mipha) } bis { str(mapha) }'''
                
            gruppentoggle = ti_ctx.req.get_value_int('gruppentoggle')
            if (gruppentoggle > 0 and gruppentoggle < 65) or \
                    gruppentoggle in [256, 512, 1024, 2048]:
                # bei den großen immer nur ein Bit toggeln
                gruppen = ti_ctx.cnf.client_get_gruppen(clip)
                gruppen = gruppen ^ gruppentoggle  # xor
                ti_ctx.cnf.client_set_gruppen(clip, gruppen)
                return f'+ Gruppenzugehörigkeit des Clients identifiziert mit {ti_lib.readable_ip(clip)} geändert'
                
            if gruppentoggle == 128:
                # Gibt an, ob der Client ein zu überwachender AnzeigePC ist
                istanzg = not ti_ctx.cnf.client_get_istanzg(clip)
                ti_ctx.cnf.client_set_istanzg(clip, istanzg)
                return '+ Client identifiziert mit ' + ti_lib.readable_ip(clip) + \
                       ' ist nun ' + ("k" if not istanzg else "") + 'eine zu überwachende Anzeige'

            fwd = ti_ctx.req.get_value('forwardziel').strip()
            if fwd:
                ti_ctx.cnf.client_set_forward(clip, fwd)
                return '+ Der Client identifiziert mit ' + ti_lib.readable_ip(clip) + \
                       ' wird zukünftig auf "' + fwd + '" umgeleitet'
            ti_ctx.cnf.client_set_forward(clip, '')
            return '+ Kein Forward-Ziel definiert'
            # clip abgeschlossen
            
        cukopplung_ip = ti_ctx.req.get_value('cukopplung_ip')
        if len(cukopplung_ip) > 4: 
            cukopplung = not ti_ctx.cnf.client_get_cukopplung(cukopplung_ip)
            ti_ctx.cnf.client_set_cukopplung(cukopplung_ip, cukopplung)
            name = ti_ctx.cnf.client_get_name(cukopplung_ip)
            return f'''+ Client {ti_lib.readable_ip(cukopplung_ip)} ist 
                    {("nicht mehr " if not cukopplung else "")} mit {name} gekoppelt'''
            
        ip2remove = ti_ctx.req.get_value('ip2remove')
        if len(ip2remove) > 0:
            ti_ctx.cnf.client_remove(ip2remove)
            return '+ Der Client identifiziert mit ' + ti_lib.readable_ip(ip2remove) + ' wurde aus der Clientliste entfernt'
                    
        caname = ti_ctx.req.get_value('CAName').strip()
        cazahl = ti_ctx.req.get_value_int('CAZahl')
        if len(caname) > 1:
            if cazahl <= 1 or cazahl > 99: 
                res = ti_ctx.cnf.client_anlegen_mit_code(caname)
                if res:
                    return f'- Client {caname} NICHT angelegt ({res})'
                return f'+ Client {caname}  angelegt'
    
    return ''  # wurde gar nicht mit Formular aufgerufen oder csrftoken gefälscht
    

HTML_TEMPLATE = '''
    <div class="unfloatleft"></div>
    <div><p>Anleitung:</p>
        <p>Der Name wird im ManagementNetwork als Verfassername für Meldungen verwendet, 
            sofern keine Anmeldung stattfand. Außerdem kann ein Client mit einem Useraccount
            ge<b>koppelt</b> werden, wenn sie a) denselben Namen haben und b) der Client im 
            ManagementNetwork liegt. Dann erhält dieser Client sowohl die Rechte des ManagementNetworks
            als auch die Rechte des gleichnamigen Users.</p>
        <p>Die Clienttypen geben wieder, ob zielspezifische Zusatzinfos eingeblendet werden 
            sollen, z.B. für Oberstufe, Lehrerzimmer usw.. 
            Einige dieser Gruppen können mit eigenen Namen versehen werden (-> Einstellungen) und 
            dann konsistent in dem Meldungseditor und auf den Anzeigen verwendet werden.</p>
        <p>Ein Client kann zu vielen Typen gehören, was aber der Übersicht in der Anzeige nicht
            dienlich ist...</p>
        <p>Ist ein Client eine Anzeige (<b>Anzg</b>), so kann sein regelmäßiger Datenabruf 
            mit /ti/do/astatus überprüft werden.</p>
        <p>Wichtig: Eine Clientkonfiguration ist nur sinnvoll, wenn die betroffenen Clients 
            <b>feste IP-Adressen</b> haben!</p>
        <p>Alternative: Starten Sie auf dem Clientbrowser die Seite
            http://tabulaserver/ti/do/frames?setclientgroups=32;sc_code={sc_code}</p>
        <p>Damit wird ein Lehrerclient simuliert. Der Code ist zufällig generiert und spezifisch 
            für diese Installation. Mit setclientgroups=1 bzw. 2 bzw. 3 wird erste bzw. zweite Q-Stufe bzw.
            beide angesteuert usw.</p>
    </div>'''

CLIENT_DIV_START = '<div class="kategorie">'


def do_config(ti_ctx):
    sc_code = ti_ctx.cnf.get_db_config_int('sc_code')
    if sc_code < 100001:
        import random
        ti_ctx.cnf.set_db_config('sc_code', str(int(100000 + random.random()*900000)))
        sc_code = ti_ctx.cnf.get_db_config_int('sc_code')
    erfolg = config_todo(ti_ctx)
    ti_ctx.res.debug("Erfolg", erfolg)
    if erfolg:
        ti_ctx.set_erfolgsmeldung(erfolg)
    md_r = config_dialog(ti_ctx)
    md_r.next_abschnitt()
    md_r.append_s(HTML_TEMPLATE.format(sc_code=sc_code))
    md_r.next_abschnitt()
    md_r.append_s(status_info(ti_ctx))
    
    return md_r


if __name__ == '__main__':
    ti_lib.quickabort2frames()
