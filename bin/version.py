'''
________________________________________________________________________
 version.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul hält die Versionsinformation und wird immer angepasst.
________________________________________________________________________
'''

ti_version = "2.1.1"
ti_release = "2024-06-17"
TI_BRANDING = f''' 
        <div id="branding"><p>
                https://tabula.info     <br>
                Version {ti_version}    <br><small>
                Release {ti_release}        </small> </p>
        </div>'''

TI_KOMPAKT_VERSION = f'''tabula.info, Version {ti_version}, Release {ti_release}'''

TI_LOGO = 'logo_2.1.png'

