'''
________________________________________________________________________
 ti_context.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_context.py liefert die gleichnamige Klasse. 
    Deren Aufgabe ist:
        * verfügbarmachen der anderen großen Objekte sys, cnf, res, rew
        * diverse Funktionen durchzuführen, die diesen context nutzen
        
    Da jede größere Methode den ti_ctx für ihren Thread bekommt, 
        kann quasi jeder diesen Context und seine Funktionen nutzen
        ohne sich um Threadsicherheit Gedanken machen zu müssen.
    Hiermit werden final globale Variablen elegant eliminiert!
________________________________________________________________________
'''

# Batteries

# TI
import ti_lib
import ti
import konst
 
# ################################################    
# ## hier beginnt der Code zur Initialisierung ###
# ################################################


class TIConTeXt(object):
    """ Der TI-Context nimmt als Objekt die Zeiger auf alle 
        notwendigen Objekte für die Interaktion mit Request, Response usw. auf.
        Eine Methode, die diesen Context übergeben bekommt kann also 
        auf alle Daten zugreifen bzw. zurückgeben.
        sys: Schnittstelle zu ....
        cnf: Schnittstelle zur Konfigurationsdatenbank
        req: Hält die Request-Daten bereit
        res: Baut die Response zusammen
        ses: verwaltet einen session (Login oder per Verwaltungsnetz)"""
    def __init__(self, sys, cnf, req, res, ses, paths):
        self.sys = sys
        self.cnf = cnf
        self.req = req
        self.res = res
        self.ses = ses
        self.paths = paths
        import s_any  # FIXME? da s_any definitiv noch nicht angepasst 2020-01-13
        self.s_interface = s_any.S_Interface(self)
        
    def replace_session(self, new_session):
        """Diese Methode benötigt, damit nach erfolgreichem Login gleich eine neue Session genutzt wird"""
        self.ses = new_session
        
    # ## nun die direkt auf diesem Datensatz operierenden Funktionen
    
    def check_management_rights(self, rechte=2 ** 16 - 1):
        # Kunstgriff, um ti_ctx implizit zu übergeben:
        return self.ses.check_management_rights(self, rechte)
    
    def get_ti_lastupload(self):
        return self.cnf.get_db_config('uploadtimestamp', '123')

    def get_client_ip_or_name(self):
        # teuer, sollte nur von management-modulen genutzt werden
        # debug("clientname", __ti_clientname__)

        name = self.ses.get_session_user()
        if name and name != 'vianetwork':
            return name
        name = self.cnf.client_get_name(self.req.client.ip)
        return name

    # liefert (wenn gesetzt) eine Client-spezifische Obergrenze für anzuzeigende
    # Vertretungsplantage, Obergrenze 99 ist hier fest kodiert
    def get_client_phaselimits(self, ip=''):
        if not ip:
            return self.req.client.phaselimits()
        ti_client = self.cnf.client_read_ti_client(i)
        # name, timestamp, minphase, maxphase, gruppe = self.cnf.client_get_config(ip)
        if not ti_client:
            return self.req.client.phaselimits()
        return ti_client.minphase, ti_client.maxphase

    def get_client_max_groups_number(self):
        gruppen, _, _ = self.req.client.groups()
        cgn = 6
        while cgn > 0:
            if gruppen >= 1 << (cgn - 1):
                return cgn
            cgn -= 1
        return 0
        
    def set_erfolgsmeldung(self, meldung):
        self.ses.set_session_meldung(meldung)
            
    def print_html_tail(self, strarray=[]):
        """Gibt ggf. das Stringarray strarray aus und schließt dann mit den Debugmeldungen jegliche Ausgabe ab. """
        for r in strarray:                              # Vorbereitung auf andere Ausgabe als direktes ti.prt
            ti.prt(r)
        ti.prt(self.get_html_tail())

    def get_html_tail(self):
        if self.ses:
            sesdebug = self.ses.get_n_clear_debugmeldungen()   # DEBUG-Ausgaben der Session anhängen
        else:
            sesdebug = ''
        # eigentliches html abschließen und die DEBUG-Ausgaben anhängen
        return f'\n{konst.INCLUDESCRIPTS}\n\t</body>\n</html>\n\n\n{sesdebug}\n{self.res.debug_flush()}'  # one string
        
    def print_html_head(self, *args, **kwargs):
        # Kunstgriff, um ti_ctx implizit zu übergeben
        ti_lib.prt(ti.get_html_head(self, *args, **kwargs))

    def set_csrftoken(self, menu):
        csrftoken =  self.ses.set_csrftoken(menu);
        self.res.debug('csrftoken', csrftoken, menu)
        return csrftoken
        
    def check_csrftoken(self, menu):
        csrftoken = self.req.get_value_int('csrftoken')
        self.res.debug('csrftoken', csrftoken)
        return self.ses.check_csrftoken(csrftoken, menu)
        
if __name__ == '__main__':
    ti_lib.quickabort2frames()
