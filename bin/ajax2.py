'''
________________________________________________________________________
 ajax2.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul bearbeitet die AJAX-Aufrufe und liefert immer json-Datensätze zurück
________________________________________________________________________

'''

# Batteries
import json

# TI
import ti_lib
import messages
import persons
import pages
import man


def do_ajax(ti_ctx):
    task = ti_ctx.req.get_value('task')
     
    resultat = {}
    if task not in ['get_all', 'get_msections', 'get_popup', 'get_bottom', 'get_plan', 'get_bo_status']:
        resultat["status"] = "unbekannter Datenwunsch!"
        return json.dumps(resultat)

    resultat["status"] = "ok"

    # Backoffice - Import - Status ermitteln
    if task == 'get_bo_status':
        resultat['neu_status'], resultat['html'] = man.get_bo_status(ti_ctx)
        return json.dumps(resultat) 
        
    # Ab hier Ajax-Anfragen für das neue Interface...
    if task in ['get_all', 'get_plan']:
        resultat["plans"] = alle_seiten(ti_ctx)

    if task in ['get_all', 'get_bottom']:
        resultat["bottom"] = [messages.create_messages(ti_ctx, 1), messages.create_messages(ti_ctx, 2)]

    if task in ['get_all', 'get_msections']:
        clientgroups = 0
        messagesstring = messages.create_messages(ti_ctx, clientgroups)
        personsstring = persons.iterate_persons(ti_ctx, False)
        resultat["msections"] = [messagesstring, personsstring]
    resultat["frontstatus"] = ti_lib.unix_time_2_human_date_n_time(None, True)
    return json.dumps(resultat) 


def iframe(url):
    """liefert den notwendigen HTML-Code, um ein iframe mit der Wunschurl einzublenden"""
    return '''<iframe class="plan_iframe" src="{}">
                Uuups, Ihr Browser kann keine eingebetteten Frames anzeigen</iframe>'''.format(url)


def zweiplaene(linker, rechter):
    return '''
        <div id="contentpl" class="tiP_Row">{}</div>
        <div id="contentpr" class="tiP_Row">{}</div>
        '''.format(linker, rechter)


def alle_seiten(ti_ctx):
    pa = pages.PageAssembly(ti_ctx)
    pa.analyse_whole_schedule()
    return [page.get_dict() for page in pa.pages]
