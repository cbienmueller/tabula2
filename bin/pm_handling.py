'''
________________________________________________________________________
 pm_handling.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 pm_handling.py kümmert sich die Darstellung der Kombination von 
    Personenruf und
    Meldungen
________________________________________________________________________
 Start Nov 2020, sauberes Interface mit named tuple
'''

# Batteries
from collections import namedtuple

# TI
import messages         # liefert die Nachrichten
import persons          # liefert den Personenruf    
import weekmessage


PM_Analyse = namedtuple('PM_Analyse', ['spalte1', 'spalte2', 'spaltenzahl', 'timeout_s', 'miniflag1', 'miniflag2'])


def pm_analyse(ti_ctx, clientgroups, hochkant):
    '''Diese Routine klärt, ob Personenruf und Messages in eine Spalte passen, oder auf 2 Spalten verteilt werden müssen.
    Rückgabe-Named-Tupel siehe oben'''
    # alle Meldungen holen
    p_string = persons.iterate_persons(ti_ctx, clientgroups & 3)
    m_string = messages.create_messages(ti_ctx, False) + weekmessage.get_messages(ti_ctx)
    p_zahl = p_string.count('<tr>')
    if p_zahl > 0:
        p_zahl += 2
    m1 = []
    m2 = []
    if p_zahl:
        m1.append(p_string)
    spalte1 = ''
    spalte2 = ''
    spaltenzahl = 0
    
    m_zahl = m_string.count('<h4>')*1.2 + m_string.count('<p>') + m_string.count('<br>') + m_string.count('cal_line')  
    if m_zahl and p_zahl:
        virtuelle_nachrichten_zeilenzahl = int(p_zahl + m_zahl + 1)
    else:
        virtuelle_nachrichten_zeilenzahl = int(p_zahl + m_zahl)
    # ti_ctx.res.debug("Full Content!", p_string, m_string, "-", p_zahl)
    virtuelle_nachrichten_zeilenzahl_grenze = ti_ctx.cnf.get_db_config_int('max_virt_mess_lines', 27)
    minigrenze = virtuelle_nachrichten_zeilenzahl_grenze*60/100
    ti_ctx.res.debug("virt. Zeilenanzahl , konfigurierte Grenze", 
                     virtuelle_nachrichten_zeilenzahl,
                     virtuelle_nachrichten_zeilenzahl_grenze)
    
    if not virtuelle_nachrichten_zeilenzahl:  # gibt garnichts?
        return PM_Analyse('', '', 0, 0, True, True)
    if hochkant or (virtuelle_nachrichten_zeilenzahl <= virtuelle_nachrichten_zeilenzahl_grenze): 
        # cool, passt alles in eine Spalte
        spalte1 = ''.join(m1) + m_string
        pm_a = PM_Analyse(spalte1, 
                          spalte2,
                          1,
                          virtuelle_nachrichten_zeilenzahl / 3,
                          True if (virtuelle_nachrichten_zeilenzahl < minigrenze) else False,
                          False)
        return pm_a
        
    # nun ja, doch noch Arbeit - wir bauen zwei Spalteninhalte auf
    """ Idee: Fülle mtag mit den messages eines Tages.
              mtag wird dann in m1 oder, wenn damit dessen zulässige Länge überschritten werden würde, 
                             in m2 verschoben
              Sobald m2 angefangen wurde (msplitflag == True) wird nur noch direkt in m2 geschrieben, 
              parallel werden die virtuellen Zeilenlängen (ein Schätzwert) gezählt.
              Ist eine Seite (m1 und/oder m2) unter der minigrenze, so wird die Seite als mini 
              deklariert, so dass frames noch einen INhalt unten dran hängen kann."""
    ti_ctx.res.debug('Teile Messages auf!')
    # Teile messagesstring auf
    m_zeilen = m_string.split('\n')
    mtag = []
    msplitflag = False
    h4 = 0
    br = 0
    neue_virtuelle_nachrichten_zeilenzahl = 0
    for line in m_zeilen:
        h4 += line.lower().count('<h4')
        br += line.lower().count('<br>') + line.lower().count('<p>')
        if msplitflag:
            m2.append(line)
        else:
            newday = line.lower().count('"datumdiv">')
            if newday:  # ein neuer Tag beginnt
                # ti_ctx.res.debug("Neuer Tag erreicht bei", line)
                m1 += mtag  # letzten Tag sichern
                mtag = []   # Platz für den neuen Tag
                m1_nvz = neue_virtuelle_nachrichten_zeilenzahl
            mtag.append(line)
            neue_virtuelle_nachrichten_zeilenzahl = p_zahl + 1.2 * h4 + br
            # ti_ctx.res.debug("nvz, mtag, m1, m2, erreicht bei", int(neue_virtuelle_nachrichten_zeilenzahl*10)/10.0,
            # #                len(mtag), len(m1), len(m2), line)
            if neue_virtuelle_nachrichten_zeilenzahl > virtuelle_nachrichten_zeilenzahl_grenze:
                m2.append('<div class="messages brownborder"><!-- Zweite Messagesspalte -->')
                m2 += mtag
                msplitflag = True  # jetzt werden alle weiteren Zeilen in m2 geschoben
        
    # ti_ctx.res.debug("End, mtag, m1, m2", int(neue_virtuelle_nachrichten_zeilenzahl*10)/10.0, len(mtag), len(m1), len(m2))
    if msplitflag:
        m1.append('</div>  <!-- Ende Messages links-->')
        # kein schließendes div für rechts, da dort schon enthalten!
        m2_nvz = neue_virtuelle_nachrichten_zeilenzahl - m1_nvz
        spalte2 = '\n\t'.join(m2)
        spaltenzahl = 2
    else:
        ti_ctx.res.debug('Kein Split am Ende? Sollte nicht vorkommen! Vermutlich zu viel an einem Tag - unsplittbar.')
        m1 += mtag  # beinhalted bereits schließendes /div für messages!
        m1_nvz = neue_virtuelle_nachrichten_zeilenzahl
        m1.append('<!-- das war der angefangene Tag XX sollte eigentlich nicht vorkommen! -->')
        spaltenzahl = 1
        m2_nvz = 0
    spalte1 = '\n\t'.join(m1)
    return PM_Analyse(spalte1,
                      spalte2, 
                      spaltenzahl, 
                      virtuelle_nachrichten_zeilenzahl / 3, 
                      True if (m1_nvz < minigrenze) else False,
                      True if (m2_nvz < minigrenze) else False)

    # Ende von pm_analyse


if __name__ == '__main__':
    ti_lib.quickabort2frames()
