#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
________________________________________________________________________
 weekmessage.py                                                         
 This file is part of tabula.info, which is free software under           
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 weekmessages.py zeigt für die laufende und die kommende Woche Meldungen an
    Dazu liest es eine triviale Liste (./data/week.lst) ein im Format
    # Wochennummer;Meldungstext
    28;Ferien!
________________________________________________________________________
'''
# Batteries
import codecs

# TI
import ti
import ti_lib

    
def get_messages(ti_ctx):
    
    try:
        data_path = ti_ctx.paths.get_data_path()
        weeks = codecs.open(data_path / 'week.lst', encoding='utf-8', mode='r')
        weeknr = ti.get_todays_calendarweek()
        nextnr = 99
        itmess = '''
          <div class="weekmessages">
            '''
        thisweek = ""
        nextweek = ""
        anzahl = 0
        for line in weeks:
            anzahl += 1
            if len(line) > 4 and ";" in line:  # Nur Fehlervermeidung
                lastin = line
                nrstrg, messageraw = line.split(";", 1)
                if nrstrg.isdigit():
                    nr = int(nrstrg)            # Echte Zahl
                    message = messageraw.strip()  # Sauberer String
                    # Meldung für diese Woche? Ergänzen!
                    if nr == weeknr:            
                        thisweek += message+'<br>'
                    # Meldung für eine folgende Woche, die aber kleiner 
                    # als die bisher angedachte folgende Woche ist? Neu anlegen
                    elif nr > weeknr and nr < nextnr:
                        nextweek = message + '<br>'
                        nextnr = nr
                    # Meldung für die folgende Woche - wie geplant - ergänzen
                    elif nr == nextnr:
                        nextweek += message+'<br>'
        weeks.close()
        ti_ctx.res.debug("week.lst Zeilenzahl, letzter Inhalt", anzahl, lastin)

        # itmess += '<h3>Wochenmeldungen</h3>\n'
        
        if len(thisweek+nextweek) < 1:
            return ""
        if len(thisweek):
            itmess += '\t<h4>Diese Woche (KW ' + str(weeknr) + ')</h4>\n'
            itmess += '\t<p>' + thisweek + '</p>\n'
        if len(nextweek):
            mm = ''
            if nextnr != weeknr+1:
                mm = ' mit Meldungen'
            itmess += '\t<h4>Folgende Woche' + mm + ' (KW ' + str(nextnr) + ')</h4>\n'
            itmess += '\t<p>' + nextweek + '</p>\n'
            
        itmess += "    </div>"  # von div class=weekmessages
        return itmess
    except Exception as e:
        ti_ctx.res.debug('weekmessage scheiterte', str(e))
    
    return ""
  
    
if __name__ == '__main__':
    ti_lib.quickabort2frames()

