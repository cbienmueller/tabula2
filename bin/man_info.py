'''
________________________________________________________________________
 man_info.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man_info.py ermöglicht informationen als PDF oder Bild hochzuladen
________________________________________________________________________
'''

# Batteries
import uuid
import subprocess

# TI
import ti_lib
import konst
import infobox

PRIO_COLOR = ["#F79", "white", "#FFA", "#EE8", "#DD6", "yellow"]
PRIO_LABEL = ["<small>Vorrat</small>", "1", "2", "3", "4", "Max"]


def ip_thumb_n_config(ipcfg, webpath):
    if ipcfg.prio:
        prio_zeile = f'''
            {_('Zeige inkl.')} <b>{ti_lib.iso_date_2_human_date(ipcfg.lastday)}</b><br>
            {_("Häufigkeit")}: <span style="font-size:120%;background-color:#bbb;padding:2px;">'''
        for prio_loop in range(6):
            if ipcfg.prio == prio_loop:
                loop_label = f'<b>{PRIO_LABEL[prio_loop]}</b>'
                loop_color = 'black'
            else:
                loop_label = PRIO_LABEL[prio_loop]
                loop_color = '#444'
            prio_zeile += f'''
            <a  href="/ti/do/manage/info?set_prio_for={ipcfg.name}&set_prio_to={prio_loop}" 
                style="background-color:{PRIO_COLOR[prio_loop]};color:{loop_color};">
                    {loop_label} 
            </a>&nbsp;'''
    else:
        prio_zeile = f'''
        <a href="/ti/do/manage/info?number_2_delete={ipcfg.ipid}">
            <img src="/ti/static/pix/erase.png" width="150" height="19" border="0" alt="delete"></a><br>
        <form action="/ti/do/managepost/info" method="POST">
            <label for="vlastday"><small>{_("Wieder zeigen bis")}</small></label>
            <input type="date" id="vlastday" name="vlastday" value="{ti_lib.iso_date_heute_plus_delta_days(3)}">?
            <input type="hidden" id="vname" name= "vname" value="{ipcfg.name}">
            <input type="submit" name="hochladen" value="{_('Ok')}" /></form>
                    '''

    return f'''
    <td style="padding:0.5em;text-align:center;vertical-align:bottom;background-color:{PRIO_COLOR[ipcfg.prio%6]}">
        <a href="{webpath}{ipcfg.name}" target="_blank">
            <img src="{webpath}{ipcfg.thumbnail}"></a><br><small>
        {_('Infoseite hochgeladen von')} {ipcfg.author} ({
            ti_lib.unix_time_2_human_date_n_time(ipcfg.upload_epoch).replace(" ",'&nbsp;') if ipcfg.upload_epoch else ""
            })</small><br>
        {prio_zeile}
        </span>
    </td>\n'''


def do_manage(ti_ctx):
    save_uploaded_file(ti_ctx)
    do_delete(ti_ctx)
    do_change(ti_ctx)

    md_response = ti_lib.Multi_Detail_Response()
    spezialrechte = ti_ctx.check_management_rights(konst.RECHTEInfoupload | konst.RECHTEInfouploaderweitert) \
        & konst.RECHTEInfouploaderweitert
    HTML_TEMPLATE_S = '''
        <hr>
        <h3>{}</h3>
    '''.format(_('Hochladen einer dynamischen Info-Seite'))
    HTML_TEMPLATE2 = '''
    <div class="infos">

        {imageliste}
        
        
    </div>
    '''

    infopath = ti_ctx.paths.get_dyn_path( "info/") 
    webpath = ti_ctx.paths.get_dyn_url('/info/')
    maxinfopix = min(ti_ctx.cnf.get_db_config_int('MaxInfoPix', 6), 99)  # 99, are U nuts?
    imagelist = '<div><h3>{}:</h3><br><table ><tr>'.format(
        _('Aktuelle Info-Seiten (sie werden im zufälligen Wechsel angezeigt)'))
    v_imagelist = '<div><h3>{}:</h3><br><table ><tr>'.format(_('Info-Seiten auf Vorrat'))
    zaehler = 0
    info_pix_cfgs_list = infobox.dir2info_pix_configs_list(ti_ctx, infopath)
                
    for ipcfg in info_pix_cfgs_list:
        if ipcfg.ext == 'png' and ipcfg.prio:
            if zaehler and not zaehler % 2:
                imagelist += '</tr>\n<tr>'

            imagelist += ip_thumb_n_config(ipcfg, webpath)
            zaehler += 1
    if zaehler > 0:
        imagelist += '</tr></table><br></div>'
    else:
        imagelist = ""
        
    # sammle nun die Vorratsbilder mit prio 0
    v_zaehler = 0
    for ipcfg in info_pix_cfgs_list:
        if ipcfg.ext == 'png' and not ipcfg.prio:
            if v_zaehler and not v_zaehler % 2:
                v_imagelist += '</tr>\n<tr>'

            v_imagelist += ip_thumb_n_config(ipcfg, webpath)
            v_zaehler += 1
    if v_zaehler > 0:
        v_imagelist += '</tr></table><br></div>'
    else:
        v_imagelist = ""
    
    inputlist = ""
    if zaehler >= maxinfopix:
        inputlist += '<div style="background-color:orange">{}</div>'.format(
                _('Meldung: Es können keine weiteren Bilder hinzugefügt werden!'))
    else:
        if zaehler > (maxinfopix / 2):
            imagelist += '<div style="background-color:yellow">{}</div>'.format(
                _('Warnung: Da so viele Bilder angezeigt werden sollen, sieht man die einzelnen Bilder recht selten!'))
        if not ti_ctx.cnf.get_db_config_bool("Flag_ShowInfos", False):
            inputlist += f'''<h3 class="hiprio">{
                         _('Info-Seiten müssen in den Einstellungen erst eingeschaltet werden damit sie angezeigt werden!')
                         } (ShowInfos)</h3>'''
        inputlist += f'''
            <h3>{_('Hochladen einer statischen Info-Seite')}</h3>
                <form action="/ti/do/managepost/info" method="POST" enctype="multipart/form-data">
                {_('Eine Datei (PDF und Bildformate, möglichst quadratisch) auswählen,'
                   ' das letzte Anzeigedatum wählen und dann abschicken')}.<br>
                    <input name="info_file" type="file" /> 
                    <label for="lastday">Letzter Tag:</label>
                    <input type="date" id="lastday" name="lastday" value="{ti_lib.iso_date_heute_plus_delta_days(3)}">
                    <input type="submit" name="hochladen" value="{_('Hochladen')}" /><br><br>
                    {_("""Beachte:<br>
                    Transparente Bereiche werden weiß und weiße Ränder werden abgeschnitten""")}
                </form>'''
    spezialliste = ""
    zaehler = 0
    if spezialrechte:
        spezialliste = HTML_TEMPLATE_S
        spezialmitte = '''
                        <div style="not-background-color:white">
                            <table style="not-background-color:white;border:0">
                                <tr>
                                    <th colspan=2><h3>'''+_('Aktuelle animierte Seiten')+''':</h3></th>
                                <tr>'''
        for ipcfg in info_pix_cfgs_list:
            if ipcfg.ext == 'gif':

                if zaehler and not zaehler % 2:
                    spezialmitte += '</tr>\n<tr>'

                spezialmitte += ip_thumb_n_config(ipcfg, webpath)
                zaehler += 1
        spezialmitte += '</tr></table><br></div>'
        if zaehler < 2:
            spezialliste += f'''
                    <form action="/ti/do/managepost/info" method="POST" enctype="multipart/form-data">
                        {_('Eine Datei (animiertes GIF oder mehrseitiges PDF, möglichst quadratisch) auswählen,'
                           ' das letzte Anzeigedatum wählen und dann abschicken')}<br>
                        <input name="info_gif" type="file" />
                        <label for="lastday">Letzter Tag:</label>
                        <input type="date" id="lastday" name="lastday" value="{ti_lib.iso_date_heute_plus_delta_days(3)}">
                        <input type="submit" name="hochladen" value="{_('Hochladen')}" /><br><br>
                    <br>
                    { _("""Beachte:<br>
                    - der Import kann einige Sekunden dauern, <br>
                    - PDF-Seiten wechseln immer nach placeholder Sekunden, <br>
                    - <small>bei Konvertierungs-/Darstellungsfehlern das hochzuladende GIF 
                        vorher in nicht-optimierter Form speichern</small>.""").replace(
                            "placeholder",
                            str(ti_ctx.cnf.get_db_config_int('info_pdf_ani_delay', 5)
                        ))}
            </form>'''
        else:
            spezialliste += _('Es können hier keine weiteren animierten Seiten hinzugefügt werden.')
        if zaehler > 0:
            spezialliste += spezialmitte
        else:
            spezialliste += f'''<p><b>{_('Aktuell sind keine dynamischen Info-Seiten hochgeladen')}</b></p>'''
        
    md_response.append_s('<div class="infos"><!-- Start erstes info-div -->')
    md_response.append_s(inputlist)
    md_response.append_s(spezialliste)
    md_response.append_s('</div><!-- Ende erstes info-div -->')
    
    if imagelist:
        md_response.next_abschnitt()
        md_response.append_s(HTML_TEMPLATE2.format(imageliste=imagelist))
    if v_imagelist:
        md_response.next_abschnitt()
        md_response.append_s(HTML_TEMPLATE2.format(imageliste=v_imagelist))
        
    return md_response


def save_uploaded_file(ti_ctx):
    """This saves a file uploaded by an HTML form.
    The form_field is the name of the file input field from the form.
    If no file was uploaded or if the field does not exist then
    this does nothing.
    """
    # FIXME: verhindere unbegrenzten Upload mit u.a. maxinfopix = min(ti_ctx.cnf.get_db_config_int('MaxInfoPix', 6), 20)
    infopath = ti_ctx.paths.get_dyn_path("info") 
    ist_gif = False
    unique = uuid.uuid4().hex
    upload_path = infopath / f'upl_{unique}'

    origfilename = ti_ctx.req.save_file('info_file', upload_path)
    if not origfilename:
        origfilename = ti_ctx.req.save_file('info_gif', upload_path)
        if origfilename:
            ist_gif = True
        else:
            return False

    meldung = ""
    filetyp = 'gif' if ist_gif else 'png'
    
    temp_path = infopath / f'tmp_{unique}.{filetyp}'
    try:
        if ist_gif:
            if origfilename.lower().endswith('.pdf'):
                info_ani_delay = ' -delay {} '.format(ti_ctx.cnf.get_db_config_int('info_pdf_ani_delay', 5)*100)
            else:
                info_ani_delay = ''
            subprocess.check_output(
                f'gm convert {info_ani_delay} {upload_path.as_posix()}' 
                f' -coalesce -bordercolor white -border 0 -resize 950x1000 ' + 
                f' {temp_path.as_posix()}',
                shell=True, stderr=subprocess.STDOUT)
        else:
            subprocess.check_output(
                f'gm convert {upload_path.as_posix()}'
                f'[0] -background white -flatten -trim  -resize 950x1000  -bordercolor white'
                f' -interlace line {temp_path.as_posix()}',
                shell=True, stderr=subprocess.STDOUT)
        hashhex = ti_lib.berechne_md5_von(temp_path)
        file_path = infopath / (hashhex + '.' + filetyp)
        thumb_path = infopath / (hashhex + '-thumb.' + filetyp)
        
        temp_path.rename(file_path)
        subprocess.check_output(
            f'gm convert {file_path.as_posix()} '
            f'-resize  250x250 {thumb_path.as_posix()} &', 
            shell=True, stderr=subprocess.STDOUT)

        ipcfg = ti_ctx.cnf.infopix_get_full_cfg(file_path.name)
        ipcfg.author = ti_ctx.get_client_ip_or_name()
        ipcfg.lastday = ti_ctx.req.get_value('lastday', '')
        ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
        ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())
        ipcfg = ti_ctx.cnf.infopix_get_full_cfg(file_path.name)
        ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())

        subprocess.check_output('rm '+upload_path.as_posix(), shell=True, stderr=subprocess.STDOUT)

    except subprocess.CalledProcessError as err:
        meldung = '<pre>'+err.output.decode("utf-8", "ignore")+'</pre>'
    resultat = ('+'+_('erfolgreicher Upload mit Konvertierung')) if not meldung \
        else ('-'+_('Es ist ein Fehler bei der Konvertierung aufgetreten')+':<small><small><br>'+meldung+'</small></small>')
    ti_ctx.set_erfolgsmeldung(resultat)             
    ti_ctx.cnf.set_ti_lastupload('Info')

    
def do_delete(ti_ctx):
    n2d = ti_ctx.req.get_value('number_2_delete', '')
    if n2d:
        infopath = ti_ctx.paths.get_dyn_path("info")
        info_pix_cfgs_list = infobox.dir2info_pix_configs_list(ti_ctx, infopath)
        for ipcfg in info_pix_cfgs_list:
            ti_ctx.res.debug("do_delete", n2d, ipcfg.name)
            if ipcfg.ipid == n2d:
                ti_ctx.res.debug("namen", ipcfg.name, ipcfg.thumbnail)
                try:
                    (infopath / ipcfg.name).unlink()
                    (infopath / ipcfg.thumbnail).unlink()
                    ti_ctx.set_erfolgsmeldung('+'+_('Erfolgreich gelöscht')) 
                    ti_ctx.cnf.infopix_rm_cfg(ipcfg.name)
                except Exception as e:
                    ti_ctx.set_erfolgsmeldung('-'+_('Fehler beim Löschen')+': '+str(e)) 
                break
        else:
            ti_ctx.set_erfolgsmeldung('-'+_('Zu löschende Datei nicht gefunden'))


def do_change(ti_ctx):
    # Ändere Priorität
    filename = ti_ctx.req.get_value('set_prio_for', '')
    if filename:
        newprio = ti_ctx.req.get_value_int('set_prio_to', -1)
        ti_ctx.res.debug("set_prio", filename, newprio)
        if newprio >= 0 and newprio <= 5:
            ipcfg = ti_ctx.cnf.infopix_get_full_cfg(filename)
            if ipcfg.prio != newprio:
                ipcfg.prio = newprio
                ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
                # ti_ctx.res.debug("ipcfg as_tupel()", ipcfg.as_tupel())

    else:
        # Reaktiviere aus Vorrat mit neuem Ablaufdatum
        filename = ti_ctx.req.get_value('vname', '')
        if filename:
            lastday = ti_ctx.req.get_value('vlastday')
            if ti_lib.ist_isodatum(lastday):
                ipcfg = ti_ctx.cnf.infopix_get_full_cfg(filename)
                ipcfg.prio = 1
                ipcfg.lastday = lastday
                ti_ctx.cnf.infopix_set_full_cfg(ipcfg)
                

if __name__ == '__main__':
    ti_lib.quickabort2frames()
