#!/usr/bin/python3
'''
________________________________________________________________________
 ti.wsgi                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul wird vom Apache-Modul mod_wsgi aufgerufen und 
   gibt den Aufruf normgerecht an eine app weiter.
________________________________________________________________________

'''

from ti2 import create_app
application = create_app()    
