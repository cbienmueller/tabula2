#!/usr/bin/python3
'''
________________________________________________________________________
 cal_load.py                                                         
 This file is part of tabula.info, which is free software under           
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 cal_load.py lädt konfigurierte Kalender von verschiedenen iCalendar-Quellen.
 Aufruf erfolgt von background aus, nicht interaktiv!
________________________________________________________________________
'''
# Batteries
import pickle

# TI
import ti_lib
import time

# Batteries 2.0 (via Debian apt)
try:
    import icalendar
except Exception as e:
    import ti2
    ti2.panic('python3-icalendar ist nicht installiert!', e, 
              'Sie müssen zuerst das fehlende Debian-Paket python3-icalendar installieren!')


# ## 
# ## 1. Download der Kalender und speichern als Dateien
# ## 

def do_downloads(ti_cnf, forced=False):
    to_log_summe = ""
    
    # ## Ermittle aktuelle Kalender
    id_liste = []
    cobject_liste = ti_cnf.get_calendars()
    for co in cobject_liste:
        id_liste.append(co.cal_id)
    
    # ## Erzeuge Dateiliste mit ics-Endung
    directory = list(ti_cnf.paths.get_temp_path("cal").iterdir())
    try:
        if directory: 
            for filepath in directory:            
                if not filepath.suffix == ".ics":
                    continue
                if filepath.stem not in id_liste:  # Lösche veraltete Dateien
                    filepath.unlink()

        for co in cobject_liste:
            unix_time = int(time.time())
            if forced or (unix_time > co.last_ts + 60 * co.delta_t_min):   # ist überhaupt ein Download fällig?
                to_log = download_cal(ti_cnf, co.cal_id, co.url)
                if not to_log:
                    ti_cnf.set_calendar_ts(co.cal_id, unix_time)  # den Unix-Timestamp in ganzen Sekunden
                    ti_cnf.paths.log(f'CAL: Kalenderdownload ok für "{co.cal_id}"')
                else:
                    ti_cnf.set_calendar_error(co.cal_id, to_log)
                    ti_cnf.paths.log(f'CAL: Kalenderdownload gescheitert für "{co.cal_id}": {to_log}; ')

    except Exception as e:
        to_log_summe = 'CAL: ' + ti_lib.beschreibe_exception(e)
        ti_cnf.paths.log(to_log_summe)

    pickle_errors = pickle_cal_lines(ti_cnf, ti_cnf.get_db_config_int("cal_calendar_days", 8))

    return to_log_summe + pickle_errors


# Lade einen Kalender aus URL
def download_cal(ti_cnf, cal_id, url):
    to_log = ""
    ziel = ti_cnf.paths.get_temp_path() / 'cal' / (cal_id+'.ics')  # global festgelegtes download-Verzeichnis
    import urllib.request
    try:
        req = urllib.request.Request(
                url, 
                data=None, 
                headers={
                    'User-Agent':
                    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                    'Chrome/114.0.0.0 Safari/537.36 (tabula.info 2.1)'
                }
            )
        with urllib.request.urlopen(req) as f:
            rawdata = f.read()
        if rawdata:
            with ziel.open('wb') as dest:
                dest.write(rawdata)
        
    except urllib.error.URLError:
        to_log = _('URL nicht gefunden')
    except urllib.error.HTTPError:
        to_log = str(e)
    except Exception as e:
        to_log = f'Softwareproblem: {str(e)}'

    return to_log


# ## 
# ##  2. Einlesen dieser Dateien, Erzeugen geeigneter HTML-Schnipsel in Datenstruktur und pickeln in Datei
# ##

CAL_FILTER = {"moon": [('Full', 'Vollmond'), ('New', ''), ('First', ''), ('Last', '')]}  # 'Neumond'
CAL_TAUSCH = [('"', "'"), ('<', "&lt;"), ('>', "&gt;")]


class Cal_Event_Lists():
    """ Enthält die Subjects von eingelesenen Kalendern in 2dim. Listen:
        event_list[delta][n]
        delta aus 0 (heute) ... (13) für die nächsten 2 Wochen,
        n durchnummeriert für jeden Tag
    """
    def __init__(self, ti_cnf, tage, cal_id, bcolor):
        self.bcolor = bcolor
        self.event_lists = [[] for i in range(tage)]
        self.max_tage = tage
        self.error = ""
        if cal_id:
            datei = ti_cnf.paths.get_temp_path() / 'cal' / (cal_id+'.ics')  # global festgelegtes download-Verzeichnis
            try:
                with open(datei, 'rb') as e:
                    ecal = icalendar.Calendar.from_ical(e.read())
                    for i in range(self.max_tage):
                        isodatum = ti_lib.iso_date_heute_plus_delta_days(i)
                        # print(isodatum)
                        for component in ecal.walk():
                            if component.name == "VEVENT":
                                flag = 0
                                if f'{component.decoded("dtstart")}'.startswith(isodatum):
                                    flag = 1 
                                elif f'{component.decoded("dtstart")}' < isodatum \
                                     and isodatum < f'{component.decoded("dtend")}':
                                    flag = 2
                                if flag:
                                    summary = f'{component.get("summary")}'
                                    if cal_id in CAL_FILTER:
                                        for such, ersatz in CAL_FILTER[cal_id]:
                                            if summary.startswith(such):
                                                summary = ersatz
                                    for ist, soll in CAL_TAUSCH:
                                        summary = summary.replace(ist, soll)
                                    
                                    summary = summary.strip()
                                    if summary:
                                        self.event_lists[i].append((flag, summary))
                                    # cal_debug_component(component)
            except Exception as e:
                self.error = str(e)
                            
    def get_tagesliste(self, tag): 
        try:
            return self.event_lists[tag]
        except Exception:
            return []
                            

def cal_get_full_component(component):
    result = ["---------------------------\n"]
    result.append(subprint('s', component.get("summary")))
    result.append(subprint('n', component.get("name")))
    result.append(subprint('d', component.get("description")))
    result.append(subprint('o', component.get("organizer")))
    result.append(subprint('l', component.get("location")))
    result.append(subprint('von', component.decoded("dtstart")))
    result.append(subprint('bis', component.decoded("dtend")))
    return "".join(result)


def subprint(code, item):
    if item is not None:
        try:
            return f'    {code}: {item}\n'
        except Exception:
            pass
    return ""


def kuerze_eventname(eventname, eventflag, maxlen):
    if eventflag != 1:
        maxlen = int(maxlen * 0.7)
    spaceX = int(maxlen * 0.7)
    if len(eventname) > maxlen:  # # zu lange...
        indexr = eventname.rfind(" ", spaceX, maxlen)
        index = eventname.find(" ", spaceX, maxlen)
        if indexr - index > 4:
            index = indexr    # wenn mehr als ein drei-Buchstaben-Füllwort, dann lohnt es sich es mitzunehmen...
        if index > 0:      # gute Stelle mit Leerzeichen zum Abbrechen?
            eventname = eventname[:index].strip()+"…"
        else:             # dann halt radikal
            eventname = eventname[:maxlen-2].strip()+"…"
    
    return eventname


def collect(ti_cnf, anzahl):
    """ Rückgabewert: ein tupel von 2 Listen, die wiederum CalEventLists der verschiedenen Kalender enthalten
        Die erste Liste ist öffentlich, die zweite nur für Lehrer
    """
    result, resultNL = [], []
    for cobject in ti_cnf.get_calendars():
        try:  # Kompatibilität mit älterer Konfiguration ohne Farbwahl und quasi kostenfrei
            bcolor = int(cobject.bcolor) % 360
        except Exception:
            bcolor = 240 if cobject.gflag else 160
        
        if cobject.gflag:
            resultNL.append(Cal_Event_Lists(ti_cnf, anzahl, cobject.cal_id, bcolor))
        else:
            result.append(Cal_Event_Lists(ti_cnf, anzahl, cobject.cal_id, bcolor))
    return result, resultNL
    
    
def pickle_cal_lines(ti_cnf, anzahl=8):
    """ Erzeugt ein Tripel aus zwei Dicts (ohne/mit Lehrkräfteterminen) und einem Fehlerstring. 
        Die dicts enthalten  (isodatum:HTML-String), die jeweils eine Zeile mit Terminen für einen Tag repräsentieren. 
        anzahl gibt die Anzahl der zu berücksichtigen Tage an: 8 ist heute bis heutiger Wochentag nächster Woche...    
        Das Tripel wird als Datei gepickelt und von get_cal_lines zurückgeholt und übergeben.
    """
        
    evenz, evenzNL = collect(ti_cnf, anzahl)
    # NL: nur Lehrkräfte, ML: mit Lehrkräften...
    maxlen = ti_cnf.get_db_config_int('cal_eventname_maxlen', 30) 
    merke_error = ""
    lines = {}
    linesML = {}
    for i in range(anzahl):
        isodatum = ti_lib.iso_date_heute_plus_delta_days(i)
        zeile = ""
        endzeile = ""
        gelesen = []
        for eventliste in evenz:
            if not i and eventliste.error:
                merke_error += eventliste.error
            eventbcolor = eventliste.bcolor
            for eventflag, eventnamefull in eventliste.get_tagesliste(i):
                eventname = kuerze_eventname(eventnamefull, eventflag, maxlen)
                if eventname not in gelesen:
                    gelesen.append(eventname)
                    eventname = eventname.replace(" ", '&nbsp;')
                    if eventflag == 1:  # erstes Auftreten
                        zeile += f'''<span class="cal_event condensed" 
                                title="{eventnamefull}"
                                style="background-color: hsl({eventbcolor},100%,78%);">{eventname}</span> '''
                    else:
                        endzeile += f'''<span class="cal_event_multi condensed"
                                    title="{eventnamefull}"
                                    style="background-color: hsl({eventbcolor},100%,88%);">↓{eventname}</span> '''
        if zeile or endzeile:
            lines[isodatum] = f'''
                                  <span>
                                    {zeile}
                                  </span><span>
                                    {endzeile}
                                  </span>\n'''

        for eventliste in evenzNL:
            if not i and eventliste.error:
                merke_error += eventliste.error
            eventbcolor = eventliste.bcolor
            for eventflag, eventnamefull in eventliste.get_tagesliste(i):
                eventname = kuerze_eventname(eventnamefull, eventflag, maxlen)
                if eventname not in gelesen:
                    gelesen.append(eventname)
                    eventname = eventname.replace(" ", '&nbsp;')
                    if eventflag == 1:  # erstes Auftreten
                        zeile += f'''<span class="cal_event condensed italic" 
                                 title="Lehrkräfte: {eventnamefull}"
                                 style="background-color: hsl({eventbcolor},100%,80%);">L:&nbsp;{eventname}</span> '''
                    else:
                        endzeile += f'''<span class="cal_event_multi condensed italic"
                                    title="Lehrkräfte: {eventnamefull}"
                                    style="background-color: hsl({eventbcolor},100%,90%);">L↓{eventname}</span> '''
        if zeile + endzeile:
            linesML[isodatum] = f'''
                                  <span>
                                    {zeile}
                                  </span><span>
                                    {endzeile}
                                  </span>\n'''
    # return (lines, linesML, merke_error)
    with open(ti_cnf.paths.get_temp_path('cal/cal_lines.pickle'), 'wb') as f:
        pickle.dump((lines, linesML, merke_error), f)
        
    return merke_error
