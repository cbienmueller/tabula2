#!/usr/bin/python3
'''
________________________________________________________________________
 calendar.py                                                         
 This file is part of tabula.info, which is free software under           
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 calendar.py zeigt 
    * für die kommenden Tage die
    * Termine aus verschiedenen 
    * iCalendar-Quellen an
________________________________________________________________________
'''
# Batteries

# TI
import ti_lib


def manage_calendar(ti_ctx):
    cal_id = ti_ctx.req.get_value("cal_id").strip()
    if cal_id:
        url = ti_ctx.req.get_value("url").strip()
        delta_t_min = ti_ctx.req.get_value_int("delta_t_min")
        if not delta_t_min:
            delta_t_min = 60
        if delta_t_min < ti_ctx.cnf.get_db_config_int("cal_min_reload_min", 10):
            delta_t_min = ti_ctx.cnf.get_db_config_int("cal_min_reload_min", 10)
            
        gflag = 32 if ti_ctx.req.get_value("nurlehrer") else 0
        bcolor = ti_ctx.req.get_value_int("bcolor") % 360
        resultat = ti_ctx.cnf.set_calendar(cal_id, url, delta_t_min, gflag, bcolor)
        if not resultat:
            ti_ctx.set_erfolgsmeldung(f'+Kalender {cal_id} wurde {"gesetzt" if url else "gelöscht"}')
        else:
            ti_ctx.set_erfolgsmeldung(f'-Kalender nicht gesetzt: {resultat}')
            
    skeleton = f'''
      <div class="calendar" style="text-align: left;">
        {{cal_title}}
        {{cal_liste}}
        <form action="/ti/do/managepost/calendar" method="post" 
            style="text-align: left; padding-left: 0.5em;">
            ID:<input name="cal_id" value="" placeholder=" newID " type="text" size="10" maxlength="24"  class="eingabe" />
            URL:<input name="url" value="https://" type="text" size="15" maxlength="196"  class="eingabe" />
            <input value="OK" type="submit">
        </form>
       {_("""<p>Verwende für die <b>ID</b> nur Ziffern, Buchstaben und den Unterstrich.</p>
       <p>Eine leere <b>URL</b> löscht den Eintrag.</p>
       <p>Den Zeitabstand für <b>Reloads</b> in Minuten angeben.</p>
       <p>Hue ist der Winkel im Farbkreis: 0=360=Rot, 60=gelb, 180=cyan usw.</p>
       <p><b>NL</b> kennzeichnet Kalender, die nur auf Lehreranzeigen zu sehen sind.</p>""")}
       </div>
     </body>
    '''
    
    cobject_liste = ti_ctx.cnf.get_calendars()
    calliste = []
    for cobject in cobject_liste:
        if cobject.error == "neu":
            errormeldung = '''
            <div class="tooltip">
                <span class="fragezeichen"></span>
                <span class="tooltiptext">Neuer Kalender,<br>
                noch nicht abgerufen.</span>
            </div>'''
        elif cobject.error:
            errormeldung = f'''
            <div class="tooltip">
                <span class="ausrufezeichen"></span>
                <span class="tooltiptext"><b>Fehlermeldung:</b><br>
                {cobject.error}</span>
            </div>'''
        else:
            errormeldung = f'''
            <div class="tooltip">
                <span class="checkzeichen"></span>
                <span class="tooltiptext">Letzter Download erfolgreich! ({
                    ti_lib.unix_time_2_human_date_n_time(cobject.last_ts)
                    })</span>
            </div>'''
        calliste += f''' 
        <form action="/ti/do/managepost/calendar" method="post" 
            style="text-align: left; padding-left: 0.5em;">
            ID:<input name="cal_id" value="{
                cobject.cal_id
                }" type="text" size="10" maxlength="24"  class="eingabe" readonly style="color:#00f;background-color:#aaa"/>
            URL:<input name="url" value="{
                cobject.url
                }" title="{
                cobject.url
                }" type="text" size="15" maxlength="196"  class="eingabe" />
            Reload:<input name="delta_t_min" value="{
                cobject.delta_t_min
                }" type="number" size="6" maxlength="4"  class="eingabe_3ziff" />
            Hue:<input name="bcolor" value="{
                cobject.bcolor
                }" type="number" size="6" maxlength="4"  class="eingabe_3ziff" style="background-color:hsl({
                cobject.bcolor
                },100%,80%);"/>
            NL:<input name="nurlehrer" type="checkbox" {
                'checked="checked"' if (cobject.gflag == 32) else ''
                } />
            <input value="OK" type="submit">
            {errormeldung}
        </form>
        '''
    if not ti_ctx.cnf.get_db_config_bool("Flag_ShowCal", False):
        abgeschaltet = [f'''<div style="text-align:center;"><h3 style="color:red;text-align:center;"><br>{
                        _('Kalendereinträge werden nicht dargestellt')}!</h3><p>{
                        _('ShowCal ist in den Einstellungen abgeschaltet')}.</p></div>''']
    else:
        abgeschaltet = None
    md_response = ti_lib.Multi_Detail_Response([skeleton.format(
                                                    cal_title=f'''\n<h3>{_('Kalender konfigurieren')}</h3>\n''',
                                                    cal_liste=''.join(calliste))], abgeschaltet)
    return md_response

                        
