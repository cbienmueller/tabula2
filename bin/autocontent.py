'''
________________________________________________________________________
 autocontent.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 autocontent.py liefert quasi-statische Informationen wie Uhrzeit, 
 Eigenwerbung und interne Meldungen
________________________________________________________________________
'''
# Batteries

# TI
import ti
import ti_lib
import version


def get_branding(ti_ctx):
    ti_ctx.res.debug('Flag_HideTI', ti_ctx.cnf.get_db_config_bool('Flag_HideTI', False))
    if not ti_ctx.cnf.get_db_config_bool('Flag_HideTI', False):
        return version.TI_BRANDING
    return ""


def get_autocontent(ti_ctx, countdown, hochkant=False):
    # contenttime: divs sind definiert mit float:right und werden daher in umgekehrter Reihenfolge dargestellt
    if not hochkant:
        atime = f'{_("Es ist")} {ti.get_string_from_unix_time(ti_ctx.paths, dtsep=" - ")}'
    else:
        atime = ti.get_string_from_unix_time(ti_ctx.paths, middle="", dtsep=" - ")
    contenttime = f'''
        <div class="navi-div-cd" id="ti_time">{countdown}s</div>
        <div class="navi-div-zeit"           >{atime}</div>\n'''
    st_l = ti_ctx.cnf.get_db_config("SubTextL", "").strip()
    st_r = ti_ctx.cnf.get_db_config("SubTextR", "").strip()
    base_l = ""
    base_r = ""
    if ti_ctx.cnf.get_db_config_bool("Flag_IPeinblenden"):
        base_r = f'\n<div class="firstcontent">{ti_ctx.req.client.name} ({ti_lib.readable_ip(ti_ctx.req.client.ip)})</div>\n'
    if len(st_l):
        base_l = '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_l)
    if len(st_r):
        base_r += '\n<div class="firstcontent">{internalinfo}</div>\n'.format(internalinfo=st_r)

    return contenttime, base_l, base_r

