'''
________________________________________________________________________
 composer.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 composer.py stellt die generierten und (via HTML-refresh)
    einander folgenden Seiten zusammen die tabula.info ausmachen.
    Diese Funktion wird von frames.py oder ??? aufgerufen und 
    das Resultat von diesen ausgeliefert
________________________________________________________________________

 Der komplette Funktionsumfang ist nicht vollständig
    dokumentiert - er ändert sich aber meist mit jeder Version. In Kürze:
    - Zuerst wird eine Seite mit i.d.R. Meldungen (messages) und
      Personenruf (persons) dargestellt.
    - Nach z.B. 20 Sek. wird die heutige, (bzw. früheste Vertretungsplanseite
      ab heute gerechnet) dargestellt.
    - gibt es weitere Vertretungspläne, so folgen diese, bevor wieder die Startseite erscheint.
       
    Addons:
    - Es werden ggf. Infoseiten in der rechten und ggf. linken Hälfte eingeblendet
    - Es werden Meldungen nur für spezielle Anzeigen eingeblendet (extragroups)
 Wegen der großen Zahl an Fallunterscheidungen ist dieses Skript
      leider eher unübersichtlich.
      
 Datenquellen sind das /opt/tabula.info/local - Verzeichnis und die ti_ctx-Datenstruktur
 
 Zurückgegeben wird eine Liste von HTML-Strings
________________________________________________________________________

'''

# Batteries
import random

# TI
import ti_lib
try:
    import ti               # gemeinsame Funktionen und 
    import konst            # Konstanten aller Komponenten
    import pm_handling      # liefert den Personenruf und allgemeine Meldungen
    import messages         # um Spezialmeldungen abzurufen
    import infobox          # liefert die Informationsfenster, die eingeblendet werden
    import autocontent      # liefert die quasistatischen Informationen, Uhrzeit usw.
    import motto
except Exception as e:
    import ti2
    ti2.panic('composer.py scheiterte', e)
   

def compose_frames(ti_ctx,  **values):
    
    ti_ctx.res.debug_timestamp("Start Compose Frames")
    # Flags vorbelegen:
    extrastyle = ''         # css-style, wenn extrem langer Vertretungsplan oder andere Spezialitäten
    
    previewparameter = ""         # URL-Parameter, der im Previewmodus übertragen werden soll
    columnmode = 0          # dummy
    schedule_txt = 'dummy'
    schedule_navstrg = 'dummy'
    timeout = 20
    output = []                   # nimmt alle HTML-Strings in einer Liste auf

    longdelay = ti_ctx.req.get_value_bool("longdelay")  # longdelay wird immer bei manuellem Klick gesetzt
    if longdelay:
        ti_ctx.res.debug("longdelay wurde beim Aufruf gewählt! Mehr Debug-Infos:")
        ti_ctx.res.debug("URL-Parameter per WSGI", ti_ctx.req.debug_get_all_args())
        ti_ctx.res.debug("values per werkzeug-Routing", values)
        ti_ctx.res.debug("phase per werkzeug-Routing", values.get("phase"))

    # Ist überhaupt etwas anzuzeigen?
    if ist_nacht_ruhe(ti_ctx) and not longdelay:
        output.append(ti.get_html_head(ti_ctx, 
                                       timeout=120, 
                                       extrastyle='<style>body{background-color:black}</style>\n', 
                                       url="/ti/do/frames", 
                                       onclick_to='/ti/do/frames/1?longdelay=1', 
                                       tofile=True))  # wsgi liefert selbst den Content-type usw.
        output.append(konst.BLACK_SCREEN.format(random.randint(1, 1100), 
                                                random.randint(1, 800), 
                                                _("Nachtruhemodus"), 
                                                f'{ti_ctx.cnf.get_db_config("NachtruheAb")} bis ' +
                                                f'{ti_ctx.cnf.get_db_config("NachtruheBis")}', 
                                                _("Klicken zum Anzeigen")
                                                ))
        output.append(ti_ctx.get_html_tail())
        return output

    # Phasen: 0=Aushang, 1=Planseite 1 usw.
    phase = values.get("phase", 0)
    nextphase = 0
    framecounter = ti_ctx.req.get_value_int("fc")

    #######################
    # Einlesen der Parameter
    sc_code = ti_ctx.req.get_value_int('sc_code', 0)
    if sc_code == ti_ctx.cnf.get_db_config_int('sc_code'):
        clientgroups = ti_ctx.req.get_value_int('setclientgroups', 0)
        anzeigenparameter = 0
        forward_url = ''
        sc_strg = '&setclientgroups={}&sc_code={}'.format(clientgroups, sc_code)
        ti.client_force_gruppen(clientgroups)
        ti_ctx.res.debug('Set Client Groups erkannt:', clientgroups)
    else:
        clientgroups, anzeigenparameter, forward_url = ti_ctx.req.client.groups()
        sc_strg = ''
    ti_ctx.res.debug("Clientgruppen:", str(clientgroups) + " ap:" + str(anzeigenparameter) + " sc_c:" + str(sc_code))
    soloscreen = anzeigenparameter & 1
    hochkant = (anzeigenparameter & 16) > 0  # 1024>>6
    ti_ctx.res.debug("solo, hochkant", '{}, {}'.format(soloscreen, hochkant))
    to_be_forwarded = anzeigenparameter & 4
    if to_be_forwarded:
        output.append(ti.get_html_head(ti_ctx, 
                                       timeout=0, 
                                       url=forward_url if forward_url else '/ti/do/astatus', 
                                       tofile=True))  # wsgi liefert selbst den Content-type usw.
        output.append(_('Weiterleitung...'))
        output.append(ti_ctx.get_html_tail())
        return output
        
    preview = ti_ctx.req.get_value_bool("preview")  # Test auf "preview" für die Übersichtsseite im Management 
    if preview:
        extrastyle = '<style type="text/css">body, div {overflow:auto; font-size:85%; }</style>'
        previewparameter = "preview=1&"
        clientgroups = 63     # Binärer Code für alle Gruppen
        
    # Test auf Infoseite(n):
    aushang_dauer = 0
    if not ti_ctx.cnf.get_db_config_bool("Flag_ShowInfos", False):  # liest den konfigurierten Wunsch aus
        infobox_mdr = ti_lib.Multi_Detail_Response(cancel_flag=True)    # Dummy für "nicht nehmen"
    else:
        infobox_mdr = infobox.get_info(ti_ctx, mini=soloscreen)
        # is_info2show = infobox_md.ist_erfolg()
        # infostrg = infobox.get_info(ti_ctx, mini=soloscreen) 
        # wenn nicht is_info2show, dann kann immer noch ein Logo gezeigt werden
        if infobox_mdr.ist_erfolg:
            aushang_dauer = ti_ctx.cnf.get_db_config_int("AushangDauer", ti_ctx.cnf.get_db_config_int("InfoTime", 10))
    # ti_ctx.res.debug("infobox_mdr", infobox_mdr)

    # checken, ob lokales js eingebunden werden soll
    if ti_ctx.paths.get_local_path('local.js').is_file():
        local_js = f'''\n\t<script src="{ti_ctx.paths.get_local_url(local.js)}" type="text/javascript"></script>\n'''
    else:
        local_js = ''
    
    pm_a = pm_handling.pm_analyse(ti_ctx, clientgroups, hochkant)
    ti_ctx.res.debug("pm_a.spaltenzahl", pm_a.spaltenzahl)

    ti_ctx.res.debug("is_info2show", infobox_mdr.ist_erfolg())

    minphase, maxphase = ti_ctx.get_client_phaselimits()  
    ti_ctx.res.debug("Vom Admin konfigurierte phasenlimits: ", str(minphase) + ' bis ' + str(maxphase))
    if (minphase >= 0):
        allphases = list(range(minphase, maxphase + 1))
    else:  # ein negatives minphase signalisiert die Phase-Menge ohne das Intervall
        allphases = list(range(0, abs(minphase))) + list(range(abs(maxphase + 1), 99))
    # ti_ctx.res.debug("Vom Admin konf. allphases (<10): ", str(allphases))
    # allphases sind alle bisher erlaubten phase-Kandidaten, von denen nun gestrichen wird
    #########################################
    # Nun einige Entscheidungen

    # ohne Aushang fliegt dieser aus der Liste - wenn er drin steht
    if pm_a.spaltenzahl == 0 and allphases[0] == 0:
        allphases.pop(0)

    # Für konfigurierte Anzeigen:   Ist Plan 1 in der Liste, so wird morgens alles danach eliminiert
    #                               Zweck ist der Fokus auf sofort benötigte Informationen!
    if ti_ctx.req.client.istanzg and (1 in allphases) and \
            (ti.vergleiche_mit_uhrzeit(ti_ctx.cnf.get_db_config('s_any_nur_erster_plan_bis_uhrzeit')) <= 0):
        allphases = allphases[0:allphases.index(1) + 1]
        ti_ctx.res.debug('wg. Grenze nur_erster_plan_bis_uhrzeit gibt es nur einen Plan für diese Anzeige!')

    # ehemals: phase = ti_ctx.req.get_value_int("phase", -1)
    if phase < 0:
        phase = 1
        ti_ctx.res.debug("keine phase per URL")
    else:
        ti_ctx.res.debug("phase aus URL:", phase)
    if soloscreen:
        maxphase = 1
        phase = 1
        ti_ctx.res.debug('phase UND maxphase=1, da soloscreen')

    if phase not in allphases:
        phase = allphases[0]
        ti_ctx.res.debug('phase=allphases[0]=' + str(phase) + ' da phase nicht in allphases')
    # die folgende Logik, um infoseiten anzuzeigen, wird erst einmal ignoriert 2018-02-24
    # if phase>maxphase and not longdelay: # limits werden bei direktem Klick ignoriert
    #    if ( pm_spaltenzahl == 0 and not infobox_mdr.ist_erfolg()): 
    #        phase = max(minphase, 1) # mindestens erste Planseite
    #        ti_ctx.res.debug('phase=' + str(phase) + ' da phase>maxphase und keine Aushangseite')
    #    else:
    #        phase = minphase # ggf. mindestens Aushang
    #        ti_ctx.res.debug('phase=minphase=' + str(minphase) + ' da phase>maxphase')
            
    ########################################
    # Aufruf der Vertretungsplansoftware zur Analyse
    # phase, nextphase, sched_cm = ti.analyse_schedule(phase, allphases, hochkant)
    phase, nextphase, sched_cm = ti_ctx.s_interface.analyse_schedule(phase, allphases, hochkant)
    ti_ctx.res.debug("Vertretungsplananalyse: phase", phase)
    ti_ctx.res.debug("Vertretungsplananalyse: nextphase", nextphase)
    if phase < allphases[0] and not longdelay:  # limits werden bei direktem Klick ignoriert
        ti_ctx.res.debug("trotz Vertretungsplananalyse: phase ist kleiner allphases[0]:", allphases[0])
        phase, nextphase, sched_cm = ti_ctx.s_interface.analyse_schedule(allphases[0], allphases, hochkant)
    ########################################
    # Festlegung, was angezeigt wird
    if phase < 0:  # kein schedule verfügbar
        ti_ctx.res.debug('Festlegung: Kein Schedule verfügbar')
        phase = 0
        
    elif phase == 0 and ((pm_a.spaltenzahl == 0 and not infobox_mdr.ist_erfolg()) or allphases[0] > 0): 
        # also keinerlei Meldungen, Personenrufe und Infos
        if not allphases[0]:
            ti_ctx.res.debug('Festlegung: Aushang angefordert - es gibt aber keinen - daher phase>0')
        else:
            ti_ctx.res.debug('Es soll kein Aushang angezeigt werden')
        phase = max(allphases[0], 1)
                
    ########################################### 
    # Vertretungsplan eines Tages holen, denn jetzt liegt phase fest vor
    schedule_txt, extrastyle, columnmode, schedule_navstrg, timeout = \
        ti_ctx.s_interface.compose_schedule(phase, allphases, extrastyle, sc_strg, hochkant)
    ti_ctx.res.debug("Vertretungsplancompose: columnmode", columnmode)
    if not columnmode:
        phase = 0  # ist ja nichts anzuzeigen
    #############################################
    # Anzeigedauer berechnen - nach Ergebnissen
    # ggf. nextphase anpassen
    if phase >= allphases[-1]:   
        nextphase = allphases[0]         # Damit's danach direkt wieder neu losgehen kann
    if not phase:
        timeout = aushang_dauer + int(pm_a.timeout_s)
                
    if clientgroups & 3:
        timeout += 10
    if clientgroups & 12:
        timeout += 5
            
    if soloscreen or \
       ((phase == 1 and nextphase == 0 and pm_a.spaltenzahl < 2) and columnmode == 1):
        pm_muenze = (framecounter % 2) == 0 and pm_a.spaltenzahl
        nextphase = 1
        if pm_muenze:
            timeout += int(pm_a.timeout_s)
        else:
            timeout += aushang_dauer
        ti_ctx.res.debug("Keine nextphase=0 sondern 1, da Aushang neben Plan passt! Diesmal: {}".format(
            "Aushang" if pm_muenze else "Infoseite"))   
    else:
        pm_muenze = 0
        
    #############################################
    # Anzeigedauer berechnen - höhere Mächte
    if longdelay and timeout < 123:
        timeout = 123
    elif soloscreen:
        timeout = max(60, min(timeout, konst.TI_MAXTIME))
    else:
        timeout = max(10, min(timeout, konst.TI_MAXTIME))  # gewisse Grenzen einhalten
    ti_ctx.res.debug("timeout", str(timeout))
        
    ################################
    # Nun die eigentliche Ausgabe: #
    # ##############################
    # ################
    # header schreiben

    output.append(ti.get_html_head(
        ti_ctx, 
        url=f'/ti/do/frames/{str(nextphase)}?{previewparameter}fc={str((framecounter + 1) % 10)}{sc_strg}', 
        extrastyle=extrastyle, 
        timeout=str(timeout), 
        onload='ti_start()', 
        title=_("tabula.info - Bitte Vollbildschirm wählen"), 
        tofile=True))
                    
    ##################################################
    # Navigationsleiste  & weiterer Content
    if soloscreen:
        autotime, autobase_l, autobase_r = '', '', ''
        
        rechtervorhang = f'''
        <div id="rechtervorhang">
        {pm_a.spalte1 if pm_a.spaltenzahl else ""}
        <div style="height:5px;font-size=1px;">&nbsp;</div>
        {infobox_mdr.abschnitt_to_string(0)}
        </div>'''
                                        
        output.append('<div id="solocontent">')
    else:
        autotime, autobase_l, autobase_r = autocontent.get_autocontent(ti_ctx, ("00" + str(timeout))[-3:], hochkant)
        rechtervorhang = ''
        
        # Unterscheidung "darstellung" für Lage der Navigation
        darstellung = '2' if ti_ctx.cnf.get_db_config_bool('Flag_Navi_unten') else ''
        navstrg = '\n<div id="main_navigation{}"><div class="navi-div-links">'.format(darstellung)
        if not phase:
            titistyle = ' class="main_navigation{}_selected" '.format(darstellung)
        else:
            titistyle = ' class="main_navigation{}_hide" '.format(darstellung) if 0 not in allphases else ''  # war phase statt 0
        tititle = ti_ctx.cnf.get_db_config('TI_Title').strip()
        navstrg += f'<a href="/ti/do/frames/0?longdelay=1{sc_strg}"> <span {titistyle}>{tititle}</span>&nbsp;</a></div>'
        navstrg += '<div class="navi-div-rechts">&nbsp;'
        if schedule_navstrg != "no schedule":
            if schedule_navstrg.startswith("keine"):
                navstrg += '<small> - </small>'
            else:
                navstrg += "&nbsp;{}&nbsp;{}".format(_('Pläne:'), schedule_navstrg)
        navstrg += '</div>'

        # Entscheidung, ob Navigationszeile dargestellt wird
        if not ti_ctx.cnf.get_db_config_bool('Flag_Hide_Navi'):
            output.append(navstrg)
            output.append(autotime)
            output.append('\n</div> <!-- Ende Navigation -->\n<div id="main_content{}">'.format(darstellung))

    # Extra-Meldungsbereiche und Termintabelle vorbereiten
    q11 = ''
    q12 = ''

    ####################################################################
    # Schedule schreiben
    if phase:
        output.append(schedule_txt)
        if columnmode == 1 and not hochkant:
            output.append('<div id="column2_2">')
            if soloscreen or (phase == 1 and nextphase != 2 and pm_a.spaltenzahl < 2):
                if pm_a.spaltenzahl and (soloscreen or pm_muenze):
                    output.append(pm_a.spalte1)
                    if pm_a.miniflag1:
                        output.append(infobox_mdr.abschnitt_to_string(0))
                if soloscreen or not pm_muenze:
                    output.append(infobox_mdr.abschnitt_to_string(0))
                rechtervorhang = ''
                # nextphase=1 ist mit gleichen Bedingungen bereits weiter oben gesetzt
            elif not infobox_mdr.ist_erfolg and pm_a.spaltenzahl:
                output.append(pm_a.spalte1)
                if pm_a.miniflag1:
                    output.append(infobox_mdr.abschnitt_to_string(0))
            else:
                output.append(infobox_mdr.abschnitt_to_string(0))
            output.append('</div> <!-- Ende Schedule C1 -->')
 
    ####################################################################
    # allgemeine Meldungen und Personenruf schreiben
    infostrg_left = ''
    if not phase:  # nullte Phase
        # wenn links fast nix anzuzeigen ist, rechts aber eine Infoseite kommt, 
        #  dann schauen, ob für links eine zweite Infoseite mitgegeben wurde
        ti_ctx.res.debug("infobox_mdr.abschnittanzahl()", infobox_mdr.abschnitt_max())
        if infobox_mdr.ist_erfolg() and pm_a.miniflag1 and infobox_mdr.abschnitt_max() >= 1: 
            infostrg_left = infobox_mdr.abschnitt_to_string(1) \
                            if ti_ctx.cnf.get_db_config_bool("Flag_ShowMultiInfos", False) else ""
                
        if hochkant:
            output.append('''
            <div id="column1_1" class="scrollcontent" style="overflow: scroll">
                <div>
                {}
                {}
                </div>
            </div> <!-- Ende Spalte 1-->\n'''.format(
                pm_a.spalte1 if pm_a.spaltenzahl
                else ('<span style="color:white;">' + _('Keine Meldungen anzuzeigen') + '</span>'), 
                infobox_mdr.abschnitt_to_string(0)))
        
        else:
            output.append('''
            <div id="column1_2" class="scrollcontent" style="overflow: scroll">
                <div>
                {}
                {}
            </div></div> <!-- Ende Spalte 1-->\n'''.format(
                pm_a.spalte1 if pm_a.spaltenzahl else '', 
                infostrg_left if infostrg_left else ''))         
            if pm_a.spaltenzahl > 1 or infobox_mdr.ist_erfolg:
                output.append('''
            <div id="column2_2" class="scrollcontent" style="overflow: scroll">
                <div>
                {} <!-- ggf. Ende Messages -->
                {} <!-- ggf. Ende Info -->
            </div></div> <!-- Ende Spalte 2-->\n\n'''.format(
                    pm_a.spalte2 if pm_a.spaltenzahl > 1 else '', 
                    infobox_mdr.abschnitt_to_string(0) if 
                    (pm_a.spaltenzahl < 2 or pm_a.miniflag2) else ''))
        
    ###########################
    # # linkes Infofeld schreiben
    leftbasetext = []

    # zuerst Messages mit Bits "1" "4" und "16" gesetzt
    for cg in [1, 4, 16]:
        if clientgroups & cg:
            leftbasetext.append(messages.create_messages(ti_ctx, cg, (q11 if cg == 1 else "")))

    if autobase_l:
        leftbasetext.append(autobase_l)

    if leftbasetext:  # ganzes linkes Infofeld
        output.append('<div id="leftbase" title="Klick zum Ausblenden">' + "\n".join(leftbasetext) + '</div>')

    ##########################
    # # Branding
    output.append(autocontent.get_branding(ti_ctx))

    ###########################
    # # rechtes Infofeld schreiben
    # Extrameldungen für Oberstufe, Lehrerzimmer, Motto

    rightbasetext = []
    # zuerst Messages mit Bits ... gesetzt
    for cg in [2, 8, 32]:
        if clientgroups & cg:
            rightbasetext.append(messages.create_messages(ti_ctx, cg, (q12 if cg == 2 else "")))

    if (not clientgroups & 3) or preview:
        rightbasetext.append(motto.get_motto(ti_ctx))

    if autobase_r:
        rightbasetext.append(autobase_r)

    if rightbasetext:  # ganzes rechtes Infofeld
        output.append('<div id="rightbase" title="Klick zum Ausblenden">' + "\n".join(rightbasetext) + '</div><!-- Ende Rightbase -->')
        
    if rechtervorhang:
        output.append(rechtervorhang)

    # den Content abschließen
    output.append('</div><!-- Ende Content -->')
    if local_js:
        output.append(local_js)
    ti_ctx.res.debug_timestamp("Ende Compose Frames")
    
    ti_ctx.res.debug("Outputlen", len(output))
    
    output.append(ti_ctx.get_html_tail())
    
    return output  # eine komplette Liste von HTML-Strings - von docstring bis zu den debug-Meldungen
    

def ist_nacht_ruhe(ti_ctx):
    """Gibt True zurück, wenn es später als nacht_ruhebeginn oder früher als nacht_ruheende ist"""
    uhrzeit = ti_lib.get_akt_zeit()  # als bis zu vierstelliger integer hhmm
    nacht_ruhe_ab = ''.join(ti_ctx.cnf.get_db_config("NachtruheAb").split(':'))
    nacht_ruhe_bis = ''.join(ti_ctx.cnf.get_db_config("NachtruheBis").split(':'))
    if not (len(nacht_ruhe_ab) == 4 and nacht_ruhe_ab.isdigit()):
        nacht_ruhe_ab = "22:00"
        ti_ctx.cnf.set_db_config("NachtruheAb", nacht_ruhe_ab)
        nacht_ruhe_ab = "2200"
    if not (len(nacht_ruhe_bis) == 4 and nacht_ruhe_bis.isdigit()):
        nacht_ruhe_bis = "06:00"
        ti_ctx.cnf.set_db_config("NachtruheBis", nacht_ruhe_bis)
        nacht_ruhe_bis = "0600"
    ab = int(nacht_ruhe_ab)
    bis = int(nacht_ruhe_bis)
    # ti_ctx.res.debug("Nachtruhe?", nacht_ruhe_ab, nacht_ruhe_bis, ab, bis, uhrzeit)
    if uhrzeit >= ab or uhrzeit <= bis:
        return True
    return False
    
    
if __name__ == '__main__':
    ti_lib.quickabort2frames()
