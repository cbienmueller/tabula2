'''
________________________________________________________________________
 frames.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 DIES IST NUR NOCH EIN DUMMY FÜR UPGRADES ÄLTERER SYSTEME
 
 frames.py lieferte die von composer.py generierten Seiten, welche
    tabula.info ausmachen, per C G I aus.
    Nur diese Seite wurde von den Browsern zur Darstellung aufgerufen!                                         
________________________________________________________________________

'''

# Batteries (none))
 
# TI
import ti_lib

if __name__ == '__main__':
    ti_lib.quickabort2frames()

# & tschüß
