'''
________________________________________________________________________
 s_turbo_userinput.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 s_turbo_userinput.py nimmt POSTs entgegen und 
    trägt sinnvolle Eingaben in eine Datenbank ein. 
    
    Außerdem liefert es an s_turbo auf Anfrage die Info, 
    in welchen Kommentarfeldern Markierungen eingeblendet werden sollen (AiM)
________________________________________________________________________
'''

# Batteries
import json

# TI
import ti
import ti_lib
import s_turbo_userinput_db

markierungsauswahl = ['nix', 'aim', 'abs']

def do_input(request, ti_ctx):
    tuidb = s_turbo_userinput_db.TurboUInput_DB(ti_ctx.paths)

    """ Der Input besteht aus mehreren Werten:
        tidate: ds.uitidate,
        person: ds.uiperson,
        stunde: ds.uistunde,
        wahl: einArray[0],
        code: einArray[1] };

    """
    resultat = {}
    # Vorläufige Werte:
    resultat["Ergebnis"] = 'Eintrag gescheitert'
    resultat["ErgebnisFarbe"] = 'pink'
    # if request.scheme!="https":
    #    resultat["Ergebnis"] = f'Eingaben nur über verschlüsselte Verbindung zulässig. (war {request.scheme})'
    #    return json.dumps(resultat) 
    jo = request.get_json()
    ti_ctx.res.debug(str(jo))
    tidate = jo.get('tidate')
    try:
        stunde = int(jo.get('stunde'))
    except Exception:
        stunde = 0
    person = jo.get('person')
    wahl = jo.get('wahl').lower()
    code = jo.get('code').upper()
        
    vonperson = ""

    if (tidate < str(ti.get_todays_tidate()) or tidate > str(ti.get_todays_tidate() + 15)):
        resultat["Ergebnis"] = f'Datum nicht in den nächsten Tagen: {tidate}'
        return json.dumps(resultat) 
    
    if (stunde <= 0 or stunde > 19):
        resultat["Ergebnis"] = 'Stunde falsch!?'
        return json.dumps(resultat) 
    
    if wahl not in markierungsauswahl:
        resultat["Ergebnis"] = 'Gewünschte Markierung existiert nicht!\nWähle AiM oder AbS'
        return json.dumps(resultat) 
        
    now = ti_lib.get_now_unix_time_int() 
    ad_code, ad_failed, ad_locktime = tuidb.get_user("admin")
    sp_code, sp_failed, sp_locktime = tuidb.get_user("stupla")
    db_code, db_failed, db_locktime = tuidb.get_user(person)
    if db_locktime > now:
        # bewusst kein db-write, um DOS zu verhindern
        resultat["Ergebnis"] = 'Sperre! Probiere es in 5 Minuten wieder.'
        return json.dumps(resultat) 
    
    if (not db_code or code != db_code) and \
       (not ad_code or code != ad_code) and \
       (not sp_code or code != sp_code):
        if db_failed < 10:
            tuidb.set_userfailed(person, db_failed + 1, 0)
            resultat["Ergebnis"] = 'Code nicht akzeptiert'
        else:
            tuidb.set_userfailed(person, 0, now + 300)
            resultat["Ergebnis"] = 'Sperre! Probiere es in 5 Minuten wieder.'
        return json.dumps(resultat) 
    
    # ok, klingt gut, let's do it
    if code == ad_code:
        vonperson = "Verwaltung"
    elif code == sp_code:
        vonperson = "Stundenplan"
    markierung = markierungsauswahl.index(wahl) 
    tuidb.set_markierung(tidate, stunde, person, markierung, vonperson)
    ti_ctx.cnf.set_db_config('s_any_force_convert', 1)
    
    resultat["Ergebnis"] = f'OK'
    resultat["ErgebnisFarbe"] = 'lightgreen'
    return json.dumps(resultat) 
