'''
________________________________________________________________________
 messages.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 messages.py definiert u.a. das message_obj und gibt die Liste der 
 anzuzeigenden Meldungen als HTML-Code zurück. Seit 2012-05-12 werden die
 Messages in einer sqlite3-Datenbank gespeichert
________________________________________________________________________
'''

# Batteries

# TI
import ti
import ti_lib
import konst
import cal_view


def create_messages(ti_ctx, extramode=0, zusatzmeldung=""):
    manage_mode = ti_ctx.req.mess_manage_mode
    
    message = ""
    # if not extramode:
    #    if manage_mode:
    #        message += '<div class="firstmgmcontent" style="margin-bottom:0.5em"><h1>Meldungen verwalten 1</h1></div>\n'
    if manage_mode:
        message += '''<div id="column1_2">\n'''
    normal, extra = iterate_messages(ti_ctx, extramode, zusatzmeldung=zusatzmeldung)
    message += normal
    if manage_mode:
        message += f'''
        </div><!-- Ende von column1_2 -->
        <div id="column2_2">
            {extra}
        </div>'''
    return message
    
    
def print_messages_barebone(ti_ctx):
    manage_mode = ti_ctx.req.mess_manage_mode
    
    if manage_mode:
        mode = ' managen'
        morestyles = '<style type="text/css">body { background-color:#FBB; }</style>\n'
    else:
        mode = ''
        morestyles = ""
    ti_ctx.print_html_head(title="tabula.info - Meldungen"+mode, extrastyle=morestyles, ismanagement=manage_mode)
    

def indexfirstbit(number):
    if number > 0:
        for i in range(8):
            if number & (1 << i):
                return i
    else:
        return -1
        

def iterate_messages(ti_ctx, extragroup=0, zusatzmeldung=''):
    manage_mode = ti_ctx.req.mess_manage_mode
    ist_lehrer = ti_ctx.req.client.ist_lehrer()
    
    h4strg = '<h4 class="lowmargin">' if manage_mode else '<h4>'
    pstrg = '<p class="lowmargin">' if manage_mode else '<p>'
    
    today = ti.get_todays_tidate()
    akt_hour = ti.get_akt_hour(today)
    nomessages = True
    extracount = 0  # zählt die Extrameldungen
    extragroupindex = indexfirstbit(extragroup)
    
    cal_linesAllgemein, cal_linesML, merke_error = cal_view.get_cal_lines(ti_ctx)
    if merke_error:
        ti_ctx.res.debug("Fehler beim Kalendereinlesen: ", merke_error)
    
    cal_lines = cal_linesML if ist_lehrer else cal_linesAllgemein
    
    itmess = '''
      <div class="messages brownborder">
        '''
    count_deleted = 0
    itzusatz = ''
    managestr = ''
    if manage_mode:
        # eigene Kopfzeilen für den Editbereich
        itzusatz += '''
        <div class="messages">'''
        # Zusatzkopfzeile für die Messages
        itmess += f'''
        <h3>{_('Aktuell angezeigte Meldungen')}:</h3>
        '''
        # Zusatzstring, der zu jeder Message kommt, damit man sie editieren/löschen kann
        managestr = '''
        <td style="width:5em;border:solid 1px #AAA;border-right:0px;padding:3px;" >
            <a href="/ti/do/manage/messages?number_2_delete={0}&todo=edit">
                <img src="/ti/static/pix/edit.png" align="top" width="150" height="19" border="0" alt="bearbeiten">    </a><br>
            <a href="/ti/do/manage/messages?number_2_delete={0}&todo=delete">
                <img src="/ti/static/pix/erase.png" align="bottom" width="150" height="19" border="0" alt="loeschen">  </a>
                    
        </td>
        '''
        # entsprechender Zusatzstring, nur für bereits gelöschte Nachrichten 
        remanagestr = '''
        <td style="width:5em;border:solid 1px #AAA;border-right:0px;padding:3px;" >
            <a href="/ti/do/manage/messages?number_2_delete={0}&todo=edit">
                <img src="/ti/static/pix/edit.png" align="top" width="150" height="19" border="0" alt="bearbeiten">    </a><br>
            <small style="background:none;">(wird nachts entfernt)</small>
                    
        </td>
        ''' 
        
    if extragroup:
        day_or_flag_list = [TI_EXTRAMELDUNG]
    else:
        # tricky, um wochentagslose Meldungen an den Start und Ende sowie gelöschte Meldungen ganz an's Ende zu bekommen:
        day_or_flag_list = [TI_SONDERMELDUNG]
        for i in range(18):
            day_or_flag_list += [ti.get_following_worktidate(i)]
        day_or_flag_list += [TI_INFOMELDUNG]
        if manage_mode:
            day_or_flag_list += [TI_EXTRAMELDUNG, TI_DELETED]  # gelöschte Nachrichten
    erster_tag = True      # geplanter Ersatz für schonbeendet
    for day_or_flag in day_or_flag_list:
        daydate = ''
        event_zeile = ''
        folgemeldung = False
        if day_or_flag == TI_SONDERMELDUNG:   
            contenttype = "dauer"
            dayname = "X"
            dayname = "X"
            folgemeldung = True                              # ohne Rubriküberschrift
        elif day_or_flag == TI_DELETED:
            contenttype = "erased"
            dayname = f"<small>{_('gelöscht')}</small>"
        elif day_or_flag == TI_EXTRAMELDUNG:
            contenttype = "content"
            dayname = '?'
            # dayname = 'tiextracount '+("Extrameldungen" if (manage_mode) else '')
        elif day_or_flag == TI_INFOMELDUNG: 
            contenttype = "dayless"
            dayname = "<large>!</large>"
        else:  # if day_or_flag == firstday or day_or_flag == nextday:
            dayname, daydate = ti.get_string_from_tidate(day_or_flag).split(", ")
            isodatum = ti_lib.tidate_2_isodatum(day_or_flag)
            try:  # muss ja nicht jedes Datum vertreten sein
                event_zeile = f'<div class="cal_line">{cal_lines[isodatum]}</div>'
            except Exception:
                event_zeile = ""
            dayname = dayname[:2]
            if day_or_flag == today:  
                contenttype = "content"
                daydate += f"<br>({_('heute')})"
            elif day_or_flag == today+1:
                contenttype = "content"
                daydate += f"<br>({_('morgen')})"
            else:
                contenttype = "content" \
                    if day_or_flag == today + 2 and akt_hour >= ti_ctx.cnf.get_db_config_int("PrioWechselStunde", 7) \
                    else "loprio"

        i = -1
        if (day_or_flag > 0 and day_or_flag < 20):
            # Also spezielle, nicht nach Tagen gestaffelte Anzeigen -> benötige alle Tage
            mob_array = mdb_get_messages(ti_ctx, day_or_flag, targetday=today, 
                                         lasthour=akt_hour, target=extragroup if not (manage_mode) else -1)
        else:
            # Anzeige für nur einen Tag, also nur TI_ACTIVE
            last_hour = akt_hour if day_or_flag == today else 0
            # ti_ctx.res.debug("last_hour", last_hour)
            mob_array = mdb_get_messages(ti_ctx, TI_ACTIVE, day_or_flag, lasthour=last_hour, target=extragroup)
        
        # ## jetzt liegen alle anzuzeigenden Meldungen dieses Tags bzw. Flags in mob_array
        erstemeldung = True
        # ti_ctx.res.debug("DAS IST dayname", dayname)
        
        if (event_zeile or mob_array) and dayname != '?':
            # Wir beginnt ein normaler Tagesmeldungsblock
            if not erster_tag:
                if dayname == "Mo":
                    itmess += '''
                        <hr style="height:3px; background-color:grey; border-color:grey;">\n'''
                else:
                    itmess += '''
                        <hr>\n'''
                
            else:
                erster_tag = False
            if mob_array:
                spanklasse = 'tagspan'
                machmini = ''
            else:
                spanklasse = ''  # minitagspan'
                machmini = ' condensed'
            if event_zeile and dayname != "X":
                datumklasse = 'datumdiv' + machmini  # if not mentry.flag == TI_DELETED else 'breitdiv'
                itmess += f'''
            <div class="{datumklasse}">
                <span class="{spanklasse}">{dayname}</span>{'<br>' if mob_array else ' '}<small>{daydate}</small>
            </div>
            <div class="meldungdiv">\n'''
                erstemeldung = False
                nomessages = False

        for mentry in mob_array:  # jetzt werden die passenden Meldungen abgeklappert
            # ti_ctx.res.debug("DAS IST mentry", mentry)
            mentry_output = []
            if dayname == '?':
                # jetzt wird ein anderer Block begonnen
                extraanzahl = ''
                if manage_mode and mentry.flag == TI_EXTRAMELDUNG:
                    if mentry.target > 0:     # sollte immer true sein...
                        targetnumber = indexfirstbit(mentry.target)
                        gn, gk = ti_ctx.cnf.get_gruppenbez(targetnumber)  # GruppenName, GruppenKürzel
                    else:
                        gn = '!!'
                else:
                    gn, gk = ti_ctx.cnf.get_gruppenbez(extragroupindex)  # GruppenName, GruppenKürzel
                    dayname = gn
                    if len(mob_array) > 1:
                        extraanzahl = '<br><small>'+str(len(mob_array))+'<small> Meld.</small></small>'
                    erstemeldung = False
                mentry_output.append(f'''
                    <div class="datumdiv">
                        <b>{gn}</b>{extraanzahl}
                    </div>
                    <div class="meldungdiv">\n''')

            else:
                if erstemeldung:
                    erstemeldung = False
                    
                    # if schonbeendet:
                    #     mentry_output.append('''
                    # <hr>\n''')

                    if dayname != "X":
                        datumklasse = 'datumdiv' if not mentry.flag == TI_DELETED else 'breitdiv'
                        mentry_output.append(f'''
                    <div class="{datumklasse}">
                        <span class="tagspan">{dayname}</span><br><small><small>{daydate}</small></small>
                    </div>
                    <div class="meldungdiv">\n''')
                        if event_zeile:
                            mentry_output.append(f'''{event_zeile}\n''')
                    else: 
                        mentry_output.append('<div>')

            einzelnachricht = ''
            i += 1
            if mentry.target > 0:
                extracount += 1

            if mentry.color == "red" and mentry.flag == TI_SONDERMELDUNG:
                einzelnachricht += '\t\t\t\t\t<div class="alarm"><table cellspacing="0" style="padding:1px"><tr>\n'
            elif mentry.color == "red" and mentry.flag in [TI_ACTIVE, TI_EXTRAMELDUNG]:
                einzelnachricht += '\t\t\t\t\t<div class="hiprio"><table cellspacing="0" style="padding:1px"><tr>\n'
            else:
                einzelnachricht += '\t\t\t\t\t<div class="'+contenttype+'"><table cellspacing="0" style="padding:1px"><tr>\n'
            
            if manage_mode:
                if mentry.flag == TI_DELETED:
                    einzelnachricht += remanagestr.format(str(mentry.rowid))
                else:
                    einzelnachricht += managestr.format(str(mentry.rowid))
                    
                # ti_debug("managestrg", managestr.format(str(i)))
                
            # gib nun endlich die eigentliche Meldung aus
            # gib nur bei Bedarf den zugehoerigen Kommentar aus
            
            # Quelle angeben?
            if (not mentry.flag == TI_SONDERMELDUNG) or manage_mode:
                author_strg = ' <small><small>('+mentry.author+')</small></small>'
            else:
                author_strg = ''
            if mentry.targetday != today:
                zielwochentag = ti.get_dayname_from_tidate(mentry.targetday)
                mentry.headline = ersetze_heute(mentry.headline, zielwochentag)
                mentry.details = ersetze_heute(mentry.details, zielwochentag)
            # Details einblenden?
            stil = ' style="border:solid 1px #AAA;border-left:0px;padding:2px;"' if manage_mode else ""
            if len(mentry.details) > 1:
                einzelnachricht += \
                    f'\t\t\t\t\t\t<td nobord="true"{stil}>{h4strg}{mentry.headline}</h4>\n'
                einzelnachricht += \
                    f'\t\t\t\t\t\t{pstrg}{mentry.details}{author_strg}</p>\n'
            else:
                einzelnachricht += \
                    f'\t\t\t\t\t\t<td nobord="true"{stil}>{h4strg}{mentry.headline}<small>{author_strg}</small></h4>\n'
            if manage_mode and mentry.flag in [TI_EXTRAMELDUNG, TI_SONDERMELDUNG, TI_INFOMELDUNG]:
                ti_ctx.res.debug("2022-Mai: mentry.flag=", mentry.flag, "mentry.target", mentry.target)
                if mentry.target > 0:     # sollte true sein für Q-Stufen usw.
                    targetnumber = indexfirstbit(mentry.target)
                    gn, gk = ti_ctx.cnf.get_gruppenbez(targetnumber)  # GruppenName, GruppenKürzel
                    einzelnachricht += f''' -> Bis {
                                        ti.get_daydate_from_tidate(mentry.targetday)} ({
                                        mentry.lasthour}. Std.) für {gn}.'''
                else:
                    einzelnachricht += f''' -> Bis {
                                        ti.get_daydate_from_tidate(mentry.targetday)} ({
                                        mentry.lasthour}. Std.)'''
                
            einzelnachricht += '\t\t\t\t\t\t</td></tr></table>\n\t\t\t\t\t</div><!-- Ende Einzelnachricht -->\n'
            mentry_output.append(einzelnachricht)
               
            nomessages = False
            if dayname == "?":
                mentry_output.append(f'''
                </div> <!-- Ende von {dayname} -->
                <div class="enddiv"></div>\n''')
            
            if day_or_flag == TI_DELETED:
                itzusatz += ''.join(mentry_output)
                count_deleted += 1
            else:
                itmess += ''.join(mentry_output)
        
        if not (erstemeldung or dayname == "?"):  # ggf. mit '<br>' if...
            itmess += f'''{'<br>' if (mob_array and event_zeile and contenttype=='content') else ""}{event_zeile} 
                </div> <!-- Ende von "Tag" {dayname} -->
                <div class="enddiv"></div>\n'''
            # schonbeendet = True
        if itzusatz and day_or_flag == TI_DELETED: 
            itzusatz += '''
                </div> <!-- Ende von "gelöscht" -->
                <div class="enddiv"></div>\n'''
    # End of iterating day_or_flags 
    if zusatzmeldung:
        if not folgemeldung:
            itmess += '<h3>'+dayname+'</h3>\n'
        nomessages = False
        itmess += zusatzmeldung
        extracount += 1
    
    itmess += "     </div><!-- Ende iterate itmess -->"
    if count_deleted:
        if itzusatz:
            itzusatz += "   </div><!-- Ende iterate itzusatz -->"
    else:
        itzusatz = ""
    
    if nomessages and not manage_mode: 
        return '', ''
        
    return itmess, itzusatz
    

def ersetze_heute(text, wotag):
    pos = text.find("heute")
    while pos >= 0:
        if not ((pos > 0 and text[pos-1].isalpha()) or (pos < len(text) - 5 and text[pos + 5].isalpha())):
            text = text[:pos]+wotag+text[pos+5:]
        pos = text.find("heute", pos+1)
        
    pos = text.find("Heute")
    while pos >= 0:
        if not ((pos > 0 and text[pos-1].isalpha()) or (pos < len(text) - 5 and text[pos + 5].isalpha())):
            text = text[:pos]+wotag+text[pos+5:]
        pos = text.find("Heute", pos+1)
        
    return text
    

def finish_messages():
    return "</body></html>"


#######################################
# Messages als Objekt (eher als struct) verwalten
# FIXME umbau zu dataobject bietet sich an...
class MessageObj(object):

    def __init__(self, 
                 rowid=0, 
                 flag=0, 
                 targetday=0, 
                 created=0, 
                 deleted=0, 
                 lasthour=99, 
                 target=0, 
                 headline="", 
                 details="", 
                 color="", 
                 author=""):
        self.rowid = int(rowid)           # Autoincrement-Feld in SQLite3
        self.flag = int(flag)             # Statusfeld für TI 
        self.targetday = int(targetday)   # INT, TI-Tagesformat
        self.created = int(created)       # Sekunden seit epoche (unix_time)
        self.deleted = int(deleted)       # Sekunden seit epoche
        self.lasthour = int(lasthour)     # Unterrichtsstunde, bis zu der (einschließlich) die Meldung gezeigt wird
        self.target = int(target)         # Zielgruppennummer
        self.headline = headline          # Schlagzeile
        self.details = details            # Details
        self.color = color                # Farbe
        self.author = author              # ErstellerKürzel, notfalls IP-Adresse
        

def mdb_get_messages(ti_ctx, flag=0, targetday=0, lasthour=0, target=0):
    '''gibt alle zu den Parametern passenden Messages als Objekt-Array zurück'''
    cursor = ti_ctx.sys.cdb_conn.cursor()
    # lese Meldungen für einen Tag (targetday) ein
    if flag == TI_ACTIVE:
        cursor.execute("""
            SELECT rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author 
            FROM messages
            WHERE flag=? AND targetday=? AND lasthour >= ? AND target=?
            ORDER BY color, lasthour""", (flag, targetday, lasthour, target))
    # lese Meldungen für Sonderziel "target" ein ab dem targetday (außer am targetday ist lasthour vorbei)
    elif target >= 0:
        cursor.execute("""
            SELECT rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author 
            FROM messages
            WHERE flag=? AND ( (targetday=? AND lasthour >= ?) or targetday>?) AND target=?
            ORDER BY color, lasthour""", (flag, targetday, lasthour, targetday, target))
    # lese alle Meldungen ab targetday ein (außer am targetday ist lasthour vorbei)
    else:
        cursor.execute("""
            SELECT rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author 
            FROM messages
            WHERE flag=? AND ( (targetday=? AND lasthour >= ?) or targetday>?) 
            ORDER BY color, lasthour""", (flag, targetday, lasthour, targetday))
    mo_array = []
    for row in cursor:
        rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author = row
        mobj = MessageObj(rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author)
        mo_array.append(mobj)
    cursor.close()
    return mo_array


def mdb_get_message(ti_ctx, rowid):
    '''gibt eine Meldung zurück'''
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""
            SELECT rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author 
            FROM messages WHERE ROWID=? """, (str(rowid), ))
    mobj = None
    ti_ctx.res.debug("Das ist der Cursor", cursor)
    for row in cursor:
        rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author = row
        mobj = MessageObj(rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author)
    cursor.close()
    return mobj


def mdb_write_message(ti_ctx, rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author):
    '''rowid < 0 bedeutet neue Message, sonst update der mit dieser rowid'''
    cursor = ti_ctx.sys.cdb_conn.cursor()
    ti_ctx.res.debug("mdb_write_message:", 
                     (rowid, flag, targetday, created, deleted, lasthour, target, headline, details, color, author))
    if rowid >= 0:
        cursor.execute("""UPDATE messages 
            SET flag=?, targetday=?, created=?, deleted=?, lasthour=?, target=?, headline=?, details=?, color=?, author=?  
            WHERE rowid=?""", (flag, targetday, created, deleted, lasthour, target, headline, details, color, author, rowid))
    else:
        cursor.execute("""INSERT INTO messages 
                    (flag, targetday, created, deleted, lasthour, target, headline, details, color, author) 
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", 
                       (flag, targetday, created, deleted, lasthour, target, headline, details, color, author))
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()


def mdb_uncolor(ti_ctx, flag, targetday):
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute('''UPDATE OR IGNORE messages SET color="x" WHERE flag=? AND targetday=?''', (flag, targetday))
    cursor.close()
    

def mdb_delete_message(ti_ctx, rowid):
    headline = 'keine'
    cursor = ti_ctx.sys.cdb_conn.cursor()
    cursor.execute("""SELECT headline FROM messages WHERE rowid=?""", (rowid, ))
    for row in cursor:
        headline = row[0] 
    cursor.execute("""UPDATE messages SET flag=?, deleted=? WHERE rowid=?""", (TI_DELETED, ti_ctx.sys.get_unix_time(), rowid))
    cursor.close()
    ti_ctx.sys.cdb_conn.commit()
    return headline


def do_show_man_messages(ti_ctx): 
    """... wird direkt von man.py aufgerufen um die Meldungen im ManagementBereich anzuzeigen.
    
    neue Funktion Nov 2020"""
    if not ti_ctx.check_management_rights(konst.RECHTEMeldungen | konst.RECHTEMeldungenerweitert):
        return [], -1
    ti_ctx.req.mess_manage_mode = True

    messages, extra = iterate_messages(ti_ctx)

    return extra, messages

    
TI_EXTRAMELDUNG = 10  # flag für Meldungen für spezielle Bereiche
TI_SONDERMELDUNG = 9  # Meldung am Anfang des globalen Bereichs
TI_INFOMELDUNG = 8    # Meldung am Ende des globalen Bereichs
TI_DELETED = 1        # nicht mehr anzuzeigen
TI_ACTIVE = 0         # die stinknormale Meldung. Nur bei dieser wird bei mdb_getmessages 
#                     # der Tag exakt gelesen, sonst ist er nur das zeitliche Limit!
TI_SPECIALS = [TI_EXTRAMELDUNG, TI_SONDERMELDUNG, TI_INFOMELDUNG, TI_DELETED, TI_ACTIVE]

# Die Konstanten RECHTExyz und das GRUPPENNAMEN-Array liegen in ti.py
if __name__ == '__main__':
    ti_lib.quickabort2frames()
