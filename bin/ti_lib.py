'''
________________________________________________________________________
 ti_lib.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 Dieses Modul stellt viele allgemeine Hilfsfunktionen für ti bereit
________________________________________________________________________

'''

# Batteries (included)
import sys
import traceback
import ast
import time
import hashlib

# TI-Import

'''
________________________________________________________________________
 ti_lib.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_lib.py kapselt u.a. die Zugriffe auf die Konfigurationsdatenbank.
 Die Zugriffe werden von allen Modulen an ti.py gerichtet, welches sie 
 durchreicht und dann z.B. in int-werte umwandelt.
 
 ti_lib.py kann auch von der Kommandozeile/ Shellskripten aufgerufen werden.

 Es werde bewusst keine anderen ti-Module außer config_db aufgerufen!
________________________________________________________________________
'''


class S_any_vplan():
    def __init__(self, unix_time, ziel, phase, spalten, url, dauer, weite=0, qf=False):
        self.unix_time = int(unix_time)
        self.ziel = ziel
        self.phase = phase
        self.spalten = spalten
        self.url = url
        self.dauer = dauer
        self.weite = weite
        self.qf = qf
   

class Multi_Detail_Response():
    """ Enthält die Rückgabewerte einer Routine, die mehrere Strings und ggf. eine Zahl und einen Bool-Wert liefert 
    
        man.py ruft Routinen für einzelne Aufgaben auf. Diese geben hiermit ihren i.d.R. HTML-Output zurück
    """
    def __init__(self, first_html_list=None, second_html_list=None, third_html_list=None, cancel_flag=False):
        """ die _html_listen enthalten Listen von Strings, welche die auszugebenden HTML-Codes enthalten
            extra_number kann eine Zahl enthalten
            cancel_flag gibt an, dass die Ausführung erfolglos beendet wurde.
        """
        self.html_list = [[], [], [], [], [], [], [], [], [], []]
        self.html_list[0] = [] if first_html_list is None else first_html_list 
        self.html_list[1] = [] if second_html_list is None else second_html_list
        self.html_list[2] = [] if third_html_list is None else third_html_list

        self.submenu_finished = False   # kann angeben, dass der Menüpunkt abgehandelt ist und nichts mehr darstellen will
        
        self.cancel_flag = cancel_flag
        self.sessionid = 0     # wird nur von login gesetzt, aber immer Richtung wsgi durchgereicht

        self._abschnitt = 0    # in welchen der Abschnitte sollen appends ohne Abschnittnummer eintragen werden
        
    def __repr__(self):
        return f"Multi_Detail_Response({self.html_list[0]}, {self.html_list[1]}," \
            f" {self.html_list[2]}, {self.cancel_flag})"
        
    def append_s(self, html_line, abschnitt=-1):
        if abschnitt <= 0 or abschnitt > 4:
            abschnitt = self._abschnitt
        self.html_list[abschnitt].append(html_line)
        
    def append_l(self, html_list, abschnitt=-1):
        if abschnitt <= 0 or abschnitt > 4:
            abschnitt = self._abschnitt
        self.html_list[abschnitt] += html_list
    
    def next_abschnitt(self):
        self._abschnitt += 1
    
    def ist_erfolg(self):
        return not self.cancel_flag

    def ist_abgebrochen(self):
        return self.cancel_flag

    def setze_abgebrochen(self):
        self.cancel_flag = True

    def setze_erfolg(self):
        self.cancel_flag = False

    def abschnitt_to_string(self, abschnitt):
        return '\n'.join(self.html_list[abschnitt])
    
    def abschnitt_max(self):
        for i in range(4, 0, -1):
            if len(self.html_list[i]) > 0:
                return i
        return -1
            

def readable_ip(ip, force_id=False):
    if force_id or ip.startswith('ID:'):
        return f"ID...{ip[-5:]}"
    if ip.startswith('CODE:'):
        return ip[5:]
    return ip[:18]
        

def iso_date_heute_plus_delta_days(delta_days):
    _timestamp_ = time.localtime(time.time())
    return timestruct_2_iso_date(timestruct_verschiebe(_timestamp_, delta_days))
    

def iso_date_2_human_date(iso_date):
    if ist_isodatum(iso_date):
        return f'{iso_date[8:]}.{iso_date[5:7]}.{iso_date[0:4]}'
    else:
        return ''
    
    
def timestruct_2_iso_date(ts):
    return time.strftime('%Y-%m-%d', ts)


def unix_time_2_iso_date(unix_time):
    return time.strftime("%Y-%m-%d", time.localtime(unix_time))


def unix_time_2_iso_date_n_time(unix_time):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(unix_time))


def unix_time_2_human_date_n_time(unix_time, ohne_sek=False):
    return time.strftime("%d.%m.%Y %H:%M" if ohne_sek else "%d.%m.%Y %H:%M:%S", 
                         time.localtime(unix_time))


def timestruct_verschiebe(ts, delta_tage):
    return time.localtime(time.mktime((ts.tm_year, ts.tm_mon, ts.tm_mday+delta_tage, 0, 0, 0, 0, 0, -1)))


def tidate_2_isodatum(tidate):
    unix_time = time.mktime((tidate // 1000, 1, tidate % 1000, 0, 0, 0, 0, 0, 0))
    return time.strftime('%Y-%m-%d', time.localtime(unix_time))


def ist_isodatum(datum):
    if len(datum) == 10 and \
            datum[:2] == '20' and  \
            datum[4] == '-' and  \
            datum[7] == '-' and  \
            datum[2:4].isdigit() and \
            datum[5:7].isdigit() and \
            datum[8:10].isdigit() and \
            int(datum[5:7]) < 13 and \
            int(datum[8:10]) < 32:
        return True
    return False
    

def strg2int(value, default=0):
    try:
        return int(ast.literal_eval(value))
    except Exception:
        return default
        

def strg2bool(value, default=False):
    if len(value) == 0:
        return default
    if value[0] in "YyJj1Tt": 
        return True
    if value[0] in "Nn0Ff": 
        return False
    vl = value.lower()
    if vl.startswith("on"):
        return True
    if vl.startswith("off"):
        return False
    return bool(default)
    
    
def strg2firstint(text, default=0):
    value = 0
    last = -1
    for c in text:
        if c.isdigit():     # Ziffer gefunden, einbeziehen
            last = int(c)
            value = value * 10 + last
        elif last >= 0:       # "keine ziffer" beendet bereits gefundene Zahl
            break
    if last == -1:
        return default
    return value
    
                
def prt(*args):
    for data in args:
        data8 = str(data).encode('utf-8')
        try:
            sys.stdout.buffer.write(data8)
        except Exception:
            sys.stdout.write(data8)
        

def get_now_unix_time_int():  # liefert unix-timestamp auf ganze Sekunden
    return int(get_now_unix_time())


def get_now_unix_time():  # liefert unix-timestamp
    return time.time()  # sec since epoche


def get_now4logs():
    return time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime(get_now_unix_time()))    
  
  
def get_now4filenames():
    return time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime(get_now_unix_time()))    
  

def get_akt_zeit():
    _timestamp_ = time.localtime(time.time())
    return _timestamp_.tm_hour*100+_timestamp_.tm_min
    

def beschreibe_exception(e):
    # t = 'Fehlermeldung: '+str(e)+'\n\n'
    # t += traceback.format_exc()
    # exc_type, exc_value, exc_traceback = sys.exc_info()
    # formatted_lines = traceback.format_exc().splitlines()
    # t += '\n'.join(formatted_lines)
    return '\n{}: {}\nTraceback:\n{}'.format(_('Fehlermeldung'), str(e), traceback.format_exc())
    

def get_akt_hour():
    _now_ = time.time()  # sec since epoche
    return time.localtime(_now_).tm_hour
    

def berechne_md5_von(filepath):
    md5_hash = hashlib.md5()
    with filepath.open("rb") as f:
        # Read and update hash in chunks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            md5_hash.update(byte_block)
        return md5_hash.hexdigest()
    

def save_filename(name):
    if "\\" in name:  # grandioser Workaround, falls der Filename kompletten Windowspfad enthält (alter IE?)
        fn = name.split("\\")[-1]
        if fn:
            name = fn
        else:  # trailing backslash? wo kommt der her?
            name = name.split("\\")[-2]
    return ''.join([x for x in list(name) if (x.isdigit() or x.isalpha() or x in '_-.')])
    
    
def compress_output(eingabe, grenze=4, delta=2, totalok=True, forcegrenze=False):
    """ Idee: Ein String braucht bei der Ausgabe ggf. zu viel Platz. Daher wird der hintere Teil condensed dargestellt.
        eingabe ist dieser String
        grenze (int) ist die Anzahl der Buchstaben, NACH denen eine Verkleinerung gewünscht ist. 
            Sobald ein nicht-alpha-Zeichen kommt, wird condensed eingeleitet.
        delta ist die Mindestlänge nach grenze, die der String haben muss, damit sich die Verkleinerung lohnt.
        Wenn totalok True ist und kein non-alpha-Zeichen gefunden wurde, so wird rückwärts (vor grenze) nach 
            einem solchen Zeichen gesucht und wenn keines gefunden wurde ggf. der gesamte Text condensed.
    """
    eingabe = eingabe.strip()  # da whitespace in diesem Zusammenhang keinen Sinn hat
    if len(eingabe) < grenze+delta:
        return eingabe
        
    if forcegrenze:             # egal was kommt...
        non_alnum_pos = grenze
    else:                       # Suche nach geeignetem non-alnum-Zeichen (Whitespace, Bindestriche usw.)
        non_alnum_pos = 0
        for i, c in enumerate(eingabe):
            if not c.isalnum():
                non_alnum_pos = i
                if non_alnum_pos >= grenze:
                    break
    
    if totalok or non_alnum_pos >= grenze:
        return (combine_with_condensed(eingabe[:non_alnum_pos], eingabe[non_alnum_pos:]))
    return eingabe


def combine_with_condensed(eingabe1, eingabe2):
    '''Liefert bequem den zweiten Teiltext im SPAN und beides als ein String zurück'''
    return (f'{eingabe1}<span class="condensed">{eingabe2}</span>')


def quickabort2frames():
    """Es wird nur ein kurzer Redirect zur eigentlichen Ausgabe gemacht.
        Zweck: Wenn ein Modul fälschlicherweise per CGI aufgerufen wird..."""
    prt('''Content-Type: text/html\n\n<!DOCTYPE html>
    <head>
        <meta http-equiv="refresh" content="1; URL=/ti/do/frames">
    </head>
    <body>
        Einen Moment bitte - dies ist die falsche Adresse :-)
    </body>''')
    exit()


def quickabort2management():
    """Es wird nur ein kurzer Redirect zur eigentlichen Ausgabe gemacht.
        Zweck: Wenn ein Modul fälschlicherweise per CGI aufgerufen wird..."""
    prt('''Content-Type: text/html\n\n<!DOCTYPE html>
    <head>
        <meta http-equiv="refresh" content="1; URL=/ti/do/manage">
    </head>
    <body>
        Einen Moment bitte - dies ist die falsche Adresse :-)
    </body>''')
    exit()


################################
# Netzwerkcheck-Helper
def ip2number(ipaddress):
    ipbin = 0
    numbers = ipaddress.split(".")
    if len(numbers) != 4:
        return 0
    try:
        for i in range(4):
            # debug("ip-Komponente", numbers[i])
            ipbin = (ipbin << 8) + int(numbers[i])
            # debug("ipbin", ipbin)
        return ipbin
    except ValueError:
        return 0
        

def ip_in_network(network, mask, ip): 
    # network und ip sind Strings, die je 4 mit Punkt getrennte Zahlen beinhalten
    # mask ist ein String einer Zahl zwischen 0 und 32, 
    #  welche die Anzahl der führenden gesetzten Bits der Netzwerkmaske angibt (z.B. 24 für Class-C)
    
    # debug("ManagementNetwork", network)
    # debug("client ip", ip)
    # debug("mask", mask)
    n = ip2number(network)
    i = ip2number(ip)
    if not n or not i:  # z.B. keine ipv4
        return False
    try:
        msk = int(mask)
    except ValueError:
        return False
    m = ((2 ** 32 - 1) >> (32 - msk)) << (32 - msk)
    # debug("n:" + str(n) + " i:" + str(i) + "Maske:" + bin(m))
    if n == 0 or i == 0:
        return False
    return (n & m == i & m)


def ip_in_local_networks(ip):
    if ip_in_network('127.0.0.0', 8, ip) or \
       ip_in_network('10.0.0.0', 8, ip) or \
       ip_in_network('172.16.0.0', 12, ip) or \
       ip_in_network('192.168.0.0', 16, ip):
        return True
    return False


if __name__ == '__main__':
    quickabort2frames()
