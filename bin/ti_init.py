'''
________________________________________________________________________
 ti_init.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_init.py führt zwei Arten von Initialisierung durch, 
            die erst via WSGI einen Unterschied machen:
    - für das System, einmalig bis zum Neustart des Skripts
    - für den Clientrequest
________________________________________________________________________
'''

# Batteries
import html  # für escape (ehem. c g i.escape)
import time
import sqlite3
import sys
import inspect  # für die debugging-Funktion
# Toolbox
import werkzeug
# TI
import ti_lib
 
try:
    import client
except Exception as e:
    import ti2
    ti2.panic('ti_init.py: IMPORT fehlgeschlagen', e)


# ################################################    
# ## hier beginnt der Code zur Initialisierung ###
# ################################################
class TI_System:
    """Enthält alle Informationen für das "hochgefahrene" TI-System
       Datenbank-initialisierungen müssen threadweise getrennt vorgenommen werden.
    """
    def __init__(self, ti_paths):
                         
        self.start_unix_time = ti_lib.get_now_unix_time()
        
        try:
            self.cdb_conn = sqlite3.connect(ti_paths.get_data_path('content.sq3'))
            cursor = self.cdb_conn.cursor()
            cursor.execute("""CREATE TABLE IF NOT EXISTS messages (
                                flag INTEGER, 
                                targetday INTEGER, 
                                created INTEGER, 
                                deleted INTEGER, 
                                lasthour INTEGER, 
                                target INTEGER, 
                                headline TEXT, 
                                details TEXT, 
                                color TEXT, 
                                author TEXT ) """)
            cursor.execute("""CREATE TABLE IF NOT EXISTS persons (
                                longname TEXT, 
                                target TEXT, 
                                timestamp INTEGER) """)
            cursor.close()
            self.cdb_conn.commit()
        except Exception as e:
            import ti2
            ti2.panic('Content-DB konnte nicht initialisiert werden', e)
    
    def get_unix_time(self):
        return self.start_unix_time

       
class TI_Response:
    """sammelt Informationen für die Response
       inkl. Fehlermeldungen"""
    def __init__(self, unix_time, debug2stderr=False): 
        self.debug_messages = []
        self.start_unix_time = unix_time
        self.debug2stderr = debug2stderr
    #######################################
    # einfache Debuggingmöglichkeit

    def debug(self, *args, use_caller_caller=False):
        values = [escape_html(repr(t)) for t in args]
        if isinstance(args[0], str):
            values[0] = escape_html(args[0])
        caller = inspect.currentframe().f_back.f_code if not use_caller_caller else inspect.currentframe().f_back.f_back.f_code 
        file_name = caller.co_filename.split('/')[-1]
        if len(file_name) > 3 and file_name[-3:] == '.py':
            file_name = file_name[:-3]
        func_name = '.' + caller.co_name
        caller_name = ' ' * (21 - len(file_name)) + file_name + func_name + ' ' * (21 - len(func_name))
        if len(values) > 1 and (values[1] and values[1][0] not in ':=') \
                and (values[0] and values[0][-1] not in ':='):
            values[0] += " ="
        thevalues = ' '.join(values)
        self.debug_messages.append(f'<!-- Info von {caller_name}: {thevalues}{" " * (70 - len(thevalues))}-->')
        if self.debug2stderr:
            sys.stderr.write(thevalues + '\n')
            sys.stderr.flush()

    # muss am Ende händisch aufgerufen werden, kann aber auch mehrfach geschehen
    def debug_flush(self):
        all_messages = '\n'.join(self.debug_messages)
        self.debug_messages = []
        return all_messages
        
    def debug_timestamp(self, eventname, timestamp=0):
        if timestamp:
            self.debug(eventname, str(timestamp - self.start_unix_time), True)
        else:
            self.debug(eventname, str(time.time() - self.start_unix_time), True)
        

class TI_Request:
    """enthält alle Informationen für den aktuellen Aufruf per WSGI via werkzeug"""
    def __init__(self, request, ti_cnf, ti_res):
        # das request-Objekt kommt von "werkzeug", das ti_res...ponseobjekt muss vorher initialisiert worden sein
        self.client_name = ""
        self.request = request
        self.werkzeug_major = ti_cnf.werkzeug_major
        
        ti_client = None
        site, clientID = get_site_clientID(request.environ)
        if len(clientID) > 48:
            ip_address = f'ID:{clientID}'
            ti_client = ti_cnf.client_read_ti_client(ip_address)
        if ti_client:
            self.ist_extern = True
            self.block_extern = False
        else:
            try:
                ip_address = request.remote_addr
            except Exception:
                ip_address = '127.0.0.1'

            # ti_res.debug("Trusted Proxy, IP-Adresse", ti_cnf.get_db_config('Man_TrustedProxy').strip(), ip_address)
            self.ist_extern = False
            self.block_extern = False
            if ip_address and ip_address == ti_cnf.get_db_config('Man_TrustedProxy').strip():
                ti_res.debug('Trusted Proxy entdeckt', ip_address)
                self.ist_extern = True
                self.block_extern = True  # kann über Cookie-Überprüfung bei externen Clients aufgehoben werden
                # ti_res.debug('request',request.environ)
                if 'HTTP_X_FORWARDED_FOR' in request.environ:
                    # ti_res.debug('HTTP_X_FORWARDED_FOR', cgi.escape(request.environ["HTTP_X_FORWARDED_FOR"]))
                    ip_address = html.escape(request.environ["HTTP_X_FORWARDED_FOR"]).split(', ')[-1].strip()
                    ti_res.debug('endgültig: X_FORWARDED_FOR', ip_address)
                
                # Erkenne einen HEADER im Request, der vom Proxy zu setzen ist.
                if 'HTTP_X_TISITE' in request.environ:
                    if '-' in request.environ['HTTP_X_TISITE']:
                        tisite, ticode = request.environ['HTTP_X_TISITE'].split('-')
                        ti_res.debug('x-TiSite-Header entdeckt für', tisite)
                        if ti_cnf.get_db_config('ti_site_code') == ticode:
                            self.block_extern = False
                            ti_res.debug('x-TiSite-Header erlaubt externen Zugriff')
                        else:
                            ti_res.debug('x-TiSite-Header mit falschem Code. Kein freier externer Zugriff')
                    else:
                        ti_res.debug('x-TiSite-Header falsch gebildet, fehlt "-". Kein freier externer Zugriff')
                if ti_cnf.get_db_config('ti_site_code') == '':
                    self.block_extern = False
                    ti_res.debug('ti_site_code ist leer, default: extern erlauben')
                
        ti_res.debug("Final: block_extern", self.block_extern)
        self.client = client.get_TI_Client(ip_address, ti_cnf, ti_res)
        
        # quasiglobale Variablen für messages.py
        self.mess_manage_mode = False
        
    # Cookies:
    def get_websessionid(self):
        if self.werkzeug_major < 3:   # you just have to love API-changes
            multidict = werkzeug.http.parse_cookie(self.request.environ, charset='utf-8', errors='replace', cls=None)
        else:
            multidict = werkzeug.http.parse_cookie(self.request.environ, cls=None)
        if "ti_sessionid2" in multidict:
            return multidict["ti_sessionid2"]
        else:
            return ""

    ################################
    # CGI-Werte handhaben
 
    def get_value(self, parameter, default=""):
        try:
            value = self.request.args[parameter]
        except Exception:
            value = default
            
        if not value and self.request.method == 'POST':
            return self.get_form(parameter, default)

        return value

    def get_value_bool(self, parameter, default=False):
        return ti_lib.strg2bool(self.get_value(parameter), default)
        
    def get_value_int(self, parameter, default=0):
        return ti_lib.strg2int(self.get_value(parameter), default)
        
    def get_form(self, parameter, default=""):
        try:
            value = self.request.form[parameter]
        except Exception:
            return default
        return value

    def save_file(self, parameter, filepath):
        """Speichert genau eine Datei ab, die im Formular unter parameter abgeschickt wurde.
            Der filepath ist der komplette Pfad MIT Dateiname, der zum Speichern verwendet werden soll.
            Rückgabewert: (nicht verwendeter, nicht gesicherter) Original-Dateiname bei Erfolg, sonst None"""
            
        if parameter not in self.request.files:
            return None
        ufile = self.request.files.get(parameter)
        ufile.save(filepath.as_posix())
        return ufile.filename
        
    def save_files(self, parameter, targetpath, path_is_filename=False):
        """Speichert mehrere Dateien ab, die im Formular unter parameter abgeschickt wurde.
            Der filepath ist der Pfad OHNE Dateiname, der zum Speichern verwendet werden soll.
            Verwendet wird der übergebene Dateiname, nachdem er gestrippt (save_filename) wurde.
            Rückgabewert: LISTE mit Dateinamen, ggf. leere Liste"""
        fnlist = []
        if parameter not in self.request.files:
            return fnlist
        for ufile in self.request.files.getlist(parameter):
            targetfn = ti_lib.save_filename(ufile.filename)
            if targetfn:
                ufile.save((targetpath / targetfn).as_posix())
                fnlist.append(targetfn)
        return fnlist
        
    def debug_get_all_args(self):
        return self.request.args


def escape_html(dangerous):
    save = dangerous.replace("&", '&amp;')
    saver = save.replace("<", '&lt;').replace(">", '&gt;').replace('--', '- -').replace('    ', " ").replace('   ', " ")
    return saver.replace('  ', " ")


# Die site muss und kann noch vor dem Initialisieren der ti_res... usw. -Objekten ermittelt werden
# - daher in statischer Methode.
def get_site_clientID(environ):
    multidict = werkzeug.http.parse_cookie(environ)
    if "ti_site_clientID" in multidict:
        k_c = multidict["ti_site_clientID"].split('.')
        if len(k_c) == 2:
            site, clientID = k_c
            return site, clientID
    return "", ""


if __name__ == '__main__':
    ti_lib.quickabort2frames()
