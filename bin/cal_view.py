#!/usr/bin/python3
'''
________________________________________________________________________
 cal_view.py                                                         
 This file is part of tabula.info, which is free software under           
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 cal_view.py zeigt 
    * für die kommenden Tage die
    * Termine aus verschiedenen (in cal_man konfigurierten)
    * iCalendar-Quellen (mit cal_load heruntergeladen) an
________________________________________________________________________
'''

# Batteries (Bibliothek)
import pickle

# TI


def get_cal_lines(ti_ctx):
    if not ti_ctx.cnf.get_db_config_bool("Flag_ShowCal", False):
        return "", "", 'Kalender abgeschaltet'
    try:
        with open(ti_ctx.paths.get_temp_path('cal/cal_lines.pickle'), 'rb') as f:
            lines, linesML, merke_error = pickle.load(f)
        return (lines, linesML, merke_error)
    except FileNotFoundError:
        return "", "", 'Datei cal_lines.pickle nicht vorhanden.'
    except EOFError:
        return "", "", 'Datei cal_lines.pickle (vorübergehend?) leer.'
    except pickle.UnpicklingError:
        return "", "", 'Kalender einlesen fehlgeschlagen:'+str(e)
    except Exception as e:
        return "", "", 'unbekannter Fehler:'+str(e)
