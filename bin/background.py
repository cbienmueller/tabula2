#!/usr/bin/python3
'''
________________________________________________________________________
 background.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 background.py läuft im Hintergrund, stellt sicher, dass es alleine läuft
  und erledigt Aufgaben, die es in der Datenbank findet.
  Dies ist immer ein eigener Python-Task, der per subprocess startet und
  der nicht von den anderen TI-Modulen direkt aufgerufen wird!
________________________________________________________________________
'''

# Batteries
import os

# TI
import ti_lib
import config_db
import cal_load


def do_periodic(ti_cnf, now):
    # normale ti-funktion
    import s_any_download
    
    forced_bg_task = True if ti_cnf.get_db_config_bool('s_any_force_convert') else False
    error_log = ""
    
    # Schritt 1: daily job
    background_last = ti_cnf.get_db_config_int('background_last_unix_time', 0)
    if background_last and \
       (ti_lib.unix_time_2_iso_date(background_last) != ti_lib.unix_time_2_iso_date(now)):
        ti_cnf.paths.reset_log()
        try:
            s_any_download.loesche_alle_downloads(ti_cnf, nurheute=True)
        except Exception as e:
            error_log += 'Loeschen der Downloads scheiterte'
            ti_cnf.paths.log('BKG: Loeschen der Downloads scheiterte', e)
        ti_cnf.set_db_config('s_any_force_convert', 1)

    # Schritt 2: Pläne herunterladen
    erfolg, r = s_any_download.do_any_download(ti_cnf)

    # Schritt 3: Pläne analysieren
    try:
        import s_analyzer
        s_analyzer.do_analyze(ti_cnf)
    except Exception as e:
        to_log = 'BKG: do_analyze scheiterte mit ' + ti_lib.beschreibe_exception(e)
        ti_cnf.paths.log(to_log)
        error_log += to_log
        
    # Schritt 4: Kalender
    error_log += cal_load.do_downloads(ti_cnf, forced_bg_task)
        
    # Abschluss
    ti_cnf.set_db_config('background_last_unix_time', str(int(now)))
    return error_log    


def do_background(ti_cnf):
    """wird vom Betriebssystem als Hintergrundtask aufgerufen"""
    myid = 'ungesetzt'
    neuer_timeout_gesetzt = False
    try:
        now = ti_lib.get_now_unix_time()
        myid = '{}_{}'.format(int(now*10), os.getpid())
        log_start = f'BKG: {myid} ist neuer externer Background-Prozess um {ti_lib.get_now4logs()}'
        old_sema = ti_cnf.get_db_config('background_semaphore')
        # abfangen, da nach 2 Minuten ist eine Reservierung verfallen - greift auch, wenn die Semaphore nicht gesetzt ist!
        if old_sema != "free" and old_sema < str(now - 120): 
            ti_cnf.set_db_config('background_semaphore', 'free', old_sema)
            ti_cnf.paths.log(f'BKG: background_semaphore zurückgesetzt wg. zu alter Reservierung (während {myid})')
            
        # nun setzen wir die Semaphore mit unserer ID - wenn der Wert noch "free" ist (macht SQLITE in einem Befehl)
        ti_cnf.set_db_config('background_semaphore', myid, 'free')
        # geklappt?
        if ti_cnf.get_db_config('background_semaphore') == myid:
            ti_cnf.paths.log(log_start, "und übernimmt den Job!")
            # gut, erst einmal ein neues timeout setzen
            if ti_lib.get_akt_hour() == ti_cnf.get_db_config_int('s_any_hintergrund_rushhour', 7):
                newtimeout = ti_cnf.get_db_config_int('s_any_hintergrundtakt_rushhour_s', 120)
                ti_cnf.paths.log('BKG: ', ti_lib.get_akt_hour(), 'Uhr -> Rushhour:', newtimeout, 's bis zum nächsten Check')
            else:
                newtimeout = ti_cnf.get_db_config_int('s_any_hintergrundtakt_s', 300)
                
            # setze den Timeout, wann die Webaktivitäten einen Backgroundtask starten sollen. Nicht den cron-Takt!
            ti_cnf.set_db_config('background_timeout', str(int(now + newtimeout)))
            neuer_timeout_gesetzt = True
            error_log = do_periodic(ti_cnf, now)
            ti_cnf.set_db_config('background_semaphore', 'free')
            ti_cnf.paths.log(f'BKG: {myid} ist abgeschlossen!')
            if error_log:
                ti_cnf.set_db_config('background_last_status', f'{ti_lib.unix_time_2_human_date_n_time(now)}: {error_log}')
            else:
                ti_cnf.set_db_config('background_last_status', f'{ti_lib.unix_time_2_human_date_n_time(now)}: ok')
        else:
            ti_cnf.paths.log(log_start, 'ist aber nicht dran! Ein anderer Prozess läuft bereits lt. Semaphore')
        
    except Exception as e:
        background_status = f'''BKG: background() scheiterte! myid:{myid}')
        BKG: -> Grund: {ti_lib.beschreibe_exception(e)}'''
        ti_cnf.paths.log(background_status)
        ti_cnf.set_db_config('background_last_status', f'{ti_lib.unix_time_2_human_date_n_time(now)}: {background_status}')
        if neuer_timeout_gesetzt:
            ti_cnf.set_db_config('background_timeout', 0) 


if __name__ == '__main__':
    # print('BKG: Init externer Aufruf')
    from sys import argv

    if len(argv) > 1:
        site = argv[1]
    else:
        site = 'local'
    import paths
    ti_paths = paths.TI_Paths(site)
    if ti_paths.status == paths.PST_OK:
        print(f"Background.py läuft für {site}")
        ti_cnf = config_db.TI_Config(ti_paths)
        do_background(ti_cnf)
    
