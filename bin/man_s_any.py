'''
________________________________________________________________________
 man_s_any.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man_s_any.py ermöglicht Pläne in diversen Formen per Browser hochzuladen und zu löschen
________________________________________________________________________

 
'''
# Batteries

# TI
import ti
import ti_lib
import konst


def get_filelist(path, delete_what=""):
    try:
        if not path.exists():
            return 'pathnotfound' 
    except Exception:
        return 'pathnotfound'
    result = []
    LINE = '''\n\t\t  <td>
            <form action="/ti/do/managepost/sched" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="file2delete" value="{filename}" />
                <input type="hidden" name="delete_what" value="{delete_what}" />
                <input  style="color:red" type="submit" name="delete_tool" value="X" />
                &nbsp;{filename}
            </form></td><td> <small>{dateinfo}</small></td>\n'''
    try:
        for filepath in path.iterdir():
            result.append(LINE.format(filename=filepath.name, 
                          dateinfo=ti.get_datestring_from_unix_time(filepath.stat().st_mtime),
                          delete_what=delete_what))
                          
    except Exception:
        result = ['<td>'+_('Fehler bei der Dateiauflistung! Existiert das Verzeichnis?')+'</td>']
    
    if not result:
        return ""
    ergebnisse = '\t\t</tr><tr>'.join(result)
    return f'<table><tr><th style="width:25em">Filename</th><th>Datum</th>\n\t\t</tr><tr>{ergebnisse}\n\t\t</tr></table>' 


def get_uploadlist(ti_ctx):
    return get_filelist(ti_ctx.paths.get_data_path("upload"), "delete_upload")


def get_downloadlist(ti_ctx):
    fl = get_filelist(ti_ctx.paths.get_data_path("download"), "delete_download")
    if fl == "":
        fl = _('Keine Dateien! Existiert vplansemaphore.txt in der Quelle?')
    return fl
    

def get_toollist(ti_ctx):
    return get_filelist(ti_ctx.paths.get_toolpath(), "delete_tool")
    

def print_html_form(ti_ctx, erfolg, fnamen):
    """Erzeugt eine md_response, so dass sowohl die aktuellen Pläne, 
    als auch Eingabemöglichkeiten dargestellt werden können.
    """
    # Templates müssen wg. dynamischer Sprache jetzt dynamisch erzeugt werden....

    HTML_TEMPLATE1 = f'''
    <div class="upload">
        <div>
            <h3>{_('Hier hochgeladene Pläne')}:</h3>
            {{filelist}}
            <form action="/ti/do/managepost/sched" method="POST" enctype="multipart/form-data">
                <input                   type="hidden" name="delete_what" value="delete_upload" >
                <input style="color:red" type="submit" name="delete"      value="{
                _('Lösche alle hochgeladenen Pläne')} " /><br><br>
            </form>
            <form action="/ti/do/managepost/sched" method="POST" enctype="multipart/form-data">
                <input name="upload_files" type="file" multiple="multiple" style="border: 0;" /><br>
                <input type="submit" name="ergaenzen" value=" {_('Hochladen')} " /> 
                <small>{_('(gleichnamige Pläne werden ersetzt)')}</small><br><br>
            </form>
        </div>
        
        
    </div>
    '''
    HTML_TEMPLATE2 = f'''
    <div class="upload">
        <form action="/ti/do/managepost/sched" method="POST" enctype="multipart/form-data">
        <div>
                <h3>{_('Per Download übertragene Pläne')}<small><small> Quelle: ({{quelle}})</small></small></h3>
                {{downloadlist}}
                <input                   type="hidden" name="delete_what" value="delete_download" >
                <input style="color:red" type="submit" name="delete" value=" {_('Lösche Download-Pläne')} " /><br><br>
                <div class="tooltip">{_('Erklärung für den Download')}
                    <span class="tooltiptext">{_("""In den Einstellungen kann eine Downloadquelle angegeben werden.
                    Hier sehen Sie die Ergebnisse des Downloads.""")}
                    </span>
                </div>
        </div>
        </form>
    </div>
    '''
    HTML_TEMPLATE3 = f'''
    <div class="upload">
        <div>
            <h3>{_('Per Tool übertragene Pläne')}:</h3>
            {{toollist}}
            <form action="/ti/do/managepost/sched" method="POST" enctype="multipart/form-data">
                <input                   type="hidden" name="delete_what" value="delete_tool" >
                <input style="color:red" type="submit" name="delete" value=" {_('Lösche Tool-Pläne')} " /><br><br>
            </form>
            <div class="tooltip">{_('Erklärung für den Tool-Upload')}
                <span class="tooltiptext">{_("""Sie können z.B. mit einem FTP-Programm oder per ssh(fs) 
                    ebenfalls Dateien hochladen. 
                    Ziel muss /home/tabula/upload sein.
                    Dort vorhandene Dateien werden hier aufgelistet und mitverwendet.
                    Sie können hier nur gelöscht werden!""")}
                </span>
            </div>
        </div>
        
    </div>
    '''
    HTML_TEMPLATE_INFO_T = f'''
    <div class="upload">
        <div ><br>
                <H3>{_('Per Tool übertragene Pläne')}<small><small> {{path}}</small></small></H3>
                ...{_('werden jetzt nicht dargestellt, da das obige Vierzeichnis nicht existiert oder für www-data nicht lesbar ist')}.
                <div class="tooltip">{_('Erklärung für den Tool-Upload')}
                    <span class="tooltiptext">{_("""Sie können z.B. mit einem FTP-Programm oder per ssh(fs) 
                    ebenfalls Dateien hochladen. 
                    Ziel muss /home/tabula/upload sein.
                    Dort vorhandene Dateien werden hier aufgelistet und mitverwendet.
                    Sie können hier nur gelöscht werden!""")}
                    </span>
                </div>
        </div>
    </div>
    '''
    HTML_TEMPLATE_INFO_D = f'''
    <div class="upload">
        <div ><br>
                <H3>{_('Per Download übertragene Pläne')}:</H3>
                ...{_('werden jetzt nicht dargestellt, da keine Download-Quelle in den Einstellungen (Kontext Pläne, Allgemein) angegeben ist.')}.
        </div>
    </div>
    '''

    H_T_INFO = _('''
        <H3>Info:</H3><small>
            <ul><li>Die Reihenfolge der Dateien ist beliebig</li>
                <li>Die Namen müssen keinen Regeln entsprechen (können aber), die Erweiterung (.pdf etc.) ist jedoch wichtig</li>
                <li>Werden Namen mit Datum, "heute" oder "morgen" verwendet, so haben diese Priorität, 
                    ansonsten gilt die lexikalische Reihenfolge</li>
            </ul>
            <ul>Dateinamenslogik:<li><b>2013-01-13</b>.pdf wird bis zu diesem Tag angezeigt.
                <li>plan_<b>heute</b>_mensa_M.pdf wird nur am Upload-Tag angezeigt (in der M-Anzeigegruppe)</li>
                <li>Endungen: Bspw. subst_001<b>_L</b>.htm wird nur bei Lehrern angezeigt. 
                    Erkannte Endungen sind _Q, _A, _B, _M und _L</li>
                <li>Gibt es mindestens eine Datei für z.B. Lehrer, so wird/werden nur dies/e dort angezeigt.<br>
                 Gibt es keine Datei für eine solche Anzeigegruppe, so werden an den entsprechenden Clients die allgemeinen Dateien
                    (ohne Endung) angezeigt.</li>
                <li>Ist ein Client in mehreren Clientgruppen, so wird für Pläne nur die höchstwertige, 
                    also die am weitesten rechts berücksichtigt.</li>
            </ul>
            <ul>Dateitypen:
                <li>HTML: wird ggf. ergänzt (für scrolling), refresh entfernt und direkt angezeigt. Siehe auch CSS!</li>
                <li>PDF: wird seitenweise in ein in PNG, ggf. mit horizontalem Scrolling, konvertiert.</li>
                <li>PNG, GIF, JPG werden 1:1 übernommen.</li>
                <li>CSV: wird als TurboPlaner (Haneke)-Export interpretiert, vereinfacht und als HTML dargestellt. 
                            Eine zweite CSV-Datei wird ignoriert!</li>
                <li>CSS: wird 1:1 kopiert unter Beibehaltung des Dateinamens. 
                    Damit können HTML-Dateien problemlos eine Formatierung erhalten.</li>
                <li>URL: Textdatei, welche eine Zeile mit einer URL enthält. 
                    Diese wird angezeigt - der Client muss sie im Netzwerk direkt erreichen können!</li>
            </ul>
        </small>
    ''')
    
    md_response = ti_lib.Multi_Detail_Response()

    if erfolg:
        anzahl = len(fnamen)
        erfolgsmeldung = f'''
                <div class="erfolg">
                    {erfolg}: {anzahl} {_('Dateien') if anzahl != 1 else _('Datei')}
                    <ul style="text-align:left;padding-left:12em;margin:1px;"><li>
                    {'</li><li>'.join(fnamen)}
                    </li></ul>
                </div>'''
        ti_ctx.set_erfolgsmeldung(erfolgsmeldung)

    md_response.append_s(HTML_TEMPLATE1.format(filelist=get_uploadlist(ti_ctx)))
    md_response.next_abschnitt()
    
    downloadquelle = ti_ctx.cnf.get_db_config('s_any_quelle')
    if not downloadquelle:
        md_response.append_s(HTML_TEMPLATE_INFO_D.format("Downloadquelle"))
    else:
        md_response.append_s(HTML_TEMPLATE2.format(downloadlist=get_downloadlist(ti_ctx), quelle=downloadquelle))
    md_response.next_abschnitt()
    toollist = get_toollist(ti_ctx)
    if toollist == 'pathnotfound':
        md_response.append_s(HTML_TEMPLATE_INFO_T.format(path=ti_ctx.paths.get_toolpath().as_posix))
    else:
        md_response.append_s(HTML_TEMPLATE3.format(toollist=toollist))
    md_response.next_abschnitt()
    md_response.append_s(H_T_INFO)
    return md_response
    

def save_uploaded_files(ti_ctx):
    """This saves the files uploaded by an HTML form.
    The form_field is the name of the file input field from the form.
    If no file was uploaded or if the field does not exist then
    this does nothing.
    """
    
    filenamelist = []
    usepath = ti_ctx.paths.get_data_path("upload")
    
    filenamelist = ti_ctx.req.save_files('upload_files', usepath)
        
    with (usepath / "vplansemaphore.txt").open(mode='wb') as f:
        f.write(b'semaphore um valide Dateien anzuzeigen')
        
    if filenamelist:
        force_reconvert(ti_ctx)
    
    return filenamelist


def force_reconvert(ti_ctx):
    ti_ctx.cnf.set_ti_lastupload('Plan')
    ti_ctx.cnf.set_db_config('s_any_force_convert', 1)
    ti_ctx.cnf.set_db_config('background_timeout', 0)
    

def delete_files(ti_ctx):
    """Löscht alle oder eine Datei
    
    Pfad: je nach Request-Parameter delete_what
    Datei: je nach file2delete - wenn leer: alle
    """
    success = False
    filenamelist = []
    usepath = None
    delete_what = ti_ctx.req.get_value("delete_what", "")
    if delete_what == "delete_upload":
        usepath = ti_ctx.paths.get_data_path("upload")
        ti_ctx.res.debug("Upload-Dateien löschen")
    elif delete_what == "delete_tool":
        usepath = ti_ctx.paths.get_toolpath()
        ti_ctx.res.debug("Tool-Dateien löschen")
    elif delete_what == "delete_download":
        usepath = ti_ctx.paths.get_data_path("download")
        ti_ctx.res.debug("Download-Dateien löschen")

    if usepath:
        file2delete = ti_ctx.req.get_value("file2delete", "")
        try:
            for filepath in usepath.iterdir():
                if (filepath.name == file2delete) or (not file2delete):
                    ti_ctx.res.debug('Datei zu loeschen', filepath.as_posix())
                    filepath.unlink()
                    filenamelist.append(filepath.name)
                    success = True
        except Exception as e:
            ti_ctx.res.debug("Dateien löschen fehlgeschlagen!", e)
            filenamelist.append(file2delete)
    if success:
        force_reconvert(ti_ctx)
    return success, filenamelist


def do_manage(ti_ctx):
    ti_ctx.check_management_rights(konst.RECHTEVertretungsplan)
    ti_ctx.res.debug("Man_s_Any", "gestartet")  
    erfolg, fnamen = delete_files(ti_ctx)              # entweder löschen
    if erfolg:
        return print_html_form(ti_ctx, _('Löschen erfolgreich'), fnamen)  # wird mdr zurückgeben
    elif fnamen:
        return print_html_form(ti_ctx, _('Löschen fehlgeschlagen'), fnamen)  # wird mdr zurückgeben
    fnamen = save_uploaded_files(ti_ctx)   # oder hochladen
    return print_html_form(ti_ctx, _('Hochladen erfolgreich') if fnamen else '', fnamen)  # wird mdr zurückgeben


if __name__ == '__main__':
    ti_lib.quickabort2frames()
