#!/usr/bin/python3
'''
________________________________________________________________________
 ti_tool.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti_tool.py kann nur von der Kommandozeile/ Shellskripten aufgerufen werden.

________________________________________________________________________
'''

# Batteries (included)
import sys

# TI-Import 
import paths
ti_paths = paths.TI_Paths('local')

import ti_lib
    
try:
    import config_db
except Exception as e:
    print("ti_tool.py failed importing config_db.py", e)
    exit(-1)


try:
    if __name__ == '__main__':
        exitcode = 0
        ti_cnf = config_db.TI_Config(ti_paths)
        if len(sys.argv) < 2 or len(sys.argv) > 3:
            # also kein legitimer cli-Aufruf, gehe von CGI aus und beende sofort
            ti_lib.quickabort2frames()

        elif sys.argv[1] == "--set_semaphore" and len(sys.argv) > 2:
            exitcode = ti_cnf.check_n_set_semaphore(sys.argv[2]) 

        elif sys.argv[1] == "--clear_semaphore" and len(sys.argv) > 2:
            ti_cnf.clear_semaphore(sys.argv[2])  # endet mit exit(code)

        elif sys.argv[1] == "--s_html_convert":
            import s_html_upload
            exitcode = s_html_upload.convert_files(ti_paths.get_basepath())

        elif sys.argv[1] == "--set_ti_lastupload":
            ti_cnf.set_ti_lastupload()

        elif len(sys.argv) == 2:
            if sys.argv[1] == "basepath":
                print(ti_paths.get_basepath())
            else:
                print(ti_cnf.get_db_config(sys.argv[1], default=''))         

        elif len(sys.argv) == 3:
            ti_cnf.set_db_config(sys.argv[1], sys.argv[2])
        exit(exitcode)
except Exception as e:
    import ti2
    ti2.panic("ti_tool failed!", e)

