'''
________________________________________________________________________
 motto.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 motto.py liefert ein Wochenmotto
________________________________________________________________________
'''
# Batteries
# (nix)
# TI
import ti_lib


def get_motto(ti_ctx):
    motto = ti_ctx.cnf.get_db_config('ti_motto')
    if motto != "off" and len(motto) > 0:
        if len(motto) < 100:
            content = '''
            <div  id="wochenmotto" >
                <p><small><small><small>{title}:</small></small></small></p>
                <h1>{m}</h1>
            </div>\n'''.format(title=_('Das Wochenmotto'), m=motto)
        else:
            content = '''
                <div id="wochenmotto">
                    <p><small><small><small>Das Wochenmotto:</small></small></small></p>
                    <h1  style="font-size:97% ">{m}</h1>
                </div>      
                '''.format(m=motto)
        return content
    return ""


def manage_motto(ti_ctx):
    m = ti_ctx.req.get_value("motto")
    if len(m) > 1:
        ti_ctx.cnf.set_db_config('ti_motto', m[:64])
        ti_ctx.set_erfolgsmeldung('+Motto wurde gesetzt' if m != 'off' else '+Motto wurde abgeschaltet')
    skeleton = '''
      <div class="wochenmotto" style="text-align: left;">
        <h3>Ändern des Mottos</h3>
        <form action="/ti/do/managepost/motto" method="post" 
            style="text-align: left; margin-top: 10px; padding-left: 5em;font-size=20px;">
            <input name="motto" value="{mottowert}" type="text" size="48" maxlength="64"  class="eingabe" />(max 64 Zeichen)
            <br>
            Dieses Motto jetzt <input value="setzen" type="submit">
        </form><br>
        <form action="/ti/do/managepost/motto" method="post" 
            style="text-align: left; margin-top: 10px;padding-left: 5em; font-size=20px;">
        <br>...oder...<br>
            <input name="motto" value="off" type="hidden"><br>Das Motto vorläufig
            <input value="abschalten" type="submit">
        </form><br>
       </div>
     </body>
    '''
    mw = ti_ctx.cnf.get_db_config('ti_motto')
    md_response = ti_lib.Multi_Detail_Response([skeleton.format(mottowert=mw if mw != 'off' else '')])
    return md_response


if __name__ == '__main__':
    ti_lib.quickabort2frames()
