'''
________________________________________________________________________
 man_psns.py                                                            
 This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 Diese Datei ist Teil von tabula.info, das Freie Software gemäß
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 man_psns.py ermöglicht den Personenruf per Browser zu verwalten
 und nutzt dabei u.a. die Datenstruktur P_Call aus persons.py
________________________________________________________________________
'''
# Batteries

# TI
import ti
import ti_lib
import konst
import persons  # wg. Objektdefinition P_Call


def print_goto_messages(resultatmeldung=""):
    forwarder = '''<head>
    <meta http-equiv="refresh" content="{0}; URL=messages.py?manage_mode=1">
    <title>tabula.info - forwarding...</title>
    </head>'''
    if len(resultatmeldung) != 0:
        delay = 10
    else:
        delay = 0
    ti.prt(forwarder.format(str(delay)))
    if delay > 0:
        ti.prt("<h2>Fehler: "+resultatmeldung+"</h2>")


def get_edit_persons_form(ti_ctx, pnr):
    targets = []
    for t in ti_ctx.cnf.get_db_config("Ziele").split(";"):
        t = t.strip()
        if t:
            targets.append(t)
    targets.sort()
    targetselect = ""
    for t in targets:
        if t[0] in "-+~":                                        # Lösche Prioritätenkennzeichnung - nur zum Sortieren benötigt
            t=t[1:]
        if t:
            if not targetselect:
                targetselect += f'<option selected>{t}</option>' # Ersten Eintrag vorauswählen
            else:
                targetselect += f'<option>{t}</option>'
    skeleton1 = f'''
        <div class="persons"  style="border:0;width:auto;">
            <h3 class="mint">{_('Neuer Eintrag')}:</h3>
            <form action="/ti/do/managepost/persons" method="post" style="text-align: center; margin-top: 10px;">
                <input name="todo" value="add" type="hidden">
                <input name="pnr" value="{{pnr}}" type="hidden">
                <input name="number_2_delete" value="" type="hidden">
                <table>
                    <tr>
                        <td>{_('Klasse')}</td>
                        <td>{_('Name, &nbsp;Vorname')}</td>
                        
                    </tr>
                    <tr>
                        <td><input name="klasse"  id="klasse" size="6" maxlength="10" type="text" class="eingabe"></td>
                        <td><input name="name"  id="name" size="30" maxlength="200" type="text" class="eingabe"></td>
                        
                    </tr><tr><td> </td><td>&nbsp;</td></tr>
                    <tr>
                        <td>{_('Kontakt')}</td>
                        <td>{_('(evtl.&nbsp;Grund)')}</td>
                        
                        
                    </tr>
                    <tr>
                        <td><select name="target" size="10">
                              {{targetselect}}
                            </select></td>
                        <td>(<input name="reason"  id="reason" size="10" maxlength="20" type="text" class="eingabe">)
                        <br><br><br><br><br>
                            <input value="{_('Absenden')}" type="submit" style="color:white;background-color:blue;">
                        </td></tr>
                </table>
            </form>
            <p style="text-align:right">
            <div class="tooltip">{_('Anleitung')}!
                <span class="tooltiptext">
              {_("""Unbedingt die ersten beiden Felder ausfüllen.<br>
                    Die Einträge werden später erst nach Klasse, dann alphabetisch sortiert.<br>
                    Man kann mehrere Schüler der gleichen Klasse gleichzeitig eingeben, 
                    wenn man sie mit Doppelpunkten oder Strichpunkten trennt!<br>
                    Die Angabe der Klasse hilft: Personen derselben Klasse werden gruppiert!<br>
                    Eine Eingabe, bei der Klasse oder Name leer bleibt, wird kommentarlos ignoriert!<br>
                    <b>Tricks:</b> Zwischen den Eingabefeldern mit Tabulatortaste wechseln, 
                    in der Auswahlliste mit den Pfeiltasten wählen, 
                    mit der Eingabetaste absenden....""")}</span>
            </div> 
       
            </p>
        </div>
      '''
    skeleton2 = f'''

      
        <div class="persons" style="border:0;width:auto;">
        <h3 class="mint">{_('Aktuelle Einträge')}:</h3>
            {{ps}}
        </div>
      
      
    '''

    persons_string = ""
    i = 0
    timestamp_now = ti_ctx.sys.get_unix_time()
    Flag_psns_caps = ti_ctx.cnf.get_db_config_bool('Flag_psns_caps')
    po_array = persons.pdb_get_persons(ti_ctx)
    for p in po_array:
        persons_string += '\t<p class="mint"><a href="/ti/do/manage/persons?number_2_delete=' + \
                str(p.get_rowid()) + \
                '&todo=delete"><img src="/ti/static/pix/erase.png" width="150" height="19" border="0" alt="loeschen"></a> ' + \
                p.get_visible_longname(Flag_psns_caps) + ' <big>&rArr;</big><small> ' + \
                p.get_target() + f''' {_('seit')} ''' + \
                str(p.get_age_hours(timestamp_now)) + f' {_("Stunden")}</small></p>\n'
        i += 1

    return skeleton1.format(pnr=str(pnr), targetselect=targetselect), \
        skeleton2.format(ps=persons_string)
    

def stripbadletters(eingabe):
    goodletters = ' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!()*+, -.:?@_äöüÄÖÜß€àáâèéêïìíîòóôùúû'
    return''.join([letter for letter in eingabe if letter in goodletters]).strip()


def do_manage(ti_ctx):
    md_response = ti_lib.Multi_Detail_Response()
    if not ti_ctx.check_management_rights(konst.RECHTEPersonenruf):
        ti_ctx.res.debug("keine Personenruf-Rechte!")
        return [], -1
        
    pnr = ti_ctx.req.get_value_int("pnr", 1)

    n2d = ti_ctx.req.get_value_int("number_2_delete", -1)
    resultatmeldung = ""
    abgelehnte = ""
    anzahl = 0
    ti_ctx.res.debug("n2del", n2d)
    try:
        
        new_name = ti_ctx.req.get_value("name")
        new_klasse = ti_ctx.req.get_value("klasse")
        new_target = ti_ctx.req.get_value("target")
        new_reason = ti_ctx.req.get_value("reason")
        min_entry_len = max(1, ti_ctx.cnf.get_db_config_int("psns_min_entry_len", 3))

        if new_name and new_target:  # neue Minimalanforderungen, sinnlose Eingaben werden trotzdem abgefangen
            ti_ctx.res.debug("Klasse", new_klasse, "Name", new_name, 
                             "Target", new_target, "Grund", new_reason, "Len", min_entry_len)
        
            new_name = ":".join(new_name.split(";"))
            new_name = stripbadletters(new_name)

            new_klasse = stripbadletters(new_klasse)
            if len(new_klasse) > 1 and not new_klasse[0].isdigit():
                new_klasse = new_klasse[0].upper()+new_klasse[1:].lower()

            new_target = stripbadletters(new_target)

            new_reason = stripbadletters(new_reason)
            
            if len(new_name) >= min_entry_len and len(new_target) >= 2:
                if len(new_klasse) > 1 and new_klasse[0] in "123456789" and not new_klasse[1].isdigit():
                    new_klasse = "0" + new_klasse
                if len(new_reason) >= min_entry_len:
                    new_reason = " ("+new_reason+")"
                else:
                    new_reason = ""
                names = new_name.split(":")
                
                while names:
                    nextname = names.pop()
                    if len(nextname) >= min_entry_len:
                        persons.pdb_write_person(ti_ctx, new_klasse+": "+nextname,
                                                 new_target+new_reason, int(ti_ctx.sys.get_unix_time()))
                        anzahl += 1
                    else:
                        if not abgelehnte:
                            abgelehnte = " Abgelehnt wurde wg. zu kurzem Namen: "
                        else:
                            abgelehnte += ", "
                        abgelehnte += nextname
            else:
                resultatmeldung = f"-Die Felder Name und Ziel müssen mit mindestens {min_entry_len} Zeichen enthalten!"
        elif n2d >= 0:
            resultatmeldung = persons.pdb_delete_person(ti_ctx, n2d)
        if anzahl == 1:
            resultatmeldung = f'+Es wurde ein Personenruf für {nextname} eingetragen.{abgelehnte}'
        elif anzahl > 1:
            resultatmeldung = f'+Es wurden {str(anzahl)} Personenrufe eingetragen.{abgelehnte}'
            
    except():
        resultatmeldung = "-I/O-Fehler beim Namen einlesen! "

    # Nun die eigentlichen HTML-Ausgaben organisieren
        
    if resultatmeldung:
        ti_ctx.set_erfolgsmeldung(resultatmeldung)
    left, right = get_edit_persons_form(ti_ctx, pnr)
    md_response.append_s(left, 0)
    md_response.append_s(right, 1)

    return md_response


if __name__ == '__main__':
    ti_lib.quickabort2frames()
