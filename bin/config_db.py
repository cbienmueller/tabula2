'''
________________________________________________________________________
 config_db.py                                                            
 EN: This file is part of tabula.info, which is free software under              
     the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß          
         der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 config_db.py kapselt die Zugriffe auf die Konfigurationsdatenbank.
 Die Zugriffe werden von allen Modulen an ein Objekt der Klasse 
 TI_Config gerichtet, welches an geeigneter Stelle erzeugt wird.
 
 Es werden bewusst keine anderen ti-Module außer dem 
 unproblematischen ti_lib aufgerufen!
________________________________________________________________________
'''

# Batteries
import sqlite3
import time
import socket
from dataclasses import dataclass
import random

# TI
import ti_lib
import client


@dataclass
class InfoPixCfg:
    name: str = "Fehler"
    prio: int = 1
    lastday: str = "1970-00-01"
    lasthour: int = 23
    author: str = ""
    upload_epoch: int = 0
            
    def __get_thumbnail(self):
        vor, ext = str(self.name).split(".")
        return f'{vor}-thumb.{ext}'

    def __get_id(self):
        return str(self.name).split(".")[0]
     
    def __get_ext(self):
        return str(self.name).split(".")[1]

    thumbnail = property(__get_thumbnail)
    ext = property(__get_ext)
    ipid = property(__get_id)
    
    def as_tupel(self):
        return (self.name, self.prio, self.lastday, self.lasthour, self.author, self.upload_epoch)
    

@dataclass
class Calendar:
    cal_id: str = ""      
    url: str = ""       
    last_ts: int = 0    
    delta_t_min: int = 60
    gflag: int = 0
    error: str = ""
    bcolor: int = 160
    
    def as_tupel(self):
        return (self.cal_id, self.url, self.last_ts, self.delta_t_min, self.gflag, self.error, bcolor)
    

class TI_Config:
    
    def __init__(self, ti_paths, werkzeug_version=""):
        self.paths = ti_paths  # ggf. redundant, aber vereinfacht die Nutzung
        self.werkzeug_version = werkzeug_version
        try:
            wv=werkzeug_version.split(".")
            self.werkzeug_major = int(wv[0])
            self.werkzeug_minor = int(wv[1]) 
        except Exception as e:
            self.werkzeug_major = 0
            self.werkzeug_minor = 0
        
        # Verbindung zur Datenbank herstellen
        try:
            self.conf_db_conn = sqlite3.connect(self.paths.get_data_path('configdb.sq3'))
            conf_db_cursor = self.conf_db_conn.cursor()
            # Standarddefinitionen, auf die "gebaut" werden kann - nicht mehr ändern!!
            conf_db_cursor.execute('CREATE TABLE IF NOT EXISTS configpairs (parameter TEXT PRIMARY KEY, wert TEXT) ')
            conf_db_cursor.execute('''CREATE TABLE IF NOT EXISTS clients 
                                        (   ip TEXT PRIMARY KEY, 
                                            timestamp INTEGER DEFAULT 0, 
                                            name TEXT, 
                                            gruppen INTEGER DEFAULT 0, 
                                            maxphase INTEGER DEFAULT 0, 
                                            istconf INTEGER DEFAULT 0) ''')
            # Weitere Definitionen oder Abweichungen weiter unten in dem Test der dbversion einbauen
            self.conf_db_conn.commit()

        except Exception as e:
            import ti2
            ti2.panic(_('Keine Verbindung zur Konfigurationsdatenbank configdb.sq3. Rechte falsch gesetzt?'), e, 
                      'basepath:' + self.paths.get_basepath().as_posix() + 'data_path:' + 
                      self.paths.get_data_path().as_posix() + '<br>username:' + self.paths.username)

        self.clientgroups = 0
        self.conf_db_upgrade()
        
    def get_db_cursor(self):
        return self.conf_db_conn.cursor()

    def conf_db_upgrade(self):
        """Diese Routine stellt sicher, dass mit einer aktuellen Datenbank gearbeitet wird. 
        Die Überprüfung kostet quasi nichts."""
        dbversion = self.get_db_config('ticonfig_db_version', '') 
        db_cursor = self.get_db_cursor()
        if dbversion < '0.9.8b':
            try: 
                db_cursor.execute(
                    '''ALTER TABLE clients ADD COLUMN istanzg INTEGER DEFAULT 0''')
                # bewusste Erzeugung eines ersten Upgrades (real: von 0.9.8.alpha zu 0.9.8.beta) 
            except Exception:
                pass
            try: 
                db_cursor.execute(
                    '''ALTER TABLE clients ADD COLUMN cukopplung INTEGER DEFAULT 0''')
                # Clientname - Username - Kopplung für einfachere Rechtevergabe
            except Exception:
                pass
            self.set_db_config('ticonfig_db_version', '0.9.8b')
            dbversion = self.get_db_config('ticonfig_db_version', '') 

        if dbversion < '0.9.9':
            try: 
                db_cursor.execute(
                    '''ALTER TABLE clients ADD COLUMN forward TEXT DEFAULT ""''')
                # Erzeugung eines Upgrades (real: von 0.9.9.preview zu 0.9.9.final) 
            except Exception:
                pass
            # self.conf_db_conn.commit() in set_db_config enthalten
            self.set_db_config('ticonfig_db_version', '0.9.9')
            dbversion = self.get_db_config('ticonfig_db_version', '') 

        if dbversion < '0.9.X7':
            db_cursor.execute('DROP TABLE IF EXISTS s_files')
            db_cursor.execute('''CREATE TABLE s_files (
                                                name    TEXT PRIMARY KEY, 
                                                md5     TEXT, 
                                                checked INTEGER )''')  # neue Tabelle für s_any
            db_cursor.execute('DROP TABLE IF EXISTS s_any_vplan')
            db_cursor.execute('''CREATE TABLE s_any_vplan (
                                                epoche  INTEGER NOT NULL, 
                                                phase   INTEGER NOT NULL, 
                                                ziel    INTEGER NOT NULL, 
                                                spalten INTEGER DEFAULT 1, 
                                                url     TEXT, 
                                                dauer   INTEGER, 
                                                weite   INTEGER DEFAULT 0, 
                                                qf      INTEGER DEFAULT 0, 
                                                PRIMARY KEY (epoche, phase, ziel))''')  # neue Tabelle für alle s_any-Dateien

            self.set_db_config('ticonfig_db_version', '0.9.X7')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
        if dbversion < '1.1.a4':
            self.set_db_config('flag_s_any_html_ganzseitig', self.get_db_config_bool('flag_s_any_html_zweispaltig', False))
            try: 
                db_cursor.execute('''ALTER TABLE clients ADD COLUMN minphase INTEGER DEFAULT 0''') 
            except Exception:
                pass
            db_cursor.execute('''UPDATE clients SET maxphase=10 WHERE maxphase=0''')
            self.set_db_config('ticonfig_db_version', '1.1.a4')
            dbversion = self.get_db_config('ticonfig_db_version', '') 

        if dbversion < '1.9.b':
            """Die neue Tabelle nimmt Infos zu den Info-Bildern auf.
                Der name enthält typischerweise den md5-hash der Datei.
                Die Datei sollte dann <hash>.png oder <hash>.gif heissen.
                Davon abgeleitet gibt es die Thumbnail-Datei <hash>-thumb.png/gif, die natürlich selbst
                 einen anderen hash hat, hier aber unter dem hash der Quelldatei geführt wird.
                In dieser Tabelle wird aber nur der reale Name der "großen" Bilddatei gespeichert.
                Implizit wird der Dateiname als name.extension, also ohne weiteren Punkt, angenommen.
            """
            db_cursor.execute('DROP TABLE IF EXISTS infopix')
            db_cursor.execute('''CREATE TABLE infopix (
                                                name     TEXT, 
                                                prio     INTEGER default 1, 
                                                lastday  TEXT default "2222-12-31", 
                                                lasthour INTEGER DEFAULT 23, 
                                                author   TEXT default "", 
                                                upload_epoch INTEGER default 0, 
                                                PRIMARY KEY (name))''')  # neue Tabelle für alle Info-Dateien

            self.set_db_config('ticonfig_db_version', '1.9.b')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
        
        if dbversion < '2.0.b':
            migratepairs = [("LoginZeit", "Login_Duration"), 
                            ("ManagementNetzwerk", "ManagementNetwork"), 
                            ("Flag_ZeigeInfos", "Flag_ShowInfos"), 
                            ("SubTextlinks", "SubTextL"), 
                            ("SubTextrechts", "SubTextR")]
            for par_alt, par_neu in migratepairs:
                if self.get_db_config(par_neu, "") == "":
                    oldvalue = self.get_db_config(par_alt, "")
                    if oldvalue:
                        self.set_db_config(par_neu, oldvalue)
                self.remove_db_config(par_alt)
            # remove info_by_*
            db_cursor.execute(
                '''DELETE FROM configpairs 
                    WHERE parameter IN (
                        SELECT parameter FROM configpairs WHERE parameter LIKE "%info_by_%"
                    )''')
            self.set_db_config('ticonfig_db_version', '2.0.b')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
        if dbversion < '2.0.c':
            db_cursor.execute('DROP TABLE IF EXISTS s_files')
            db_cursor.execute('''CREATE TABLE s_files (
                                                name      TEXT PRIMARY KEY, 
                                                md5       TEXT, 
                                                timestamp INTEGER DEFAULT 0,
                                                checked   INTEGER DEFAULT 0)''')  # neue Tabelle für s_any
            self.set_db_config('ticonfig_db_version', '2.0.c')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
            
        if dbversion < '2.0.e':
            db_cursor.execute('''UPDATE clients SET maxphase=99 WHERE maxphase>8''')
            self.set_db_config('ticonfig_db_version', '2.0.e')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
        if dbversion < '2.1.e':
            db_cursor.execute('''DROP TABLE IF EXISTS calendars''')
            db_cursor.execute('''CREATE TABLE calendars (
                                                cal_id      TEXT PRIMARY KEY, 
                                                url         TEXT,
                                                last_ts     INTEGER DEFAULT 0,
                                                delta_t_min INTEGER DEFAULT 60,
                                                gflag       INTEGER DEFAULT 0,
                                                error       TEXT DEFAULT "",
                                                color       TEXT DEFAULT "",
                                                bcolor      TEXT DEFAULT "")''')  # neue Tabelle für online-Kalender
            self.set_db_config('ticonfig_db_version', '2.1.e')
            dbversion = self.get_db_config('ticonfig_db_version', '') 
    
    # ## s_any ##        
    def get_s_any_2unix_times(self):
        'Liefert die letzten beiden unix_times'
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT DISTINCT epoche FROM s_any_vplan ORDER BY epoche DESC LIMIT 2')
        erste = 0
        zweite = 0
        for zeile in db_cursor:
            zweite = erste
            erste = zeile[0]
        return erste, zweite

    def get_s_any_aktuelle_unix_time(self):
        'Liefert die aktuelle unix_time'
        return self.get_db_config_int('s_any_akt_unix_time', 0)
        
    def set_s_any_aktuelle_unix_time(self, akt_unix_time):
        '''Setzt die aktuelle Epoche
         - sollte also nach allen set_s_any_vplan aufgerufen werden
         - löschte alle vor der vorletzen Epoche (unix_time) in der DB
         - Liefert Liste diese beiden Epochen zurück, um ein Verzeichnislöschen der restlichen zu ermöglichen'''
        db_cursor = self.get_db_cursor()
        self.set_db_config('s_any_akt_unix_time', akt_unix_time)
        self.conf_db_conn.commit()
        erste, zweite = self.get_s_any_2unix_times()
        keep_zeiten = [str(erste), str(zweite)]
        db_cursor.execute('DELETE FROM s_any_vplan WHERE epoche NOT IN (?, ?)', (erste, zweite)) 
        self.conf_db_conn.commit()
        return keep_zeiten

    def get_s_any_vplan_liste(self, ziel, debuginfo=''):
        'liefert eine Liste mit S_any_vplan-Objekten für das Ziel - mit default-Dateien, wenn es für das Ziel nichts gibt'
        akt_unix_time = self.get_s_any_aktuelle_unix_time()
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
            '''SELECT phase, spalten, url, dauer, weite 
                    FROM s_any_vplan 
                    WHERE epoche=? AND ziel=? 
                    ORDER BY phase ASC''', (akt_unix_time, ziel))
        liste = []
        for zeile in db_cursor:
            neuer_s_any_vplan = ti_lib.S_any_vplan(akt_unix_time, ziel, phase=zeile[0], 
                                                   spalten=zeile[1], url=zeile[2], 
                                                   dauer=zeile[3], weite=zeile[4])
            liste.append(neuer_s_any_vplan)
        if liste:
            debuginfo += 'Es gibt {} Pläne f. Ziel {}'.format(len(liste), ziel)
            return liste, debuginfo
        # keine Zielpläne?
        if ziel != 0:                         # wenn noch nicht default
            return self.get_s_any_vplan_liste(0, f'Keine {ziel}-Pläne! ')  # dann defaultpläne
        debuginfo += 'Es gibt keine Pläne!{},{}'.format(len(liste), ziel)
        return [], debuginfo  # sehr doof, es gibt also keine default-Pläne

    def set_s_any_vplan(self, s):
        'schreibt den db-Eintrag für eine vplan-Datei, die als S_any_vplan-Objekt übergeben wurde'
        db_cursor = self.get_db_cursor()
        db_cursor.execute('''INSERT INTO s_any_vplan 
                                    (epoche, ziel, phase, spalten, url, dauer, weite, qf) 
                                    VALUES  (?, ?, ?, ?, ?, ?, ?, ?)''', 
                          (s.unix_time, s.ziel, s.phase, s.spalten, 
                           s.url, s.dauer, s.weite, s.qf))
        self.conf_db_conn.commit()  # weil sonst zu viel Zeit bis zum commit vergeht (db locked)

    # ## s_files ##
    def s_files_get_md5_ts(self, name):
        db_cursor = self.get_db_cursor()
        try:
            db_cursor.execute('SELECT md5, timestamp FROM s_files WHERE name=?', (name, ))
            for row in db_cursor:
                return row[0], row[1]
        except Exception:  # egal...
            pass
        return '', 0

    def s_files_set_md5(self, name, md5, ts):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('INSERT OR REPLACE INTO s_files (name, md5, timestamp, checked) VALUES (?, ?, ?, 1)',
                          (name, md5, ts))
        self.conf_db_conn.commit()

    def s_files_count_n_delete_unchecked(self):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT count(name) FROM s_files WHERE checked=0')
        for row in db_cursor:
            anzahl = row[0]
        db_cursor.execute('DELETE FROM s_files WHERE checked=0')
        self.conf_db_conn.commit()
        return anzahl

    def s_files_uncheck(self):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE s_files SET checked=0')
        self.conf_db_conn.commit()

    # ## Calendar ##
    def get_calendars(self):
        resultat = []
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT cal_id, url, last_ts, delta_t_min, gflag, error, bcolor FROM calendars ORDER BY cal_id')
        for row in db_cursor:
            resultat.append(Calendar(*row))
        return resultat
        
    def set_calendar(self, cal_id, url, delta_t_min, gflag, bcolor=160):
        for c in cal_id:
            if not (c.isalpha() or c.isdigit() or c in "_"):
                return '{_("Verbotenes Zeichen in der ID")}'
        if bcolor < 0 or bcolor > 360:
            return _('Farb-Wert nicht 0...360')
        db_cursor = self.get_db_cursor()
        if url:
            try:
                db_cursor.execute(
                    '''INSERT OR REPLACE INTO calendars 
                        (cal_id, url, last_ts, delta_t_min, gflag, error, bcolor) 
                        VALUES (?, ?, ?, ?, ?, ?, ?)''', 
                    (cal_id, url, 0, delta_t_min, gflag, "neu", bcolor))
            except Exception:
                return _("DB-Zugriff fehlgeschlagen")
        else:
            try:
                db_cursor.execute(
                    'DELETE FROM calendars WHERE cal_id= ?', 
                    (cal_id, ))
            except Exception:
                return _("DB: Löschen fehlgeschlagen")
        self.conf_db_conn.commit()
        return ""
                
    def set_calendar_ts(self, cal_id, last_ts):
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
                'UPDATE calendars SET last_ts = ?, error="" WHERE cal_id = ?', 
                (last_ts, cal_id))
        self.conf_db_conn.commit()
        
    def set_calendar_error(self, cal_id, meldung):
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
                'UPDATE calendars SET error = ? WHERE cal_id = ?', 
                (meldung, cal_id))
        self.conf_db_conn.commit()
                
    # ## db_config ##
    def get_db_config(self, parameter, default=''):
        db_cursor = self.get_db_cursor()
        wert = default
        try:
            db_cursor.execute('SELECT wert FROM configpairs WHERE parameter=?', (parameter, ))
            for row in db_cursor:
                wert = row[0].strip()
        except Exception:  # egal was schief geht
            pass
        return wert

    def get_db_config_int(self, parameter, default=-1):
        return ti_lib.strg2int(self.get_db_config(parameter), default)
        
    def get_db_config_bool(self, parameter, default=False):
        return ti_lib.strg2bool(self.get_db_config(parameter), default)

    def remove_db_config(self, parameter):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('DELETE FROM configpairs WHERE parameter=?', (parameter, ))
        self.conf_db_conn.commit()

    def set_db_config(self, parameter, wert, bedingung=''): 
        # wenn die bedingung nichtleer ist, dann wird das Setzen nur gemacht, 
        #   wenn der alte Wert genau gleich der bedingung ist.
        # Dies ermöglicht quasiatomare Änderungen von einem gegebenen/angenommenen Wert aus.
        
        wert = str(wert).strip()
        db_cursor = self.get_db_cursor()
        if not bedingung:
            db_cursor.execute(
                'INSERT OR REPLACE INTO configpairs (parameter, wert) VALUES (?, ?)', 
                (parameter, wert))
        else:
            db_cursor.execute('''UPDATE configpairs 
                                        SET wert=?
                                        WHERE parameter=? AND wert=?''', (wert, parameter, bedingung))
        self.conf_db_conn.commit()
        
    def dump_db_config(self):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT * FROM configpairs ORDER BY parameter')
        return db_cursor.fetchall()
    
    def get_gruppenbez(self, nr):
        gruppenname = ['ClientTypeQ1', 'ClientTypeQ2', 'ClientTypeA', 'ClientTypeB', 'ClientTypeM', 'Lehrer', 
                       'Soloscreen', 'Anzeige', 'Forward', 'RaspberryPi', 'hochkant']
        gruppenkuerzel = ['Q_1', 'Q_2', 'A', 'B', 'M', 'Leh.', 
                          'Solo', 'Anzg', 'Fwd', 'Rπ', 'hk.']  
    # die kompletten Namen sind in ti.py
        # für 0-4 sind die Namen die Parameter in der Config-Datenbank, welche dann die Namen liefert
        if nr in range(5):
            erg = self.get_db_config(gruppenname[nr], 'CT' + gruppenname[nr][-1])
            return erg, erg[:4]
        if nr < len(gruppenname):
            return gruppenname[nr], gruppenkuerzel[nr]
        return "", ""
        
    # ### client ###
    """neu 2023-11-20:
        * Spalte ip ist nur für interne Netze sinnvoll, daher kann es für externe Zugriffe umgewidmet werden:
        * CODE:A3C-F4K-Q2P wird vorgegeben und am Browser i.d.R. einmalig eingegeben. Dann wird der Code ersetzt durch
        * ID:123456789875643463456435645664566 welche man im Cookie wiederfindet.
        * Spalte name wird dadurch relevanter und muss bei der CODE-Erzeugung eingegeben werden
        * Zukunft: Man kann auch z.B. 20 Clients mit durchnummerierten Namen mit gleichem CODE erzeugen lassen und 
        *          es wird immer nur der erste gefundene in ID umgewandelt. Damit können 20 Clients individuell 
        *          authentifiziert werden.
    """
    # def client_get_config(self, ip):
    #    db_cursor = self.get_db_cursor()
    #    db_cursor.execute('SELECT name, timestamp, minphase, maxphase, gruppen FROM clients WHERE ip=?', (ip, ))
    #    for row in db_cursor:
    #        return row
    #    return 'Fehler', 0, 0, 0, 0

    def client_get_all_ORG(self, ip):
        """Liest aus der db die Clientkonfiguration aus."""
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
            '''SELECT name, timestamp, minphase, maxphase, gruppen, istanzg, cukopplung, forward
                FROM clients WHERE ip=?''', (ip, ))
        for row in db_cursor:
            return row
        return 'Fehler', 0, 0, 99, 0, 0, 0, ''
        
    def client_read_ti_client(self, ip):
        """Liest aus der db die Clientkonfiguration aus und gibt Dataclass-Objekt zurück"""
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
            '''SELECT ip, name, timestamp, minphase, maxphase, gruppen, istanzg, cukopplung, istconf, forward
                FROM clients WHERE ip=?''', (ip, ))
        for row in db_cursor:
            return client.TI_Client(*row)   # Tupel als Parameterliste übergeben
        return None
        
    def client_force_gruppen(self, clientgroups):
        self.clientgroups = clientgroups
        
    def client_get_gruppen(self, ip):
        if self.clientgroups > 0:
            return self.clientgroups
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT gruppen FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return row[0]
        return 0

    def client_is_portrait(self, ip):  # Display hochkant?
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT gruppen FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return (row[0] & 1024) > 0
        return False

    def client_get_forward(self, ip):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT forward FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return row[0].strip()
        return ''

    def client_get_istanzg(self, ip):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT istanzg FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return row[0]
        return 0

    def client_get_cukopplung(self, ip):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT cukopplung FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return row[0]
        return 0
        
    def client_get_name(self, ip):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT name FROM clients WHERE ip=?', (ip, ))
        for row in db_cursor:
            return row[0]
        return ''

    def client_set_gruppen(self, ip, gruppen):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE clients SET gruppen=? , istconf=1 WHERE ip=?', (gruppen, ip))
        self.conf_db_conn.commit()
        
    def client_set_forward(self, ip, fwd):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE clients SET forward=? , istconf=1 WHERE ip=?', (fwd, ip))
        self.conf_db_conn.commit()
        
    def client_set_istanzg(self, ip, istanzg):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE clients SET istanzg=? , istconf=1 WHERE ip=?', (istanzg, ip))
        self.conf_db_conn.commit()
        
    def client_set_cukopplung(self, ip, cukopplung):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE clients SET cukopplung=? , istconf=1 WHERE ip=?', (cukopplung, ip))
        self.conf_db_conn.commit()
        
    def client_set_name(self, ip, name):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('UPDATE clients SET name=? , istconf=1 WHERE ip=?', (name, ip))
        self.conf_db_conn.commit()

    def client_get_list_ORG(self, sortbyname):
        db_cursor = self.get_db_cursor()
        
        db_cursor.execute(
            f'''SELECT ip, name, timestamp, maxphase, gruppen, istconf, istanzg, cukopplung, forward
                FROM clients ORDER BY istconf DESC, istanzg DESC, {("name" if sortbyname else "ip")} ASC''')
        return db_cursor.fetchall()
        
    def client_get_list(self, sortbyname):
        db_cursor = self.get_db_cursor()
        erg = []
        
        db_cursor.execute(
            f'''SELECT ip, name, timestamp, minphase, maxphase, gruppen, istanzg, cukopplung, istconf, forward
                FROM clients ORDER BY istconf DESC, istanzg DESC, {("name" if sortbyname else "ip")} ASC''')
        for row in db_cursor:
            erg.append(client.TI_Client(*row))    # Tupel als Parameterliste übergeben
        return erg    
            
    def client_set_phaselimits(self, ip, mipha, mapha):
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
            'UPDATE clients SET minphase=? , maxphase=?, istconf=1 WHERE ip=?', 
            (mipha, mapha, ip))
        self.conf_db_conn.commit()

    def client_show_status(self):
        import ti
        ausgabe = []
        t_schwelle = ti_lib.get_now_unix_time() - 190  # 3min + 10Sek. Karenzzeit
        felddefinition = '''
        <div style="float:left;margin:0.5em;border:1;border-style:solid;border-color:white;border-radius:5px;
        padding:5px;text-align:center;vertical-align:middle;
        width:10em;height:9em;'''
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT ip, name, timestamp FROM clients WHERE istanzg>0 ORDER BY name ASC')
        for row in db_cursor:
            ip, name, timestamp = row
            lasttime = ti.get_datestring_from_unix_time(timestamp)
            if timestamp < t_schwelle:
                farbe = 'orange'
                delta_t = int((ti_lib.get_now_unix_time() - timestamp) / 60)
                if delta_t < 120:
                    kommentar = f'''<br>{str(delta_t)} {_('Minuten')} {_('ausgefallen')}'''
                else:
                    h = int(delta_t / 60)
                    if h < 48:
                        kommentar = f'''<br>{_('über')} {str(h)} {_('Stunden')} {_('ausgefallen')}'''
                    else:
                        kommentar = f'''<br>{_('über')} {str(int(h / 24))} {_('Tage')} {_('ausgefallen')}'''
                        if h / 24 > 14:
                            farbe = "grey"
                kommentar = f'<i>{kommentar}</i>'
            else:
                farbe = 'lightgreen'
                kommentar = ""
            ausgabe.append(f'''{felddefinition}background-color:{farbe};"><br>
                        <b>{name}</b><br><small>({ti_lib.readable_ip(ip)})</small><br>{lasttime + kommentar}</div>''')
        ausgabe.append(felddefinition + '''background-color:lightgrey;color:grey;">
            <small>Status der Anzeigen!<br>Die Zeilen sind:<br>
            <b>Der Clientname</b>, <br>
            <small>(die IP-Adresse)</small><br>
            Datum & Uhrzeit des letzten Zugriffs auf das Tabula-System.</small>
        </div>
        ''')
        return ausgabe
        
    def client_ping(self, ip, ti_response):
        # Hier wird nicht nur der aktuelle Zeitpunkt des Aufrufs vermerkt, 
        # sondern ggf. auch erst ein neuer Eintrag für einen neuen client angelegt.
        ti_response.debug('request durch IP', ti_lib.readable_ip(ip), 'für Site', self.paths.site)
        db_cursor = self.get_db_cursor()
        db_cursor.execute('SELECT timestamp FROM clients WHERE ip=?', (ip, ))
        _now_ = time.time()  # sec since epoche
        for result, in db_cursor:
            if int(_now_) > int(result) + 10:  # frühestens nach 10 Sekunden aktualisieren wg. Denial of Service Gefahr
                try:
                    db_cursor.execute('UPDATE clients SET timestamp=? WHERE ip=?', (int(_now_), ip))
                    self.conf_db_conn.commit()
                except Exception as e:
                    import ti2
                    ti2.panic("Schreibzugriff auf Datenbank scheiterte", e)
            return
            
        # aha, es gibt den Client noch nicht
        try:
            # Namen ermitteln
            ti_response.debug("IP", ip)
            hostname, alias, adressen = socket.gethostbyaddr(ip)
            ti_response.debug("hostname", hostname)
            hostname = hostname.split(".")[0]
        except Exception:  # egal welche...
            hostname = ip
        try:
            db_cursor.execute(
                'INSERT INTO clients (ip, name, timestamp, maxphase) VALUES (?, ?, ?, ?)', 
                (ip, hostname, _now_, 99))
            self.conf_db_conn.commit()
        except Exception as e:
            ti_response.debug('INSERT fehlgeschlagen', e, '')
            
    def client_anlegen_mit_code(self, name):
        db_cursor = self.get_db_cursor()
        _now_ = time.time()  # sec since epoche

        cs = "ACEFGHJKLMNPQRSTUVWXYZ"
        ns = "2345679!?"
        code = 'CODE:'
        for i in range(3):
            code += cs[random.randint(0, len(cs) - 1)]
            code += ns[random.randint(0, len(ns) - 1)]
            code += cs[random.randint(0, len(cs) - 1)]
            code += "-" if i < 2 else ""
        try:
            db_cursor.execute(
                'INSERT INTO clients (ip, name, timestamp, maxphase, istconf) VALUES (?, ?, ?, ?, ?)', 
                (code, name, _now_, 99, 1))
            self.conf_db_conn.commit()
            return None
        except Exception as e:
            return ('INSERT mit code fehlgeschlagen', e)
            
    def client_code2id(self, ip):
        """ der betroffene, durch den in der ip enthaltenen Code identifizierte Client 
            wird umgestellt auf Identifikation mit einer 48-stelligen ID. Der Code verfällt.
            Die ID wird zurückgegeben.
        """
        db_cursor = self.get_db_cursor()
        theid = hex(random.getrandbits(48 * 4))
        newip = f'ID:{theid}'
        db_cursor.execute('UPDATE clients SET ip = ? WHERE ip = ?', (newip, ip))
        self.conf_db_conn.commit()
        return theid
            
    def client_remove(self, ip):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('DELETE FROM clients WHERE ip=?', (ip, ))
        self.conf_db_conn.commit()

    def client_remove_oldz(self):
        db_cursor = self.get_db_cursor()
        db_cursor.execute('DELETE FROM clients WHERE istanzg = 0 AND istconf = 0 AND timestamp < ?', 
                          (ti_lib.get_now_unix_time() - 14 * 24 * 3600, ))
        self.conf_db_conn.commit()

    def check_n_set_semaphore(self, name, onlycheck=False):
        now = int(time.time())
        timeout = self.get_db_config(name + "_sema")
        # print(timeout, ">", now, "?")
        if timeout.isdigit():
            if now < int(timeout):
                return -1  # heisst: du musst noch warten
        if not onlycheck:
            self.set_db_config(name + "_sema", str(now + 15 * 60))
        return 0  # heisst: du darfst
            
    def clear_semaphore(self, name):
        now = int(time.time())
        self.set_db_config(name + "_sema", str(now - 1))
        
    def set_ti_lastupload(self, typ=''):
        unix_time = int(time.time())
        self.set_db_config('uploadtimestamp', unix_time)
        self.set_db_config('uploadtimestamp_klartext', ti_lib.unix_time_2_human_date_n_time(unix_time))
        if typ in ['Plan', 'Info']:
            self.set_db_config('uploadtimestamp_klartext_' + typ, ti_lib.unix_time_2_human_date_n_time(unix_time))

    def infopix_rm_cfg(self, filename):
        """Entfernt aus der db die infopix-Konfiguration zum Filename.
        """
        db_cursor = self.get_db_cursor()
        db_cursor.execute('DELETE FROM infopix WHERE name=?', (filename, ))
        self.conf_db_conn.commit()

    def infopix_get_full_cfg(self, filename):
        """Liest aus der db die infopix-Konfiguration zum Filename aus.
        
        Legt ggf. leeren Eintrag an.
        """
        db_cursor = self.get_db_cursor()
        for i in range(2):
            db_cursor.execute(
                '''SELECT name, prio, lastday, lasthour, author, upload_epoch FROM infopix WHERE name=?''', 
                (filename, ))
            for n, p, ld, lh, a, u in db_cursor:
                return InfoPixCfg(n, p, ld, lh, a, u)
            # wenn wir hier ankommen, dann gibt es noch keinen Config-Eintrag=> Anlegen! Loop! Neuer Versuch
            ip_cfg = InfoPixCfg(filename, upload_epoch=ti_lib.get_now_unix_time())
            self.infopix_set_full_cfg(ip_cfg)
        return InfoPixCfg(filename)  # sollte hier nie ankommen!

    def infopix_set_full_cfg(self, infopixcfg):
        """Schreibt das InfoPixCfg-Objekt in die Datenbank
        """
        db_cursor = self.get_db_cursor()
        db_cursor.execute(
            '''INSERT OR REPLACE INTO infopix 
                (name, prio, lastday, lasthour, author, upload_epoch) 
                VALUES (?, ?, ?, ?, ?, ?)''', 
            (infopixcfg.name, infopixcfg.prio, infopixcfg.lastday, infopixcfg.lasthour, 
                infopixcfg.author, infopixcfg.upload_epoch))
        self.conf_db_conn.commit()


if __name__ == '__main__':
    ti_lib.quickabort2frames()
