'''
________________________________________________________________________
 ti2.py                                                            
 EN: This file is part of tabula.info, which is free software under              
      the terms of the GPL without any warranty - see the file COPYING 
 DE: Diese Datei ist Teil von tabula.info, das Freie Software gemäß
          der GPL ohne jegliche Garantie ist - siehe die Datei COPYING 
________________________________________________________________________

 ti2.py ist der zentrale Aufruf von/für tabula.info in der Version 2
   Neu in Version 2: Aufruf via wsgi, systematischere Content-Verteilung
________________________________________________________________________

'''

# Batteries (included)
import sys
import os
import traceback
import importlib.metadata

# Importe von werkzeug (per apt installiert)
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.utils import redirect as w_u_redirect
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.middleware.shared_data import SharedDataMiddleware

# TI-Importe I (reine Konstanten)
import version
import html_barebone2
    

panic_text = ''


def panic(text, ausnahme, details='', requesterror=False):
    global panic_text
    if requesterror:
        headline = 'Request failed (Aufruf ist fehlgeschlagen)'
    else:
        headline = '<span>&nbsp;tabula.info&nbsp;</span>-app could not be initialised (Anwendung kann nicht gestartet werden)'
        
    exinfo0 = str(sys.exc_info()[0]).replace('<', '&lt;').replace('>', '&gt;')
    tb = (traceback.format_exc()+'\n ').replace('<', '&lt;').replace('>', '&gt;').split("\n", 1)
    panic_text = html_barebone2.PANIC_TEMPLATE.format(version.TI_KOMPAKT_VERSION,
                                                      headline,
                                                      text,
                                                      str(ausnahme),
                                                      exinfo0,
                                                      details,
                                                      tb[0],
                                                      ''.join(tb[1]),
                                                      version.TI_BRANDING)
    sys.stderr.write(panic_text)
    sys.stderr.flush()
    
    
# TI-Importe II (eigene Module)    
try:
    import paths
    import ajax2
    import config_db
    import ti_init
    import ti_context
    import composer
    import ti
    import ti_lib
    import login
    import man
    
except Exception as e:
    panic('Import in ti2 fehlgeschlagen!', e)


def hacklog(*args):
    sys.stderr.write("!! {}\n".format(": ".join([str(i) for i in args])))
    sys.stderr.flush()
    

class TiApp(object):
    app_number = 0   # nur zur Dokumentation, nicht sicher threadsafe
    # Kontruktor: Legt Zuordnung zwischen der URL und der aufgerufenen Funktion fest
        
    def __init__(self, config):
        # links URL-Pfad bezogen auf das aufgerufene Skript
        # Pfade kommen doppelt vor, damit per mod_wsgi@apache und per run_simple (s.u.) lauffähig
        # Diese Verdopplung erledigt rule2 durch doppelten Rückgabewert (im Array)
        def rule2(path, endpoint, methods=None, defaults=None):
            return [Rule(path, endpoint=endpoint, methods=methods, defaults=defaults),
                    Rule(f"/ti/do/{path}", endpoint=endpoint, methods=methods, defaults=defaults)]

        self.url_map = Map(
            rule2('/', 'forward') +
            rule2('/ti', 'forward') + 
            rule2('/frontend', 'frontend') +
            rule2('/ajax', 'ajax') +
            rule2('/manage', 'manage') +
            rule2('/manage/<task>', 'manage', methods=["GET"]) +
            rule2('/managepost/<task>', 'managepost', methods=["POST"]) +
            rule2('/userinput/<task>', 'userinput') +
            rule2('/debugfile', 'debugfile') +
            rule2('/frames', 'frames', defaults={'phase': 99}) +
            rule2('/frames/<int:phase>', 'frames') +
            rule2('/astatus', 'astatus') +
            rule2('/background', 'background') + 
            rule2('/signup', 'signup')
            # rule2('/sitecookie', 'sitecookie') +
            # rule2('/unblock', 'unblock')
        )
        hacklog("ti.app initialisiert")
        self.callcounter = 0
        self.app_number = TiApp.app_number
        TiApp.app_number += 1

        self.werkzeug_version = importlib.metadata.version("werkzeug")
        hacklog("werkzeug_version", self.werkzeug_version)
        
    # unsere App ist ein callable
    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    # Die Funktion wsgi_app() wird ggf. mehrfach parallel aufgerufen. 
    # Sie muss also alle dynamischen Variablen (bereits ti_paths und insbesondere ti_ctx) 
    #   selbst erzeugen und nicht in self ablegen!
    
    # Dies ist die zentrale, für jeden Request durch den Webserver einmal aufgreufene Funktion!
    def wsgi_app(self, environ, start_response):
        global panic_text
        
        self.callcounter += 1  # Nur zur Info. Race condition kann ignoriert werden
        
        request = Request(environ)
        
        site, action = self.audit_request(request)
        if not site:
            response = self.force_redirect('/ti/static/signup.html')
            return response(environ, start_response)
        
        if action == "SIGNUP":
            pass  # Hier muss der Login-Aufruf hin
            
        # hacklog("final site", site)
        
        ti_paths = paths.TI_Paths(site)
        # hacklog("data_path",ti_paths.get_data_path())
        
        if ti_paths.status == paths.PST_UNKNOWN:
            response = self.force_redirect('/ti/static/signup.html')
            return response(environ, start_response)
        if ti_paths.status == paths.PST_ERROR:
            panic_text = "Initialisation of path variables failed!"

        if panic_text:
            nixgeht = Response(panic_text, mimetype='text/html') 
            nixgeht.status = "500 Internal Server Error"
            nixgeht.status_code = 500
            return nixgeht(environ, start_response)
            
        ti_sys = ti_init.TI_System(ti_paths)  # Basis
        ti_cnf = config_db.TI_Config(ti_paths, self.werkzeug_version)  # Config-Datenbank
        ti_res = ti_init.TI_Response(ti_sys.get_unix_time())  # für die Ausgabe
        ti_req = ti_init.TI_Request(request, ti_cnf, ti_res)  # der Request
        if ti_req.block_extern and action == "TEST":  
            # Der Test gegen die Datenbank war noch offen und der Client fiel wohl durch
            response = self.force_redirect('/ti/static/signup.html?proxycodefalsch')
            return response(environ, start_response)
            
        # hacklog("Client", ti_req.client.ip_address, "Loginuser", ti_req.get_value("tiuser"),"PW_Laenge",
        #  len(ti_req.get_value("tipassword")))
        
        ti_ses = login.TI_Session(ti_req, ti_cnf, ti_sys.get_unix_time())

        ti_ctx = ti_context.TIConTeXt(ti_sys, ti_cnf, ti_req, ti_res, ti_ses, ti_paths)
        # ti_ctx wird nun jedem Tool zur Verfügung gestellt

        # in dem folgenden Aufruf wird die eigentlich gewünschte Nutz-Funktion aufgerufen und das Ergebnis zurückgegeben
        try:
            response = self.dispatch_request(request, ti_ctx, action)
        except Exception as e:
            panic('Request fehlgeschlagen!', e, requesterror=True)
            response = Response(panic_text, mimetype='text/html') 
            response.status = "500 Internal Server Error"
            response.status_code = 500
            
        return response(environ, start_response)
        
    def dispatch_request(self, request, ti_ctx, action):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            if (action == "SIGNUP" and endpoint not in ["signup"]) and ti_ctx.req.ist_extern: 
                raise HTTPException()
            
            if False: 
                hacklog(ti_ctx.req.client.ip, "ruft", endpoint, request.method, "- P:{},C:{},mp={},mt={}".format(
                    os.getpid(),
                    self.callcounter,
                    request. is_multiprocess,
                    request.is_multithread))
                    
            return getattr(self, 'do_' + endpoint)(request, ti_ctx, **values)
            
        except HTTPException as e:
            hacklog('Fehlschlag bei URL:', request.environ['REQUEST_URI'])
            return e

    # Ab hier kommen die Nutz-Funktionen:

    def do_forward(self, request, ti_ctx):
        return Response(html_barebone2.ROOT_FORWARD, mimetype='text/html')
        
    def do_frontend(self, request, ti_ctx):
        return Response(html_barebone2.FRONTEND, mimetype='text/html')

    def do_frames(self, request, ti_ctx,  **values):
        if ti.check_n_background(ti_ctx):
            ti_ctx.res.debug('Background noetig')
        res = Response(composer.compose_frames(ti_ctx,  **values), mimetype='text/html')
        return res
    
    def do_ajax(self, request, ti_ctx):
        return Response(ajax2.do_ajax(ti_ctx), mimetype='application/json')
        
    def do_astatus(self, request, ti_ctx):
        # res = Response('\n'.join(ti.astatus(self.ti_ctx, wsgi=True)), mimetype='text/html')
        res = Response(ti.astatus(ti_ctx, wsgi=True), mimetype='text/html')
        return res
        
    def do_background(self, request, ti_ctx):
        if ti.check_n_background(ti_ctx):
            ti_ctx.res.debug('Background noetig')
        return Response('<!DOCTYPE html>\n<html>empty</html>', mimetype='text/html')
     
    def do_manage(self, request, ti_ctx, **values):
        if ti.check_n_background(ti_ctx):
            ti_ctx.res.debug('Background noetig')
        htmlresponse, sessionid = man.do_the_managing(ti_ctx, **values)
        res = Response(htmlresponse, mimetype='text/html')
        if not sessionid:
            sessionid = ti_ctx.ses.setcookiesessionid  # Dann wird der Wert für vianetwork gesetzt
        if sessionid:
            hacklog("Ein Cookie zu setzen!", sessionid)
            res.set_cookie('ti_sessionid2', sessionid, path='/ti/do', samesite='Strict')
        return res
        
    def do_managepost(self, request, ti_ctx, **values):
        redirecttask, sessionid = man.do_manage_post(ti_ctx, **values)
        if redirecttask:
            redirecttask = f"/{redirecttask}"
        res = w_u_redirect(f"/ti/do/manage{redirecttask}", 303)
        if sessionid:
            res.set_cookie('ti_sessionid2', sessionid, path='/ti/do', samesite='Strict')
        return res

    def do_userinput(self, request, ti_ctx, **values):
        task = values.get("task", "")
        if task == "turbo" and ti_ctx.cnf.get_db_config_bool("flag_turboplan") and ti_ctx.cnf.get_db_config_bool("flag_s_turbo_userinput"):
            import s_turbo_userinput
            return Response(s_turbo_userinput.do_input(request,ti_ctx), mimetype='application/json')
        raise NotFound("In Ihrer Konfiguration nicht verfügbar.")
        
    def do_debugfile(self, request, ti_ctx):
        # hacklog("Debugfile angefordert")
        res = Response(man.do_debugfile(ti_ctx), 
                       headers=[('Content-Disposition', f'attachment;filename=debug-{ti_lib.get_now4filenames()}.html.xz')],
                       mimetype='application/octet-stream')
        # hacklog("Debugfile erstellt")
        return res
     
    def do_signup(self, request, ti_ctx, **values):
        siup = ti_ctx.req.get_value("ti_signup", "")
        site = ti_ctx.req.get_value("ti_site", "").strip()
        code = ti_ctx.req.get_value("ti_code", "").strip()
        if siup and site and code:
            ip = f'CODE:{code}'
            hacklog("signup", siup, site, code, ip)
            ti_client = ti_ctx.cnf.client_read_ti_client(ip)
            if ti_client:  # Also wurde der Code erkannt!
                newid = ti_ctx.cnf.client_code2id(ip)
                site_clientID = f'{ti_ctx.paths.site}.{newid}'
                res = w_u_redirect("/ti/do/frames", 303)
                res.set_cookie('ti_site_clientID', 
                               site_clientID,
                               path='/ti/do', 
                               samesite='Strict',
                               expires=ti_lib.get_now_unix_time()+3600*24*365)
            else:
                res = w_u_redirect("/ti/static/signup.html?wrongcode", 303)
        else:
            res = w_u_redirect("/ti/static/signup.html?missing_values", 303)
        return res
            
    def force_redirect(self, ziel):
        res = w_u_redirect(ziel, 303)
        return res

    def audit_request(self, request):
        import ti_init  # ???
        """ Hier wird der Request daraufhin überprüft, ob er legal ist.
            Als Nebenprodukt und Kennzeichen wird die site als String zurückgegeben, auf die sich der Request bezieht. 
                "",*:               Abweisen
                <site>, "SIGNUP":   Client will sich erst legitimieren und ein Cookie erhalten
                <site>, "":         erkannte Wunsch-Site, normaler Dispatch
            Legal ist der Request, wenn eine der nummerierten Bedingungen erfüllt ist. Reihenfolge von speziell zu allgemein!
                1 interne Adresse oder externe Adresse via trusted Proxy mit SIGNUP-Wunsch
                    site->"SIGNUP"
                2 interne Adresse oder externe Adresse via trusted Proxy mit Site-Cookie
                    Client überträgt Cookie inkl. site
                3 via trusted Proxy legitimierte (Base-Login) externe Adresse
                    Proxy erzeugt X-HEADER inkl. site, z.B: KGA-12345
                    KGA wird dann hier geprüft, die Zahl im Konstruktor von ti_req
                4 interne IP-Adresse oder trusted Proxy mit einer internen Adresse
                    site->"local"
            Den Test, ob site existiert macht dann in wsgi_app der Aufruf von ti_paths
        """
        site = ""
        # Bedingung 1: Wird SIGNUP-Wunsch per POST übergeben?
        if "ti_site" in request.form and "ti_signup" in request.form:
            site = request.form["ti_site"]
            return site, "SIGNUP"

        # Bedingung 2: Wird Site-Cookie übertragen?
        else:
            site, clientID = ti_init.get_site_clientID(request.environ)
            # hacklog("audit2", site, ti_lib.readable_ip("ID:"+clientID), True)
            if site and len(clientID) > 48:  # Die Kontrolle der clientID erfolgt später in ti_init
                return site, "TEST"

        # Bedingung 3: Ermittle, ob ein Proxy legitimiert (wird bei Client-Init detaillierter überprüft)
        if 'HTTP_X_TISITE' in request.environ:
            if '-' in request.environ['HTTP_X_TISITE']:
                site, ticode = request.environ['HTTP_X_TISITE'].split('-')
                if site:
                    return site, "TEST"

        # Bedingung 4:
        try:
            ip_address = request.remote_addr
            if ti_lib.ip_in_local_networks(ip_address):
                return "local", "TEST"
        except Exception:
            pass
            
        return "", ""  # Access denied
        
        
# # Hier wird die eigentliche App erzeugt - diese muss als callable multithreaded aufgerufen werden können
def create_app(selfcontained=False):
    # eigentliche App anlegen, Demodaten werden nicht benutzt
    app = TiApp({
        'redis_host':       123, 
        'redis_port':       8000
    })
    # FIXME - App ergänzen, damit statische Inhalte unterhalb von www gezeigt werden
    if selfcontained:
        ti_paths = paths.TI_Paths('local')
        hacklog('App liefert statischen Content selbst per WSGI aus.')
        app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
            '/ti/static':  ti_paths.get_basepath('www/static').as_posix(), 
            '/ti/web/local/dyn':     ti_paths.get_dyn_path().as_posix(), 
            '/ti/web/local/local':   ti_paths.get_dyn_path('../local').resolve().as_posix(),
        })
    return app
    

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = create_app(True)
    run_simple('0.0.0.0', 5000, app, use_debugger=True, use_reloader=True, processes=4)
