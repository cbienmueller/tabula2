// Dieser Code erledigt Kleinigkeiten
// Kontext: frontend von tabula.info
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)

////////////////////////////////////
//          Countdownzaehler
// globals:
var d_s_handle;         // nimmt das setInterval-Objekt auf
var ti_time_element;    // Element im DOM, das die verbleibenden Sekunden anzeigt
var counter=30;         // Sekunden, bis die Seite neu geladen werden soll (Countdown)
var plaene=[ {  content:    "<h3>Initialisiere</h3>",
                timeout:    10 } ];             // array aus Objekten
//
// Wird 1x z.B. aus dem Body-Tag aufgerufen
function ti_start() {
    // Rufe alle 60 Sekunden die Nachrichten ab...
    m_s_handle=window.setInterval( function(){ call_ajax_msections() }, 60000);
    call_ajax_msections();
    call_ajax_plans();
    nextplan(); //ruft sich dann immer selbst wieder auf
    // Lasse Countdown zaehlen
    ti_time_element=document.getElementById("ti_time");
    if (ti_time_element) {
        c=parseInt(ti_time_element.innerHTML)
        if (c>0) counter=c;
        do_countdown();
        d_s_handle=window.setInterval( function(){ do_countdown() },1000); // Zeitintervall in Millisekunde
    }
}
///////////////////////////////////////////////////////////////////////////
// Wird sekuendlich aufgerufen und aktualisiert den Countdown zum Reload
function do_countdown() {
    ti_time_element.innerHTML=" "+counter+"s";
    if (counter>1)  {        counter--;    }
    else            {        counter=300;  call_ajax_plans();  }
}

var plannumber=0;
var nextplan=function() {
    plannumber++;
    if (plannumber>=plaene.length) {
        plannumber=0; }
    console.log("nextplan: "+plannumber+" fuer "+plaene[plannumber].timeout+" Sekunden");
    compare_delay_load('#contentp',plaene[plannumber].content,100);
    window.setTimeout(nextplan, plaene[plannumber].timeout*1000);
}
    
/////////////////////////////////////
//        Selektiv Inhalte aktualisieren

///////////////////////////////////////        Plaene aktualisieren
var call_ajax_plans=function() {
    $('#alerter').html('<span style="color:yellow;">🗘</span>');
    $.getJSON  (   {    url:    "/ti/do/ajax",
                        data:   "task=get_plan",
                        success: function(result) 
                                    {   $('#alerter').html('<span style="color:green;">🗸</span>'); // Da Netzwerkoperation erfolgreich
                                        if (result["status"]==='ok') {
                                            plaene=result["plans"];
                                            compare_delay_load('#contentp',result["plans"][0],100);
                                            compare_delay_load('#contentp',result["plans"][1],12000);
                                            compare_delay_load('#contentp',result["plans"][2],24000);
                                            $('#frontstatus').html(result["frontstatus"]);
                                            }
                                        else {
                                            console.log("Ajax-Fehlermeldung: "+result["status"]);    
                                            }
                                    },
                        error: function() { $('#alerter').html('<span style="color:#F55;">🗲</span>');}
        }
    )
}
var call_ajax_msections=function() {
    $('#alerter').html('<span style="color:yellow;">🗘</span>');
    console.log("call_ajax_msections aufgerufen");
    $.getJSON  (   {    url:    "/ti/do/ajax",
                        data:   "task=get_msections",
                        success: function(result) 
                                    {   $('#alerter').html('<span style="color:green;">🗸</span>'); // Da Netzwerkoperation erfolgreich
                                        if (result["status"]==='ok') {
                                            compare_delay_load('#contentm0',result["msections"][0],400,true);
                                            compare_delay_load('#contentm1',result["msections"][1],900,true);
                                            $('#frontstatus').html(result["frontstatus"]);
                                            console.log("call_ajax_msections erfolgreich");
                                            }
                                        else {
                                            console.log("Ajax-Fehlermeldung: "+result["status"]);    
                                            }
                                    },
                        error: function() { $('#alerter').html('<span style="color:#F55;">🗲</span>');}
        }
    );
    call_ajax_bottom();
}

var call_ajax_bottom=function() {
    $('#alerter').html('<span style="color:yellow;">🗘</span>');
    console.log("call_ajax_bottom aufgerufen");
    $.getJSON  (   {    url:    "/ti/do/ajax",
                        data:   "task=get_bottom",
                        success: function(result) 
                                    {   $('#alerter').html('<span style="color:green;">🗸</span>'); // Da Netzwerkoperation erfolgreich
                                        if (result["status"]==='ok') {
                                            if (result["bottom"][0]+result["bottom"][1]=='') {
                                                $("#bodenvorhang").animate({bottom:"-1000px"},500);
                                            } else {
                                                $("#bodenvorhang").animate({bottom:"0px"},500);
                                                window.setTimeout(function(){
                                                        $("#bodenvorhang").animate({bottom:"-"+($("#bodenvorhang").height()-28)+"px"}, 2000 ); 
                                                    }, 20000);
                                            }
                                            compare_delay_load('#contentbottomleft',result["bottom"][0],400,false);
                                            compare_delay_load('#contentbottomright',result["bottom"][1],500,false);
                                            $('#frontstatus').html(result["frontstatus"]);
                                            console.log("call_ajax_bottom erfolgreich");
                                            }
                                        else {
                                            console.log("Ajax-Fehlermeldung: "+result["status"]);    
                                            }
                                    },
                        error: function() { $('#alerter').html('<span style="color:#F55;">🗲</span>');}
        }
    )
}

/////////////////////////////////////7
// delay abwarten und dann
//          * ggf. ersetzen
//          * Scrollen verzoegert ermöglichen
function compare_delay_load(idStrg,content,delay,doScroll) {
    window.setTimeout(function(){  
        if ( $(idStrg).html()!=content )  $(idStrg).html(content)  
        //else console.log("..unveraendert.."); 
        window.setTimeout(function() { divscroll_start(idStrg); } , 5000 );
    },delay);
}

/////////////////////////////////////
// Hier wird ein div in seinem parent-div gescrollt
function divscroll_start(idStrg) { // macht einen Bewegungsdurchlauf
    let delta=$(idStrg).height()-$(idStrg).parent().height();
    if (delta>0) {
        console.log("Animiere Scroll fuer "+idStrg+" ueber "+delta+" Pixelzeilen");    
        $(idStrg).animate({top:"-" + delta + "px"},delta*50,'swing',divscroll_animationComplete);
    } else {
        console.log("Kein Scroll fuer "+idStrg+", da "+delta+" <0");    
    }
}
// und in 5 Sekunden wieder zurueck
function divscroll_animationComplete() {
  let myobj=$(this);
  window.setTimeout(function(){ 
    myobj.animate({top:"0px"}, 5000 ); },
    2000);
}

// $(document).ready( function() {        ti_start()    } )
