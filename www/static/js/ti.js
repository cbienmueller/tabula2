// dieser Code erledigt Kleinigkeiten
// Kontext: frame.py in tabula.info
// Autor: C. Bienmueller
// Rechte: public domain (da trivial)


var d_s_handle;
var ti_time_element;
var counter=50;
var timeout=1000;
var counterdelta=1;
const ms_per_line=800;

// Wird 1x z.B. aus dem Body-Tag aufgerufen
function ti_start() {
	if (window.location.search.substring(1).indexOf("RasPi")>=0) {
		timeout=2000; 
        counterdelta=2;
		console.log("RasPi -> slow... -> alle 2 Sekunden Zeit aktualisieren");}
    ti_time_element=document.getElementById("ti_time");
    counter=parseInt(ti_time_element.innerHTML)
	ti_time();
    d_s_handle=window.setInterval( function(){ ti_time() },timeout);
    startelementscrolling();
    document.getElementById("leftbase").addEventListener("click", function() {
        document.getElementById("leftbase").style.display = 'none'; }  );
    document.getElementById("rightbase").addEventListener("click", function() {
        document.getElementById("rightbase").style.display = 'none'; }  );
}
function startelementscrolling() {
    var allelements = document.getElementsByClassName('scrollcontent');
    /*console.log(allelements,"!");*/
    for (let oneelement of allelements) {
        scrollelement(oneelement);
    }
}
// Wird immer wieder aufgerufen 
function ti_time() {
    ti_time_element.innerHTML=" "+counter+"s";
    if (counter>=counterdelta) {
        counter-=counterdelta;
    }
    else {
        counter=0;
    }
}

// Nun: Scrollen eines div-Inhalts:
// scrollelement(document.getElementById('scrollcontent'));
    
function scrollelement(element) {
    if (element!=undefined) {
        var lasttop = element.scrollTop;
        const animateScroll = function(){
            var newtop=lasttop+1;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop===lasttop) {
                console.log("scrolling up ",lasttop);
                setTimeout(animateScroll, 5000);
            }
            else {
                console.log("ganz unten");
                setTimeout(animateReScroll, 5000);
            };
        };
        const animateReScroll = function(){
            var newtop=lasttop-5;
            element.scrollTop = newtop;
            lasttop = element.scrollTop;
            if(newtop>0) {
                console.log("scrolling down ",lasttop);
                setTimeout(animateReScroll, ms_per_line);
            }
            else {
                console.log("ganz oben");
                setTimeout(animateScroll, 5000);
            };
        };
        setTimeout(animateScroll, 5000);
    }
}

// BACKOFFICE / MANAGEMENT
var bo_request; //globale Variable
var bo_ti_status="9"; //kein legaler Wert, wird daher geändert
var bo_st_timer=0;

var call_ajax_bo_status=function() {
    console.log("call_ajax_bo_status");
    $.getJSON  (   {    url:    "/ti/do/ajax",
                        data:   "task=get_bo_status&origstatus="+bo_ti_status,
                        success: function(result) {
                                    nexttimeout=1000;
                                    if (result["status"]==='ok') {
                                        bo_ti_status=result["neu_status"];
                                        bo_zeige(result["html"]); 
                                        if (result["neu_status"]===405) {
                                            nexttimeout=60*1000;
                                        }
                                    }
                                    else {
                                        console.log("Ajax-Fehlermeldung: "+result["status"]);    
                                    }
                                    console.log("del:", bo_st_timer);
                                    window.clearTimeout(bo_st_timer);  // eliminiert ggf. parallel laufende timer
                                    bo_st_timer = window.setTimeout("call_ajax_bo_status()", nexttimeout+100*bo_st_timer);
                                    console.log("new:", bo_st_timer);
                                },
                        error: function() { 
                                    window.clearTimeout(bo_st_timer);  // eliminiert ggf. parallel laufende timer
                                    bo_st_timer = window.setTimeout("call_ajax_bo_status()", 1000);
                                }
        }
    )
}            

var bo_getAJAX = function() {
    call_ajax_bo_status();
}

function noneesta() {
    bo_request= new XMLHttpRequest();
    bo_request.open("GET", "ajax?task=get_bo_status&origstatus="+bo_ti_status);
    bo_request.onreadystatechange = checkData;
    bo_request.send(null);
} // end function getAJAX

function checkData() {
    if (bo_request.readyState == 4) {
        if ( bo_request.status == 200) {
            felder=bo_request.responseText.split(';', 2);
            bo_ti_status=felder[0];
            bo_zeige(felder[1]);
        }
        else{
            bo_zeige('&nbsp;<br>&nbsp;');
        }
        window.setTimeout("bo_getAJAX()", 1000)
    }   
} // end function checkData

function bo_zeige(statustext) {
    s_div=document.getElementById("statusmeldung");
    s_div.innerHTML = statustext;
} // end function zeige()

function tui_dialog() {
    // markiere das geklickte Feld gelb
    let mytarget = event.target;
    oldbackground = mytarget.style.background;
    mytarget.style.background = "yellow";
    let ds = mytarget.dataset;
    sslwarning = 'https:' != document.location.protocol ? 
                 'Deine Verbindung ist nicht verschlüsselt.\nDu solltest abbrechen statt Informationen einzugeben!\nGehe über https://ma.kg-ab.de/ti\n\n'
                  : "";

    // rufe dann den Prompt leicht verzögert auf, damit die Background-Änderung auch angezeigt wird.
    setTimeout(function () {
        //eingabe = prompt(sslwarning  + 
        //              "Gib erst die gewünschte Markierung und dann, nach einem Leerzeichen,\n den tabula-Code für "+ ds.uiperson + " ein,\num es für die " + ds.uistunde + 
        //              ". Stunde zu setzen. Unterstützte Markierungen: AiM, AbS und Nix (löschen)","AiM 23-CDE-89");
        eingabe = prompt(sslwarning +
                    "Für eine Markierung in der " + ds.uistunde + ". Stunde für " + ds.uiperson +
                    " gib erst\n- das Markierungskürzel (AiM, AbS oder Nix (zum löschen)),\n" + 
                    "- dann ein Leerzeichen und danach\n- deinen Code ein.\n" +
                    "Beispiel: AiM 23-C#E-89",""); 
        if (eingabe) { 
            einArray = eingabe.trim().split(" ");
            if (einArray.length!=2) {
                alert("Eingabe neu probieren:\nFormat mit Leerzeichen einhalten!\n"); mytarget.style.background = "#FBB";}
            else if (!["aim","abs","nix"].includes(einArray[0].toLowerCase()) ) {
                alert("Eingabe neu probieren:\n" + einArray[0]+" ist keine akzeptierte Markierung!\n"); mytarget.style.background = "#FBB";}
            else if (einArray[1].length !=9) {
                alert("Eingabe neu probieren:\nRichtigen Code eingeben!"); mytarget.style.background = "#FBB";}
            else { 
                let data = {    tidate: ds.uitidate,
                                person: ds.uiperson,
                                stunde: ds.uistunde,
                                wahl: einArray[0],
                                code: einArray[1] };

                
                fetch('/ti/do/userinput/turbo', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data)
                    
                })
                .then(res => res.json())
                .then(res => {    // dies wird nach dem Reply aufgerufen
                                    console.log("Request complete! response:", JSON.stringify(res));
                                    mytarget.style.background = res.ErgebnisFarbe;
                                    if (res.Ergebnis != "OK") {
                                        alert(res.Ergebnis);
                                    }
                                }
                        );

            }
        }
        else {
            mytarget.style.background = oldbackground;
        }
    }, 50);  // 0,05 Sekunden verzögert...
    
}

